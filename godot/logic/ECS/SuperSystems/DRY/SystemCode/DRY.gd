extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "DRY"
var entities: Array = []

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
        on_real_ready()

#func _process(delta) -> void:
#    pass
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func on_editor_ready() -> void:
    pass
    
func on_real_ready() -> void:
    pass

func reset_to_defaults():
    pass

func reset_for_next_game():
    pass
