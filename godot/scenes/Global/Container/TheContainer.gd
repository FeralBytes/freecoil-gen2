tool
extends Node
# Major.Minor.Micro-alpha, beta, rc, preview, dev
export(String) var VERSION: String = "0.1.0-dev0"
export(int) var VERSION_INT = 1  # Increment any time the micro changes.
export(bool) var update_boot_image: bool = false
export(String, FILE, ".tscn") var jump_to_state_post_splash: String = ""
export(String, "Android") var fake_os: String = ""
export(String, "FAKED8UNIQUE8ID8NOT8REAL8AT8ALL") var fake_unique_id: String = ""

export(bool) var erase_user_dir: bool = false

export(bool) var run_unit_tests: bool = false
export(bool) var run_specified_test: bool = false
export(float) var artificial_delay_between_states: float = 0
export(int, "not_set", "debug", "info", "warning", "error", "critical", 
    "testing") var debug_level: int = 3

# Declare member variables here.
var current_scene
var previous_scene
var active_scene_container = 0
var initialized = false
var ui_child_offset: Vector2

var SuperSystems: Dictionary = {}
var supersystem_execution_order: Array = []

func _ready():
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        if not initialized:
            current_scene = $Scene0
            add_to_group("Container")
            initialized = true

func on_editor_ready():
    pass
    # Currently supporting only 2 layouts and  aspecific aspect ratio of
    # 16:9 == 1920x1080 or 9:16 == 540x960
    if ProjectSettings.has_setting("display/window/size/width"):
        if ProjectSettings.get_setting("display/window/size/width") == 540:
            $"%TheCamera".offset.x = 270
        elif ProjectSettings.get_setting("display/window/size/width") == 1920:
            $"%TheCamera".offset.x = 960
    if ProjectSettings.has_setting("display/window/size/height"):
        if ProjectSettings.get_setting("display/window/size/height") == 960:
            $"%TheCamera".offset.y = 480
        elif ProjectSettings.get_setting("display/window/size/height") == 1080:
            $"%TheCamera".offset.y = 540

func next_screen(screen_jumps):
    var xy = screen_jumps.split_floats(",")
    get_tree().call_group("Camera", "instant_pan_camera", int(xy[0]), int(xy[1]))
    if PM.STD.get_("previous_screen_jumps") != PM.STD.get_("current_screen"):
        if PM.STD.get_("current_screen") != null:
            PM.STD.set_("previous_screen_jumps", PM.STD.get_("current_screen"))
    call_deferred("offset_UI_child", xy)
    PM.STD.set_("current_screen", screen_jumps)

func offset_UI_child(offsets):
    if $UI.get_child_count() != 0:
        var calc_offset_x = offsets[0] * 540 * 1
        var calc_offset_y = offsets[1] * 960 * 1
        $UI.get_child(0).rect_position.x = calc_offset_x
        $UI.get_child(0).rect_position.y = calc_offset_y
        ui_child_offset = $UI.get_child(0).rect_position

func offset_UI_for_virtual_keyboard(keyboard_height):
    $UI.get_child(0).rect_position = ui_child_offset
    $UI.get_child(0).rect_position.y += keyboard_height
