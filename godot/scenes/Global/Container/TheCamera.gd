extends Camera2D

var panning = false
var panning_speed = 25
var last_x = 0
var last_y = 0
var screen_width = 540
var screen_height = 960
var last_no_was_left = false
var onetime_setup_complete = false

# Called when the node enters the scene tree for the first time.
func _ready():
    add_to_group("Camera")
    
func _process(_delta):
    if PM.submodules_ready:
        if not onetime_setup_complete:
            onetime_setup_complete = true
            delayed_setup()
            PM.STD.set_("last_xy", [last_x, last_y])

func delayed_setup():
    var new_tween = Tween.new()
    new_tween.name = "Tween"
    var new_timer = Timer.new()
    new_timer.name = "Timer"
    var new_timer2 = Timer.new()
    new_timer2.name = "Timer2"
    add_child(new_tween)
    add_child(new_timer)
    add_child(new_timer2)
    
    
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

func calc_panning(steps_x, steps_y):
    var horizontal_dir = null
    var vertical_dir = null
    var total_x = (steps_x * screen_width)  # + (screen_width / 2)
    var total_y = (steps_y * screen_height)  # + (screen_height / 2)
    # Horizontal Panning in the x coordinate.
    if total_x < position.x:  # Negative
        horizontal_dir = "left"
    else:  # Positive
        horizontal_dir = "right"
    # Vertical Panning in the y coordinate.
    if total_y < position.y:  # Negative
        vertical_dir = "up"
    else:
        vertical_dir = "down"
    return [vertical_dir, horizontal_dir, total_x, total_y]
    
func pan_camera(steps_x, steps_y):
    while panning:
        yield(get_tree().create_timer(0.01), "timeout")
    PM.Log("Camera: Panning to: " + str(steps_x) + ", " + str(steps_y), "info")
    panning = true
    var array = calc_panning(steps_x, steps_y)
    var pan_vertical_direction = array[0]
    var pan_horizontal_direction = array[1]
    var total_to_pan_x = array[2]
    var total_to_pan_y = array[3]
    while panning:
        if pan_horizontal_direction == "right":
            if position.x + panning_speed > total_to_pan_x:
                position.x = total_to_pan_x
            else:
                position.x += panning_speed
        else:
            if position.x - panning_speed < total_to_pan_x:
                position.x = total_to_pan_x
            else:
                position.x -= panning_speed
        if pan_vertical_direction == "down":
            if position.y + panning_speed > total_to_pan_y:
                position.y = total_to_pan_y
            else:
                position.y += panning_speed
        else:
            if position.y - panning_speed < total_to_pan_y:
                position.y = total_to_pan_y
            else:
                position.y -= panning_speed
        yield(get_tree().create_timer(0.01), "timeout")
        if position.x == total_to_pan_x and position.y == total_to_pan_y:
            panning = false
    #yield(get_tree().create_timer(0.5), "timeout")
    if not panning:
        last_x = steps_x
        last_y = steps_y
        PM.STD.set_("last_xy", [last_x, last_y])
        get_tree().call_group("MenuButtons", "enabled", true)

func return_to_last_pan():
    pan_camera(last_x, last_y)        
    
func instant_pan_camera(steps_x, steps_y):
    var array = calc_panning(steps_x, steps_y)
    var total_to_pan_x = array[2]
    var total_to_pan_y = array[3]
    position.x = total_to_pan_x
    position.y = total_to_pan_y
    last_x = steps_x
    last_y = steps_y
    PM.STD.set_("last_xy", [last_x, last_y])
    get_tree().call_group("MenuButtons", "enabled", true)
    
func pan_for_virtual_keyboard(keyboard_height):
    var xy = PM.STD.get_("last_xy")
    var x = xy[0]
    var y = xy[1]
    instant_pan_camera(x, y)
    #FIXME: It may happen that when we do this on a screen that is in the 
    #       negative y that we may have to subtract instead of adding an offset.
    position.y += keyboard_height
    get_tree().call_group("Container", "offset_UI_for_virtual_keyboard", keyboard_height)

func shake_no():
    var total_shakes = 15
    get_node("Timer").wait_time = 1 / float(total_shakes)
    var __
    if not get_node("Timer").is_connected("timeout", self, "on_shake_frequency"):
        __ = get_node("Timer").connect("timeout", self, "on_shake_frequency")
    get_node("Timer2").wait_time = 0.2
    if not get_node("Timer2").is_connected("timeout", self, "on_duration_timeout"):
        __ = get_node("Timer2").connect("timeout", self, "on_duration_timeout")
    get_node("Timer2").one_shot = true
    get_node("Timer").start()
    get_node("Timer2").start()
    new_shake()

func new_shake():
    var amplitude_right = 80 + 270
    var amplitude_left = 270 - 80
    var rand = Vector2()
    if last_no_was_left:
        rand.x = rand_range(270 + 40, amplitude_right)
    else:
        rand.x = rand_range(amplitude_left, 270 - 40)
    last_no_was_left = !last_no_was_left
    rand.y = 480
    get_node("Tween").interpolate_property(self, "offset", offset, rand, get_node("Timer").wait_time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
    get_node("Tween").start()
    
func on_shake_frequency():
    new_shake()

func on_duration_timeout():
    reset_offset()
    get_node("Timer").stop()

func reset_offset():
    get_node("Tween").interpolate_property(self, "offset", offset, Vector2(270, 480), get_node("Timer").wait_time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
    get_node("Tween").start()
