extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "GameCommon"

export(int) var editor_yield_duration: int = 60

var game_over = false
var last_connection_status = null
var next_available_player_laser_id = 1
# 0 is not loaded and not_ready, 1 is loaded but not ready, 2 is loaded and ready/waiting,
# 3 is actively playing, 4 is game over
var game_state
var game_state_by_mup = {}
var change_weapon_in_process = false
var min_frame_rate:float = 1.0 / 240.0
var frame_start_time: float
var all_in_game_ui_added_to_tree = false
var generic_timer_limit: int
var all_process_events_handled = true
var in_game_added_to_tree_complete = false
var game_start_time: int
var shot_count_begin
var shot_count_end
var will_be_freed: bool = false
var set_player_start_game_vars_comp: bool = false

var reset_in_progress = false
var mups_that_have_ack_reset = {}
var mups_that_have_completed_reset = {}
var reset_status = "No reset has been started."
var host_knows_reset_is_complete = false

onready var ReloadSound = get_node("ReloadSound")
onready var EmptyShotSound = get_node("EmptyShotSound")
onready var GunShotSound = get_node("GunShotSound")
onready var RespawnTimedSound = get_node("RespawnTimedSound")
onready var RespawnZoneSound = get_node("RespawnZoneSound")
onready var TangoDownSound = get_node("TangoDownSound")
onready var NiceSound = get_node("NiceSound")
onready var HitIndicatorTimer = get_node("HitIndicatorTimer")
onready var RespawnTimer = get_node("RespawnDelayTimer")
onready var ReloadTimer = get_node("ReloadTimer")
onready var TickTocTimer = get_node("TickTocTimer")
onready var EventRecordTimer = get_node("EventRecordTimer")
onready var StartGameTimer = get_node("StartGameTimer")
onready var TimeRemainingTimer = get_node("TimeRemainingTimer")
onready var CombatPayTimer = get_node("CombatPayTimer")
var LPanel: ReferenceRect
var RPanel: ReferenceRect
var Footer: ReferenceRect
var SupplyDepot: ReferenceRect
var StartPanel: ReferenceRect
var WaitingPanel: ReferenceRect
var RespawnPanel: ReferenceRect

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
        on_real_ready()

func _process(_delta) -> void:
    # Process Alerts
    pass
                    
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func process_event_all_other_events(event):
    if event["type"] == "fired":
        process_event_fired(event)
    elif event["type"] == "misfired":
        process_event_misfired(event)
    elif event["type"] == "reloading":
        process_event_reloading(event)
    elif event["type"] == "died":
        process_event_died(event)
    elif event["type"] == "hit":
        process_event_hit(event)
    elif event["type"] == "shots_update":
        process_event_shots_update(event)
    elif event["type"] == "eliminated":
        process_event_eliminated(event)
    elif event["type"] == "end_game":
        process_event_end_game(event)
    elif event["type"] == "edit_map_object":
        process_event_edit_map_object(event)
    elif event["type"] == "comms_message":
        process_event_send_comms_message(event)
    elif event["type"] == "award_credits":
        process_event_award_credits(event)
    elif event["type"] == "test_client":
        process_event_test_client(event)
    elif event["type"] == "test_server":
        process_event_test_server(event)


func check_if_frame_time_exceeded():
    if all_process_events_handled:  # No need to keep looping in process, if everything is done.
        return true
    var time_passed = OS.get_ticks_msec() - frame_start_time
    if time_passed > min_frame_rate * 1000:
        return true
    return false

func on_editor_ready() -> void:
    pass
    
func on_real_ready() -> void:
    PM.Networking.EventSync.event_processing_hooks_refs["GameCommon"] = self
    game_state = 0
    PM.STD.set_("game_status", 0)
    if PM.STD.get_("is_host") >= 1:
        PM.Networking.set_process(true)
        PM.Networking.EventSync.is_a_client = false
    fix_looping_audio_to_not()
    if PM.STD.get_("is_host") >= 1:
        PM.STD.connect(PM.STD.monitor_(
            "game_status"), self, "game_status_update"
        )
        var mups_to_peers = PM.STD.get_("mups_to_peers")
        for mup in mups_to_peers:
            PM.SS["GameCommon"].game_state_by_mup[mup] = 0  # 0 is not loaded and ready
        PM.STD.connect(PM.STD.monitor_("mups_status"), 
                self, "on_new_identified_connection")
        PM.STD.connect(PM.STD.monitor_("mups_reconnected"), self, 
                "on_mup_reconnected")
        PM.STD.set_("player_laser_id_by_mup", {})
        invert_mups_to_lasers(PM.STD.get_("player_laser_id_by_mup"))
        if PM.STD.get_("is_host") == 1:
            get_next_available_player_laser_id(PM.unique_id)
        yield(server_setup_ingame_vars(), "completed")
    else:
        while not PM.Networking.server_time_deviation_is_acceptable():
            if will_be_freed:
                return
            yield(get_tree(), "idle_frame")
        if not PM.STD.get_("is_observer"):
            PM.Networking.EventSync.record_event("client_request", 
                {"method": "get_next_available_player_laser_id", "object": "GameCommon"
                }, PM.Networking.get_server_time_usec()
            )
    if PM.STD.get_("force_recoil_action") == null:
        PM.STD.set_("force_recoil_action", false)
    var force_recoil_action = PM.STD.get_("force_recoil_action")
    var recoil_action = PM.LTD.get_("recoil_action")
    if force_recoil_action == true and recoil_action == true:
        PM.SS["FreecoiLInterface"].enable_recoil(true)
    elif force_recoil_action == false or recoil_action == false:
        PM.SS["FreecoiLInterface"].enable_recoil(false)
    # Make Connections
    PM.STD.connect(PM.STD.monitor_("fi_trigger_btn_counter"
            ), self, "fi_trigger_btn_counter_event")
    PM.STD.connect(PM.STD.monitor_("fi_reload_btn_counter"
            ), self, "fi_reload_btn_counter_event")
    PM.STD.connect(PM.STD.monitor_("fi_power_btn_counter"
            ), self, "fi_power_btn_counter_event")
    PM.STD.connect(PM.STD.monitor_("fi_thumb_btn_counter"
            ), self, "fi_thumb_btn_counter_event")
    PM.STD.connect(PM.STD.monitor_("connection_status"
            ), self, "connection_status_event")
    PM.STD.connect(PM.STD.monitor_("fi_shooter1_shot_counter"
            ), self, "fi_shot_by_shooter1")
    PM.STD.connect(PM.STD.monitor_("fi_shooter2_shot_counter"
            ), self, "fi_shot_by_shooter2")
    
    if PM.STD.get_("is_host") >= 1:
        set_player_start_game_vars()
    else:
        pass

func in_editor_yield():
    for _i in range(editor_yield_duration):
        yield(get_tree(), "idle_frame")

func invert_mups_to_lasers(mups_to_lasers):
    if PM.STD.get_("is_host") >= 1:
        var lasers_to_mups = {}
        for key in mups_to_lasers:
            lasers_to_mups[mups_to_lasers[key]] = key
        # NOTE: For JSON Objects must have keys that are strings not Integers.
        # Invert players and do not store in JSON.
        PM.STD.set_("player_mup_by_laser_id", lasers_to_mups)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_mup_by_laser_id", "var_val": lasers_to_mups},
            PM.Networking.get_server_time_usec()
        )

func fi_trigger_btn_counter_event(_counter):
    if PM.STD.get_("game_player_alive"):
        if PM.STD.get_("game_weapon_magazine_ammo") == 0:
            EmptyShotSound.volume_db = 0
            EmptyShotSound.play()
            PM.Networking.EventSync.record_event("misfired", 
                {"gun": PM.STD.get_("game_weapon_type")}, 
                PM.Networking.get_server_time_usec()
            )
        else:
            GunShotSound.volume_db = 0
            GunShotSound.play()
            PM.Networking.EventSync.record_event("fired", 
                {"gun": PM.STD.get_("game_weapon_type")},
                PM.Networking.get_server_time_usec())
    #else you are dead so pass.
    
func fi_reload_btn_counter_event(__):
    if PM.STD.get_("game_player_alive"):
        reload_start()

func fi_power_btn_counter_event(__):
    if PM.STD.get_("game_player_alive"):
        var game_weapon_shot_modes = PM.STD.get_("game_weapon_shot_modes")
        var number_of_shot_modes = game_weapon_shot_modes.size()
        if number_of_shot_modes > 1:
            var game_weapon_shot_mode = PM.STD.get_("game_weapon_shot_mode")
            var current_shot_mode_index = game_weapon_shot_modes.find(game_weapon_shot_mode)
            var next_shot_mode_index
            if current_shot_mode_index + 1 == number_of_shot_modes: #Add 1 for 0-based array index
                next_shot_mode_index = 0
            else:
                next_shot_mode_index = current_shot_mode_index + 1
            PM.STD.set_("game_weapon_shot_mode", game_weapon_shot_modes[next_shot_mode_index])
            #Implement new firing mode setting
            var long_power
            var short_power
            if PM.STD.get_("indoor_game") == true:
                long_power = PM.STD.get_("game_weapon_indoor_long_power")
                short_power = 0
            else: #Outdoor game
                long_power = PM.STD.get_("game_weapon_long_power")
                short_power = PM.STD.get_("game_weapon_short_power")
            PM.SS["FreecoiLInterface"].new_set_shot_mode(
                PM.STD.get_("game_weapon_shot_mode"), 
                long_power, short_power, PM.STD.get_("game_weapon_damage")
            )

func fi_thumb_btn_counter_event(__):
    if PM.STD.get_("game_player_alive") and not PM.STD.get_("is_medic"):
        change_weapon(null)
                
func fi_shot_by_shooter1(__):
    process_shot_by_shooter(1, PM.STD.get_("fi_shooter1_laser_id"), \
            PM.STD.get_("fi_shooter1_weapon_profile"))
    
func fi_shot_by_shooter2(__):
    process_shot_by_shooter(2, PM.STD.get_("fi_shooter2_laser_id"), \
            PM.STD.get_("fi_shooter2_weapon_profile"))

func process_shot_by_shooter(shooter1_or2, laser_id, shooter_weapon_profile):
    #this area is only coded to deal with weapon profiles as damage values
    #future plan is to constrain to profile/damage of 1-3, and not use other profile #s for damage
    var legit_hit = false
    # We already check that it is not laser ID 0 with a counter or 0 in FrecoiLInterface.gd.
    if PM.STD.get_("game_player_alive"):
        if laser_id in PM.STD.get_("player_mup_by_laser_id"): 
            #laser id is a valid id in the current game.
            var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
            if PM.STD.get_("player_medics_by_mup")[shooter_mup]: #Shot by Medic
                var hit_by_team_medic = {"laser_id":laser_id, "weapon_profile":shooter_weapon_profile}
                if PM.STD.get_("team_amount") > 0:
                    if (shooter_mup in PM.STD.get_("game_player_teammates")):
                        PM.STD.set_("hit_by_team_medic", hit_by_team_medic)
                else:
                    if PM.STD.get_("game_status") == 2: 
                        #Pregame (where medics can heal anyone)
                        PM.STD.set_("hit_by_team_medic", hit_by_team_medic)
            else: #Not Medic
                if PM.STD.get_("team_amount") > 0:
                    if PM.STD.get_("friendly_fire"):
                        legit_hit = true
                    else:
                        if not (shooter_mup in PM.STD.get_("game_player_teammates")):
                            legit_hit = true
                else:
                    legit_hit = true
    if legit_hit:
        var damage = shooter_weapon_profile #shorten name
        process_player_damage(damage, laser_id, "Gun", shooter1_or2)

func process_player_damage(damage, laser_id, weapon_type, _shooter1_or2=null):
    if PM.STD.get_("game_player_alive"):
        var takes_damage = false #Initial value
        #Determine if medic, and if medics take this type of damage
        if PM.STD.get_("is_medic"):
            var medic_type = PM.STD.get_("medic_type")
            if weapon_type == "Gun":
                if medic_type == 3 or medic_type == 4 or medic_type == 7 or medic_type == 8:
                    takes_damage = true
            elif weapon_type == "Outside Field":
                takes_damage = true
            elif weapon_type == "In Storm":
                takes_damage = true
            else: #All area-effect weapons
                if medic_type == 2 or medic_type == 4 or medic_type == 6 or medic_type == 8:
                    takes_damage = true
        else:
            takes_damage = true
        #Process damage
        if takes_damage:
            #Process for different weapon types
            if weapon_type == "Toxic Cloud": #Bypasses shields
                if not PM.STD.get_("gas_mask_active"):
                        PM.STD.set_("game_player_health", PM.STD.get_(
                                "game_player_health") - damage)
                else: #Gas mask is active
                    var gas_mask_status = PM.STD.get_("gas_mask_status")
                    if gas_mask_status >= damage:
                        gas_mask_status -= damage
                    else:
                        damage -= gas_mask_status
                        gas_mask_status = 0
                        PM.STD.set_("game_player_health", PM.STD.get_(
                                "game_player_health") - damage)
                    PM.STD.set_("gas_mask_status", gas_mask_status)
            else: #Doesn't bypass shields
                if PM.STD.get_("game_player_shield") >= damage:
                    PM.STD.set_("game_player_shield", PM.STD.get_(
                            "game_player_shield") - damage)
                else:
                    damage = damage - PM.STD.get_("game_player_shield")
                    PM.STD.set_("game_player_shield", 0)
                    PM.STD.set_("game_player_health", PM.STD.get_(
                            "game_player_health") - damage)
                call_deferred("delayed_vibrate")  # Because it was slowing down the processing of shots.
            if PM.STD.get_("game_player_health") <= 0:
                if PM.STD.get_("supply_depot_active"):
                    if PM.STD.get_("player_killing_spree") >= 3:
                        assess_accomplishment("killed_while_on_killing_spree", laser_id)
                    PM.STD.set_("player_killing_spree", 0)
                if PM.STD.get_("game_player_deaths") + 1 > PM.STD.get_(
                        "respawns_allowed"):
                    eliminated(laser_id)
                else:
                    respawn_start(laser_id)
            else:
                if weapon_type == "Gun":
                    var hexagons_to_blink = 0
                    if PM.STD.get_("fi_shooter" + str(_shooter1_or2) + "_sensor_clip") != 0:
                        hexagons_to_blink += 1
                    if PM.STD.get_("fi_shooter" + str(_shooter1_or2) + "_sensor_front") != 0:
                        hexagons_to_blink += 2
                    if PM.STD.get_("fi_shooter" + str(_shooter1_or2) + "_sensor_left") != 0:
                        hexagons_to_blink += 4
                    if PM.STD.get_("fi_shooter" + str(_shooter1_or2) + "_sensor_right") != 0:
                        hexagons_to_blink += 8
                    PM.SS["BlinkHexTris"].blink_hexagons(hexagons_to_blink)
            if weapon_type == "Gun":
                #Add player hits to sync variable
                PM.Networking.EventSync.record_event("hit", {"laser_id": laser_id},
                    PM.Networking.get_server_time_usec())

func server_setup_ingame_vars():
    yield(get_tree(), "idle_frame")
    if PM.STD.get_("is_host") >= 1:
        PM.SS["GameMode"].modify_server_setup_ingame_vars()
        var game_mode = PM.STD.get_("game_settings")["game_mode"]
        PM.STD.set_("game_mode", game_mode)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_mode",
             "var_val": game_mode}, PM.Networking.get_server_time_usec()
        )
        var team_amount = PM.STD.get_("game_settings")["team_amount"]
        PM.STD.set_("team_amount", team_amount)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "team_amount", 
            "var_val": team_amount}, PM.Networking.get_server_time_usec()
        )
        #FIXME: game_objective and game_objective_goal to be implemented later.
        #If game_objective_goal = 0 then game time only is used as the game end criteria.
        #If game_objective_goal != 0 then game should end when game_objective_goal is reached by a
        #player or team (game is won).
        var game_objective = PM.STD.get_("game_settings")["game_objective"]
        PM.STD.set_("game_objective", game_objective)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_objective", "var_val": game_objective}, 
            PM.Networking.get_server_time_usec()
        )
        var game_objective_goal = PM.STD.get_("game_settings")["game_objective_goal"]
        PM.STD.set_("game_objective_goal", game_objective_goal)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_objective_goal", "var_val": game_objective_goal}, 
            PM.Networking.get_server_time_usec()
        )
        var game_length_time = PM.STD.get_("game_settings")["game_length_time"]
        PM.STD.set_("game_length_time", game_length_time)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_length_time", "var_val": game_length_time}, 
            PM.Networking.get_server_time_usec()
        )
        var supply_depot_active = PM.STD.get_("game_settings")["supply_depot_active"]
        PM.STD.set_("supply_depot_active", supply_depot_active)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "supply_depot_active", "var_val": supply_depot_active},
            PM.Networking.get_server_time_usec()
        )
        var supply_depot_type = PM.STD.get_("game_settings")["supply_depot_type"]
        PM.STD.set_("supply_depot_type", supply_depot_type)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "supply_depot_type", "var_val": supply_depot_type},
            PM.Networking.get_server_time_usec()
        )
        var supply_depot_active_shops = PM.STD.get_("game_settings")["supply_depot_active_shops"]
        PM.STD.set_("supply_depot_active_shops", supply_depot_active_shops)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "supply_depot_active_shops", "var_val": supply_depot_active_shops},
            PM.Networking.get_server_time_usec()
        )
        var respawns_allowed = PM.STD.get_("game_settings")["respawns_allowed"]
        PM.STD.set_("respawns_allowed", respawns_allowed)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "respawns_allowed", "var_val": respawns_allowed},
            PM.Networking.get_server_time_usec()
        )
        #FIXME: respawn_type to be implemented later.  Currently it is assumed as "Timed" (unlimited)
        #in the gamecommon code.
        var respawn_type = PM.STD.get_("game_settings")["respawn_type"]
        PM.STD.set_("respawn_type", respawn_type)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "respawn_type", 
            "var_val": respawn_type}, PM.Networking.get_server_time_usec()
        )
        var respawn_delay = PM.STD.get_("game_settings")["respawn_delay"]
        PM.STD.set_("respawn_delay", respawn_delay)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "respawn_delay",
             "var_val": respawn_delay}, PM.Networking.get_server_time_usec()
        )
        var initial_health = PM.STD.get_("game_settings")["initial_health"]
        PM.STD.set_("initial_health", initial_health)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "initial_health",
            "var_val": initial_health}, PM.Networking.get_server_time_usec()
        )
        var initial_shield = PM.STD.get_("game_settings")["initial_shield"]
        PM.STD.set_("initial_shield", initial_shield)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "initial_shield",
            "var_val": initial_shield}, PM.Networking.get_server_time_usec()
        )
        var friendly_fire = PM.STD.get_("game_settings")["friendly_fire"]
        PM.STD.set_("friendly_fire", friendly_fire)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "friendly_fire",
            "var_val": friendly_fire}, PM.Networking.get_server_time_usec()
        )
        var force_recoil_action = PM.STD.get_("game_settings")["force_recoil_action"]
        PM.STD.set_("force_recoil_action", force_recoil_action)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "force_recoil_action", "var_val": force_recoil_action},
            PM.Networking.get_server_time_usec()
        )
        var indoor_game = PM.STD.get_("game_settings")["indoor_game"]
        PM.STD.set_("indoor_game", indoor_game)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "indoor_game",
            "var_val": indoor_game}, PM.Networking.get_server_time_usec()
        )
        var heal_per_first_aid_kit = PM.STD.get_("game_settings")["heal_per_first_aid_kit"]
        PM.STD.set_("heal_per_first_aid_kit", heal_per_first_aid_kit)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "heal_per_first_aid_kit", "var_val": heal_per_first_aid_kit}, 
            PM.Networking.get_server_time_usec()
        )
        var heal_per_health_kit = PM.STD.get_("game_settings")["heal_per_health_kit"]
        PM.STD.set_("heal_per_health_kit", heal_per_health_kit)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "heal_per_health_kit", "var_val": heal_per_health_kit},
            PM.Networking.get_server_time_usec()
        )
        var initial_first_aid_kits = PM.STD.get_("game_settings")["initial_first_aid_kits"]
        PM.STD.set_("initial_first_aid_kits", initial_first_aid_kits)
        PM.Networking.EventSync.record_event("sync_var",
            {"var_name": "initial_first_aid_kits", "var_val": initial_first_aid_kits},
            PM.Networking.get_server_time_usec()
        )
        var initial_health_kits = PM.STD.get_("game_settings")["initial_health_kits"]
        PM.STD.set_("initial_health_kits", initial_health_kits)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "initial_health_kits", "var_val": initial_health_kits},
            PM.Networking.get_server_time_usec()
        )
        var supply_depot = PM.STD.get_("supply_depot")
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "supply_depot",
            "var_val": supply_depot}, PM.Networking.get_server_time_usec()
        )
        var initial_inventory = PM.STD.get_("initial_inventory")
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "initial_inventory",
            "var_val": initial_inventory}, PM.Networking.get_server_time_usec()
        )
        if 1:#FIXME...game_mode == "Battle Royale": #FIXME: Perhaps this belongs in game-specific script?
            var storm_path:Array = []
            var initial_storm_path = [[1.0, 1.0], [1.0, -1.0],[-1.0, -1.0], [-1.0, 1.0]]
            var temp_quadrant_logger:Array = [] #Prevent re-use of quadrant
            var random_coeff #Vary the storm path within the field limits
            storm_path.append([0.0, 0.0]) #Preload array with initial storm position 
            #Randomize quadrant order, and randomize storm path within each quadrant
            while storm_path.size() != 5:
                var index = PM.SS["RandomGenerator"].roll_int_die(0, 3)
                if not temp_quadrant_logger.has(index):
                    for index2 in range(0,2):
                        random_coeff = float(PM.SS["RandomGenerator"].roll_int_die(1, 100))/100.00
                        initial_storm_path[index][index2] = initial_storm_path[index][index2] * random_coeff
                    storm_path.append(initial_storm_path[index])
                    temp_quadrant_logger.append(index)
            PM.STD.set_("storm_path", storm_path)
            PM.Networking.EventSync.record_event("sync_var", {"var_name": "storm_path",
                "var_val": storm_path}, PM.Networking.get_server_time_usec()
            )
        # This is the teams by team number
        PM.STD.set_("game_teams_by_team_num_by_mup",{})
        #FIXME: The below code was just kept around for reference for future team modes.
        PM.STD.set_("players_team_num_by_mup", {PM.unique_id: 0})
        if team_amount > 0:
            var game_teams_by_team_num_by_mup = PM.STD.get_("game_teams_by_team_num_by_mup")
            var game_team_status_by_num = {}
            for team_num in range(0, game_teams_by_team_num_by_mup.size()):
                if team_num == 0:
                    pass
                else:
                    game_team_status_by_num[team_num] = "playing"
            PM.STD.set_("teams_status_by_team_num", game_team_status_by_num)
        else:
            PM.STD.set_("teams_status_by_team_num", {})

        PM.STD.set_("start_game_delay", 8)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "start_game_delay", "var_val": 8},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("game_weapon_types", PM.STD.get_("armory"))
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_weapon_types",
            "var_val": PM.STD.get_("armory")},
            PM.Networking.get_server_time_usec()
        )
        var amount = PM.SS["RandomGenerator"].roll_int_die(1, 5) #Five Level 1 Weapons total
        var start_weapons = []
        var start_weapons_number = []
        for _i in range(0, amount):
            var random_weapon_number = PM.SS["RandomGenerator"].roll_int_die(1, 5)
            while (random_weapon_number in start_weapons_number):
                random_weapon_number = PM.SS["RandomGenerator"].roll_int_die(1, 5)
            start_weapons_number.append(random_weapon_number)
            start_weapons.append("W" + str(random_weapon_number) + "A")
        PM.STD.set_("game_start_weapon_types", start_weapons)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_start_weapon_types", "var_val": start_weapons},
            PM.Networking.get_server_time_usec()
        )
        var start_ammo = {}
        var game_weapon_types = PM.STD.get_("game_weapon_types")
        for weapon in start_weapons:
            start_ammo[weapon] = int(game_weapon_types[weapon]["initial_ammo"] * \
                    (float(PM.SS["RandomGenerator"].roll_int_die(5, 15))/10.0))
        PM.STD.set_("game_start_ammo", start_ammo)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_start_ammo", "var_val": start_ammo},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("game_weapon_prob_default", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapon_prob_default", "var_val": {}}, 
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("game_weapon_probabilities", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapon_probabilities", "var_val": {}},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("game_weapons_ammo_drop", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapons_ammo_drop", "var_val": {}}, 
            PM.Networking.get_server_time_usec()
        )
        var shots = {}
        for mup in PM.STD.get_("mups_to_peers"):
            shots[mup] = 0
        PM.STD.set_("player_shots_by_mup", shots)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_shots_by_mup", "var_val": shots},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("player_hits_by_mup", shots.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_hits_by_mup", "var_val": shots.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("player_kills_by_mup", shots.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_kills_by_mup", "var_val": shots.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("player_deaths_by_mup", shots.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_deaths_by_mup", "var_val": shots.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("player_medics_by_mup", shots.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_medics_by_mup", "var_val": shots.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        var names = {}
        for mup in PM.STD.get_("mups_to_peers"):  # reusing this variable for names
            names[mup] = ""
        PM.STD.set_("player_name_by_mup", names)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_name_by_mup", "var_val": names}, 
            PM.Networking.get_server_time_usec()
        )
        var player_avatars = {}
        for mup in PM.STD.get_("mups_to_peers"):  # reusing this variable for names
            player_avatars[mup] = ""
        PM.STD.set_("player_avatar_by_mup", player_avatars)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_avatar_by_mup", "var_val": player_avatars}, 
            PM.Networking.get_server_time_usec()
        )

func set_player_start_game_vars():
    tell_server_my_name()
    tell_server_my_picture_path()
    tell_server_my_medic_status()
    var func_state = set_player_respawn_vars()
    yield(func_state, "completed")
    PM.STD.set_("game_player_alive", false)
    PM.STD.set_("game_tick_toc_time_remaining",
            PM.STD.get_("game_length_time"))
    PM.STD.set_("game_tick_toc_time_elapsed", 0)
    # Quad State: 0=Not Started, 1=Started, 2=Paused, 3=Game Over
    PM.STD.set_("game_started", 0)  
    #if PM.run_ITM and PM.ITM_act_as_sever?
#    PM.STD.set_("game_player_team", 
#           PM.STD.get_("players_team_num_by_mup")[PM.unique_id])
#    PM.STD.set_("game_player_teammates", PM.STD.get_(
#        "game_teams_by_team_num_by_mup")[PM.STD.get_("game_player_team")])
    PM.STD.set_("game_player_last_killed_by", "")
    PM.STD.set_("game_player_shots", 0)
    PM.STD.set_("game_player_hits", 0)
    PM.STD.set_("game_player_deaths", 0)
    PM.STD.set_("game_player_kills", 0)
    PM.STD.set_("player_killing_spree", 0)
    PM.STD.set_("game_player_credits", PM.STD.get_("initial_credits"))
    PM.STD.set_("game_player_awarded_credits", {"credits":0,"time":0})
    PM.STD.set_("map_objects", {})
    PM.STD.set_("game_player_comm_message", {})
    ReloadTimer.wait_time = PM.STD.get_("game_weapon_reload_speed")
    ReloadTimer.connect("timeout", self, "reload_finish")
    HitIndicatorTimer.wait_time = PM.LTD.get_("player_hit_indicator_duration")
    HitIndicatorTimer.connect("timeout", self, "hit_indicator_stop")
    if PM.STD.get_("respawn_delay") > 0:
        RespawnTimer.connect("timeout", self, "respawn_finish")
        RespawnTimer.wait_time = PM.STD.get_("respawn_delay")
    if PM.STD.get_("is_host") >= 1:
        # Now that the server is all setup, we can tell the clients to start finishing their setup.
        PM.Networking.EventSync.record_event("server_request", {"mup": "all", 
            "method": "set_player_start_game_vars", "object": "GameCommon"}
            , PM.Networking.get_server_time_usec()
        )
        PM.Networking.EventSync.record_event("server_request", {"mup": "all", 
            "method": "now_entering_game_state_2", "object": "GameCommon"}, 
            PM.Networking.get_server_time_usec()
        )
    set_player_start_game_vars_comp =  true

func set_player_respawn_vars():
    yield(get_tree(), "idle_frame")  # forcfully become a coroutine.
    if PM.STD.get_("is_medic"): #Weapon settings for medic currently in code only
        PM.STD.set_("game_weapon_name", "None")
        PM.STD.set_("game_weapon_damage", 1)
        PM.STD.set_("game_weapon_shot_modes", ["single"])
        PM.STD.set_("game_weapon_shot_mode", 
                PM.STD.get_("game_weapon_shot_modes")[0])
        PM.STD.set_("game_weapon_magazine_size", 4)
        PM.STD.set_("game_weapon_magazine_ammo", 0)
        PM.STD.set_("game_player_ammo", 0)
        PM.STD.set_("game_weapon_total_ammo", 0)
        PM.STD.set_("game_weapon_reload_speed", 2)
        PM.STD.set_("game_weapon_rate_of_fire", 1)
        PM.STD.set_("game_weapon_short_power", 30)
        PM.STD.set_("game_weapon_long_power", 0)
        PM.STD.set_("game_weapon_indoor_long_power", 15)
    else:
        while PM.STD.get_("game_start_weapon_types") == null:
            yield(get_tree(), "idle_frame")
        var start_game_wpn_types = PM.STD.get_("game_start_weapon_types")
        var weapon_type = start_game_wpn_types[0]
        PM.STD.set_("game_player_weapons", start_game_wpn_types)
        PM.STD.set_("game_weapon_type", weapon_type)
        PM.STD.set_("game_weapon_name",
            PM.STD.get_("game_weapon_types")[weapon_type]["name"])
        PM.STD.set_("game_weapon_damage", 
            PM.STD.get_("game_weapon_types")[weapon_type]["damage"])
        PM.STD.set_("game_weapon_shot_modes", 
            PM.STD.get_("game_weapon_types")[weapon_type]["shot_modes"])
        PM.STD.set_("game_weapon_shot_mode", PM.STD.get_(
                "game_weapon_shot_modes")[0])
        PM.STD.set_("game_weapon_magazine_size", 
            PM.STD.get_("game_weapon_types")[weapon_type]["magazine_size"])
        PM.STD.set_("game_weapon_magazine_ammo", 0)
        PM.STD.set_("game_player_ammo", PM.STD.get_("game_start_ammo"))
        PM.STD.set_("game_weapon_total_ammo", PM.STD.get_(
                "game_player_ammo")[weapon_type])
        PM.STD.set_("game_weapon_reload_speed", 
            PM.STD.get_("game_weapon_types")[weapon_type]["reload_speed"])
        PM.STD.set_("game_weapon_rate_of_fire", 
            PM.STD.get_("game_weapon_types")[weapon_type]["rate_of_fire"])
        PM.STD.set_("game_weapon_short_power", 
            PM.STD.get_("game_weapon_types")[weapon_type]["short_power"])
        PM.STD.set_("game_weapon_long_power", 
            PM.STD.get_("game_weapon_types")[weapon_type]["long_power"])
        PM.STD.set_("game_weapon_indoor_long_power", 
            PM.STD.get_("game_weapon_types")[weapon_type]["indoor_long_power"])
    var long_power
    var short_power
    while PM.STD.get_("indoor_game") == null:
        yield(get_tree(), "idle_frame")
    if PM.STD.get_("indoor_game") == true:
        long_power = PM.STD.get_("game_weapon_indoor_long_power")
        short_power = 0
    else: #Outdoor game
        long_power = PM.STD.get_("game_weapon_long_power")
        short_power = PM.STD.get_("game_weapon_short_power")
    PM.SS["FreecoiLInterface"].new_set_shot_mode(PM.STD.get_(
        "game_weapon_shot_mode"), long_power, short_power, 
        PM.STD.get_("game_weapon_damage")
    ) #Shift damage 1 for 0-based weapon_profile

    PM.STD.set_("game_player_health", PM.STD.get_("initial_health"))
    PM.STD.set_("game_player_shield", PM.STD.get_("initial_shield"))

func near_time(input):
    # is the currrent delta(min/max) variance allowed.
    return abs(EventRecordTimer.time_left - input) <= 1  

func on_mup_reconnected(mups_reconnected):
    if PM.STD.get_("is_host") >= 1:
        var mup_id = mups_reconnected.pop_front()
        if PM.SS["GameCommon"].game_state_by_mup.has(mup_id):
            PM.Log("on_mup_reconnected( " + str(mups_reconnected) + " )")
        else:
            PM.SS["GameCommon"].game_state_by_mup[mup_id] = 0

func on_new_identified_connection(mups_status):
    for mup in mups_status:
        if not PM.SS["GameCommon"].game_state_by_mup.has(mup):
            PM.SS["GameCommon"].game_state_by_mup[mup] = 0
    
func process_event_fired(event_to_sort):
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            GunShotSound.volume_db = -25
            GunShotSound.play()
    if PM.STD.get_("is_host") >= 1:  # Server
        pass

func process_event_misfired(event_to_sort):
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            EmptyShotSound.volume_db = -20
            EmptyShotSound.play()
    if PM.STD.get_("is_host") >= 1:  # Server
        pass

func process_event_reloading(event_to_sort):
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            ReloadSound.volume_db = -20
            ReloadSound.play()
    if PM.STD.get_("is_host") >= 1:  # Server
        pass

func process_event_died(event_to_sort):  # Some player died.
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.STD.get_(
                "player_laser_id"
            ):
                TangoDownSound.play()
        else:
            pass
    if PM.STD.get_("is_host") >= 1:  # Server
        var laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
        var victim_mup = event_to_sort["rec_by"]
        var player_kills_by_mup = PM.STD.get_("player_kills_by_mup")
        var player_deaths_by_mup = PM.STD.get_("player_deaths_by_mup")
        #Discard accidental self-eliminations, and eliminations of fellow teammembers
        if PM.STD.get_("team_amount") > 0:
            var player_team_by_mup = PM.STD.get_("players_team_num_by_mup")
            var shooter_team_number = player_team_by_mup[shooter_mup]
            var victim_team_number = player_team_by_mup[victim_mup]
            if shooter_mup != victim_mup and not shooter_team_number == victim_team_number:
                player_kills_by_mup[shooter_mup] = player_kills_by_mup[shooter_mup] + 1
                PM.STD.set_("player_kills_by_mup", player_kills_by_mup)
        else:
            if shooter_mup != victim_mup:
                player_kills_by_mup[shooter_mup] = player_kills_by_mup[shooter_mup] + 1
                PM.STD.set_("player_kills_by_mup", player_kills_by_mup)

        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_kills_by_mup", 
                "var_val": player_kills_by_mup}, PM.Networking.get_server_time_usec())
        player_deaths_by_mup[victim_mup] = player_deaths_by_mup[victim_mup] + 1
        PM.STD.set_("player_deaths_by_mup", player_deaths_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_deaths_by_mup", 
                "var_val": player_deaths_by_mup}, PM.Networking.get_server_time_usec())

func process_event_eliminated(event_to_sort):
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.STD.get_(
                "player_laser_id"
            ):
                if not true:  # FIXME: We should play the EliminatedPlayerSound
                    # We have 4 to choose from.
                    pass
    if PM.STD.get_("is_host") >= 1:  # Server
        var laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
        var victim_mup = event_to_sort["rec_by"]
        var player_kills_by_mup = PM.STD.get_("player_kills_by_mup")
        var player_deaths_by_mup = PM.STD.get_("player_deaths_by_mup")

        var players_status_by_mup = PM.STD.get_("players_status_by_mup")
        players_status_by_mup[victim_mup] = "eliminated"
        PM.STD.set_("players_status_by_mup", players_status_by_mup)
        player_deaths_by_mup[victim_mup] = player_deaths_by_mup[victim_mup] + 1
        PM.STD.set_("player_deaths_by_mup", player_deaths_by_mup)

        #Discard accidental self-eliminations, and eliminations of fellow teammembers from stats
        if PM.STD.get_("team_amount") > 0:
            var player_team_by_mup = PM.STD.get_("players_team_num_by_mup")
            var shooter_team_number = player_team_by_mup[shooter_mup]
            var victim_team_number = player_team_by_mup[victim_mup]
            if shooter_mup != victim_mup and not shooter_team_number == victim_team_number:
                player_kills_by_mup[shooter_mup] = player_kills_by_mup[shooter_mup] + 1
                PM.STD.set_("player_kills_by_mup", player_kills_by_mup)

            #Check to see if all members of team are eliminated
            var team_is_eliminated = true
            for player in player_team_by_mup:
                if player_team_by_mup[player] == victim_team_number:
                    if players_status_by_mup[player] != "eliminated":
                        team_is_eliminated = false
            if team_is_eliminated:
                var team_num_in_elimination_order = PM.STD.get_(
                        "team_num_in_elimination_order")
                team_num_in_elimination_order.append(victim_team_number)
                PM.STD.set_("team_num_in_elimination_order", 
                        team_num_in_elimination_order)
                var team_status_by_num = PM.STD.get_("teams_status_by_team_num")
                team_status_by_num[victim_team_number] = "eliminated"
                PM.STD.set_("teams_status_by_team_num", team_status_by_num)
                #Check to see if all teams are eliminated
                var teams_remaining = 0
                for team in team_status_by_num:
                    if team_status_by_num[team] == "playing":
                        teams_remaining += 1
                if teams_remaining <= 1:
                    PM.Networking.EventSync.record_event("end_game", 
                        {"reason": "team_elimination"}, PM.Networking.get_server_time_usec())

        else: #PM.STD.get_("team_amount") == 0:
            #Discard accidental self-eliminations from stats
            if shooter_mup != victim_mup:
                player_kills_by_mup[shooter_mup] = player_kills_by_mup[shooter_mup] + 1
                PM.STD.set_("player_kills_by_mup", player_kills_by_mup)

            var number_of_players_alive = 0
            #FIXME: Fix the code below, just not sure how to yet.
            var game_teams_by_team_num_by_mup = PM.STD.get_(
                    "game_teams_by_team_num_by_mup")
            for player_mup in game_teams_by_team_num_by_mup[0]:
                if players_status_by_mup[player_mup] != "eliminated":
                    number_of_players_alive += 1
            if number_of_players_alive == 1:
                PM.Networking.EventSync.record_event("end_game", {"reason": "ffa_elimination"}, 
                    PM.Networking.get_server_time_usec())


func process_event_hit(event_to_sort): # Some player got hit.
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.STD.get_(
                "player_laser_id"
            ):
                if not NiceSound.playing:  # FIXME: NiceSound should be renamed to hit sound. 
                    # Also we have 6 sounds to choose from that we can play now.
                    # But dont play them every hit right now.
                    if PM.SS["RandomGenerator"].roll_int_die(1, 6) == 6:
                        NiceSound.play()
    if PM.STD.get_("is_host") >= 1:  # Server
        var laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
        var player_hits_by_mup = PM.STD.get_("player_hits_by_mup")
        player_hits_by_mup[shooter_mup] = player_hits_by_mup[shooter_mup] + 1
        PM.STD.set_("player_hits_by_mup", player_hits_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_hits_by_mup", 
                "var_val": player_hits_by_mup}, PM.Networking.get_server_time_usec())

func process_event_shots_update(event_to_sort): # Periodic, not continuous, update of shot count
    if PM.STD.get_("is_host") >= 1:  # Server
        var shooter_laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[shooter_laser_id]
        var player_shots_by_mup = PM.STD.get_("player_shots_by_mup")
        player_shots_by_mup[shooter_mup] = event_to_sort["additional"]["shots"]
        PM.STD.set_("player_shots_by_mup", player_shots_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_shots_by_mup", 
                "var_val": player_shots_by_mup}, PM.Networking.get_server_time_usec())

func process_event_end_game(event_to_sort):
    #update final shot count (otherwise only updated on reloads)
    if PM.STD.get_("using_weapon") and not PM.STD.get_("is_medic"):
        yield(update_shot_count_during_reload(), "completed")
    if PM.STD.get_("game_started") != 3:
        PM.SS["GameMode"].end_game(event_to_sort["additional"]["reason"])
    if PM.STD.get_("is_host") >= 1:  # Server
        pass

func process_event_start_game(event_to_sort):
    if PM.STD.get_("is_host") == 0:
        var peer_id = PM.STD.get_("mups_to_peers")[event_to_sort["rec_by"]]
        if peer_id == 1:  # This came from the server.
            if (PM.STD.get_("game_status") == 2 or 
                    PM.STD.get_("game_status") == 4):
                start_game_now()

func process_event_pause_game(event_to_sort):
    if event_to_sort["rec_by"] != PM.unique_id: # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            pass
    if PM.STD.get_("is_host") >= 1:  # Server
        pass

func edit_map_object(process, object_name, _item_name=null, _latitude=null, _longitude=null, _active=null):
    PM.Networking.EventSync.record_event("edit_map_object", \
            {"process":process, "object_name":object_name, "type":_item_name, \
            "latitude":_latitude, "longitude":_longitude, "active":_active}, PM.Networking.get_server_time_usec())

func process_event_edit_map_object(event_to_sort):
    var mup_id_and_suffix = event_to_sort["additional"]["object_name"]
    if event_to_sort["additional"]["process"] == "add":
        var latitude = event_to_sort["additional"]["latitude"]
        var longitude = event_to_sort["additional"]["longitude"]
        var added_item_name = event_to_sort["additional"]["type"]
        var active = event_to_sort["additional"]["active"]
        var map_objects = PM.STD.get_("map_objects")
        if not map_objects.has(mup_id_and_suffix):
            map_objects[mup_id_and_suffix] = {"type":added_item_name, 
                "latitude":latitude, "longitude":longitude, "active":active
            }
            PM.STD.set_("map_objects", map_objects)
    elif event_to_sort["additional"]["process"] == "remove":
        var map_objects = PM.STD.get_("map_objects")
        if map_objects.has(mup_id_and_suffix):
            map_objects.erase(mup_id_and_suffix)
            PM.STD.set_("map_objects", map_objects)

func send_comms_message(message):
    PM.Networking.EventSync.record_event("comms_message", {"message":message}, \
            PM.Networking.get_server_time_usec())

func process_event_send_comms_message(event_to_sort):
    var sender_mup_id = event_to_sort["rec_by"]
    var time = event_to_sort["time"]
    var message = event_to_sort["additional"]["message"]
    var game_player_comm_message = PM.STD.get_("game_player_comm_message")
    if PM.STD.get_("team_amount") > 0: #FIXME: Needs validation when teams added
        if sender_mup_id in PM.STD.get_("game_player_teammates"):
            game_player_comm_message = {"mup_id":sender_mup_id, "message":message, "time":time}
        PM.STD.set_("game_player_comm_message", game_player_comm_message)
    elif PM.STD.get_("game_status") == 2: #Pregame (everyone can send messages to everyone)
        game_player_comm_message = {"mup_id":sender_mup_id, "message":message, "time":time}
        PM.STD.set_("game_player_comm_message", game_player_comm_message)

func process_event_award_credits(event_to_sort):
    var recipient_laser_id = event_to_sort["additional"]["laser_id"]
    var recipient_mup_id = PM.STD.get_("player_mup_by_laser_id")[recipient_laser_id]
    var mup_id = PM.unique_id
    if mup_id == recipient_mup_id:
        var time = event_to_sort["time"]
        var awarded_credits = event_to_sort["additional"]["credits"]
        PM.STD.set_("game_player_awarded_credits", {"credits":awarded_credits,"time":time})

func process_event_client_request(event_to_sort):
    if PM.STD.get_("is_host") >= 1:
        #PM.Log("process_event_client_request() " + str(event_to_sort), "testing")
        var method = event_to_sort["additional"]["method"]
        var mup = event_to_sort["rec_by"]
        if event_to_sort["additional"].has("args"):
            var arguments = event_to_sort["additional"]["args"]
            call(method, mup, arguments)
        else:
            call(method, mup)

func process_event_server_request(event_to_sort):
    if PM.STD.get_("is_host") == 0:  # Not a host.
        #PM.Log("process_event_server_request() " + str(event_to_sort), "testing")
        var mup = event_to_sort["additional"]["mup"]
        var apply_the_request = false
        if mup == "all":
            apply_the_request = true
        elif mup == PM.unique_id:
            apply_the_request = true
        if apply_the_request:
            var method = event_to_sort["additional"]["method"]
            if event_to_sort["additional"].has("args"):
                var arguments = event_to_sort["additional"]["args"]
                call(method, arguments)
            else:
                call(method)

func connection_status_event(new_status):
    if last_connection_status != new_status:
        last_connection_status = new_status
        PM.Networking.EventSync.record_event("connection", {"status": new_status}, 
            PM.Networking.get_server_time_usec()
        )

func change_weapon(wpn_name=null):
    yield(PM.SS["FreecoiLInterface"].reload_start(), "completed")
    yield(collect_current_weapon_ammo(), "completed")
    #Temporarily reload weapon with 0-shots to keep from disrupting ammo counts on new weapon
    yield(PM.SS["FreecoiLInterface"].reload_finish(0, 
        PM.STD.get_("player_laser_id")), "completed"
    )
    var long_power
    var short_power
    var player_wpns = PM.STD.get_("game_player_weapons")
    var weapon_type = PM.STD.get_("game_weapon_type")
    if wpn_name == "First Aid Kit" or wpn_name == "Health Kit": #Medic heals
        PM.STD.set_("game_weapon_type", "First Aid")
        PM.STD.set_("game_weapon_name", wpn_name)
        var heal_type
        if wpn_name == "First Aid Kit":
            heal_type = 1
        elif wpn_name == "Health Kit":
            heal_type = 2
        PM.STD.set_("game_weapon_damage", heal_type)
        PM.STD.set_("game_weapon_shot_modes", ["single"])
        PM.STD.set_("game_weapon_shot_mode", "single")
        PM.STD.set_("game_weapon_magazine_size", 4)
        PM.STD.set_("game_weapon_magazine_ammo", 0)
        PM.STD.set_("game_weapon_total_ammo", 4)
        PM.STD.set_("game_weapon_reload_speed", 2)
        PM.STD.set_("game_weapon_rate_of_fire", 1)
        if PM.STD.get_("indoor_game") == true:
            long_power = 15
            short_power = 0
        else: #Outdoor game
            long_power = 0
            short_power = 30
        PM.STD.set_("game_weapon_long_power", long_power)
        PM.STD.set_("game_weapon_short_power", short_power)
    else: #Soldier, change weapons
        if wpn_name == null:
            var counter = 0
            for wpn in player_wpns:  # find the index of the weapon.
                if wpn == weapon_type:
                    break
                else:
                    counter += 1
            counter += 1
            if counter == player_wpns.size():
                counter = 0
            weapon_type = player_wpns[counter]
        else:
            weapon_type = wpn_name
        if player_wpns.has(weapon_type):
            yield(update_shot_count_during_reload(), "completed")
            shot_count_begin = null #prevent subsequent shot count updates till after change_weapon is done
            PM.STD.set_("game_weapon_type", weapon_type)
            PM.STD.set_("game_weapon_name",
                PM.STD.get_("game_weapon_types")[weapon_type]["name"])
            PM.STD.set_("game_weapon_damage", 
                PM.STD.get_("game_weapon_types")[weapon_type]["damage"])
            PM.STD.set_("game_weapon_shot_modes", 
                PM.STD.get_("game_weapon_types")[weapon_type]["shot_modes"])
            PM.STD.set_("game_weapon_shot_mode", 
                PM.STD.get_("game_weapon_shot_modes")[0])
            PM.STD.set_("game_weapon_magazine_size", 
                PM.STD.get_("game_weapon_types")[weapon_type]["magazine_size"])
            PM.STD.set_("game_weapon_magazine_ammo", 0)
            PM.STD.set_("game_weapon_total_ammo", 
                PM.STD.get_("game_player_ammo")[weapon_type])
            PM.STD.set_("game_weapon_reload_speed", 
                PM.STD.get_("game_weapon_types")[weapon_type]["reload_speed"])
            PM.STD.set_("game_weapon_rate_of_fire", 
                PM.STD.get_("game_weapon_types")[weapon_type]["rate_of_fire"])
            PM.STD.set_("game_weapon_short_power", 
                PM.STD.get_("game_weapon_types")[weapon_type]["short_power"])
            PM.STD.set_("game_weapon_long_power", 
                PM.STD.get_("game_weapon_types")[weapon_type]["long_power"])
            if PM.STD.get_("indoor_game") == true:
                long_power = PM.STD.get_("game_weapon_indoor_long_power")
                short_power = 0
            else: #Outdoor game
                long_power = PM.STD.get_("game_weapon_long_power")
                short_power = PM.STD.get_("game_weapon_short_power")
    reload_start()

func reload_start():
    yield(update_shot_count_during_reload(), "completed")
    yield(PM.SS["FreecoiLInterface"].reload_start(), "completed")
    ReloadTimer.wait_time = PM.STD.get_("game_weapon_reload_speed")
    ReloadTimer.start()
    _update_reload_progressbar()
    yield(collect_current_weapon_ammo(), "completed")
    ReloadSound.volume_db = 0
    ReloadSound.pitch_scale = 4.0865*pow(PM.STD.get_("game_weapon_reload_speed"), -1.0207)
    #For the ReloadSound file (nominal length 4 sec), the pitch scale adjustment 
    # only seems reasonable at approx 3-6 seconds
    #AudioEffectPitchShift may work to offset sound distortion (= 1/pitch_scale)
    ReloadSound.play()
    PM.Networking.EventSync.record_event("reloading", 
        {"gun": PM.STD.get_("game_weapon_type"), 
        "reload_speed": PM.STD.get_("game_weapon_reload_speed")}, 
        PM.Networking.get_server_time_usec())

func reload_finish():
    var remove_magazine_ammo = 0
    if PM.STD.get_("game_weapon_total_ammo") > PM.STD.get_(
                "game_weapon_magazine_size"):
        PM.STD.set_("game_weapon_magazine_ammo", PM.STD.get_(
                "game_weapon_magazine_size"))
        remove_magazine_ammo = (PM.STD.get_("game_weapon_total_ammo") - 
            PM.STD.get_("game_weapon_magazine_size"))
    elif PM.STD.get_("game_weapon_total_ammo") == 0:
        PM.STD.set_("game_weapon_magazine_ammo", 0)
    else:
        PM.STD.set_("game_weapon_magazine_ammo", PM.STD.get_(
                "game_weapon_total_ammo"))
    PM.STD.set_("game_weapon_total_ammo", remove_magazine_ammo)
    var game_weapon_shot_mode = PM.STD.get_("game_weapon_shot_mode")
    var long_power = PM.STD.get_("game_weapon_long_power")
    var short_power = PM.STD.get_("game_weapon_short_power")
    var game_weapon_damage = PM.STD.get_("game_weapon_damage")
    var game_weapon_magazine_ammo = PM.STD.get_("game_weapon_magazine_ammo")
    var player_laser_id = PM.STD.get_("player_laser_id")
    yield(PM.SS["FreecoiLInterface"].new_set_shot_mode(game_weapon_shot_mode, 
        long_power, short_power, game_weapon_damage),  "completed"
    )
    yield(PM.SS["FreecoiLInterface"].reload_finish(game_weapon_magazine_ammo, 
        player_laser_id, game_weapon_damage),  "completed"
    )
    shot_count_begin = PM.STD.get_("game_weapon_magazine_ammo") #Used to count shots
    PM.Log("reload_finish() completed!")

func collect_current_weapon_ammo():
    yield(get_tree(), "idle_frame")
    if not PM.STD.get_("is_medic"):
        var collect_magazine_ammo = (PM.STD.get_("game_weapon_magazine_ammo") + 
            PM.STD.get_("game_weapon_total_ammo"))
        #Update game_player_ammo
        var game_player_ammo = PM.STD.get_("game_player_ammo")
        game_player_ammo[PM.STD.get_("game_weapon_type")] = collect_magazine_ammo
        PM.STD.set_("game_player_ammo", game_player_ammo)
        PM.STD.set_("game_weapon_total_ammo", collect_magazine_ammo)
        PM.STD.set_("game_weapon_magazine_ammo", 0)

func _update_reload_progressbar():
    var progressbar_value = 0
    PM.STD.set_("reload_progress", progressbar_value)
    while ReloadTimer.time_left > 0:
        yield(get_tree().create_timer(0.1), "timeout")
        progressbar_value = 100*(1 - (ReloadTimer.time_left/ReloadTimer.wait_time))
        PM.STD.set_("reload_progress", progressbar_value)
    PM.STD.set_("reload_progress", 100)

func update_shot_count_during_reload():
    yield(get_tree(), "idle_frame")
    if shot_count_begin != null and not PM.STD.get_("is_medic"): #Count shots made
        shot_count_end = PM.STD.get_("game_weapon_magazine_ammo")
        var shots_made = shot_count_begin - shot_count_end
        PM.STD.set_("game_player_shots", \
                PM.STD.get_("game_player_shots") + shots_made)
        var player_laser_id = PM.STD.get_("player_laser_id")
        var shots = PM.STD.get_("game_player_shots")
        PM.Networking.EventSync.record_event("shots_update", {"laser_id": player_laser_id, "shots":shots},
                PM.Networking.get_server_time_usec())

func eliminated(laser_id):
    if PM.STD.get_("game_started") == 1:
        if PM.STD.get_("supply_depot_active"):
            CombatPayTimer.stop()
        PM.SS["FreecoiLInterface"].reload_start()
        PM.STD.set_("game_player_alive", false)
        PM.Networking.EventSync.record_event("eliminated", {"laser_id": laser_id},
            PM.Networking.get_server_time_usec()
        )
        if laser_id != 0:
            var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
#            var shooter_name = PM.STD.get_("player_name_by_mup")[shooter_mup]
            PM.STD.set_("game_player_last_killed_by", shooter_mup)
        else:
            PM.STD.set_("game_player_last_killed_by", "UNKNOWN")
        PM.STD.set_("game_player_deaths", PM.STD.get_(
                "game_player_deaths") + 1)
        get_tree().call_group("Container", "next_menu", "2,0")
        #Add player shots to sync variable
        var player_laser_id = PM.STD.get_("player_laser_id_by_mup")[PM.unique_id]
        var shots = PM.STD.get_("game_player_shots")
        PM.Networking.EventSync.record_event("shots_update", {"laser_id": player_laser_id, "shots":shots},
            PM.Networking.get_server_time_usec())

func respawn_start(laser_id):
    if PM.STD.get_("game_started") == 1:
        PM.SS["FreecoiLInterface"].reload_start()
        PM.STD.set_("game_player_alive", false)
        if PM.STD.get_("supply_depot_active"):
            CombatPayTimer.set_paused(true)
        if PM.STD.get_("respawn_type") == "Timed":
            if PM.STD.get_("respawn_delay") > 0:
                if not TickTocTimer.is_connected("timeout", self, "update_respawn_delay_panel"):
                    TickTocTimer.connect("timeout", self, "update_respawn_delay_panel")
                RespawnTimer.start()
                update_respawn_delay_panel()
            RespawnTimedSound.volume_db = 0
            RespawnTimedSound.play()
        else: 
            #respawn_type == "Location" and location has been reached because 
            #it should be a condition before respawn_finish()
            RespawnZoneSound.volume_db = 0
            RespawnZoneSound.play()
        if laser_id != 0:
            var shooter_mup = PM.STD.get_("player_mup_by_laser_id")[laser_id]
#            var shooter_name = PM.STD.get_("player_name_by_mup")[shooter_mup]
            PM.STD.set_("game_player_last_killed_by", shooter_mup)
        else:
            PM.STD.set_("game_player_last_killed_by", "UNKNOWN")
        PM.STD.set_("game_player_deaths", PM.STD.get_(
                "game_player_deaths") + 1)
        call_deferred("instant_move_panel_in", RespawnPanel)
        PM.Networking.EventSync.record_event("died", {"laser_id": laser_id},
            PM.Networking.get_server_time_usec()
        )
        #Add player shots to sync variable
        var player_laser_id = PM.STD.get_("player_laser_id_by_mup")[PM.unique_id]
        var shots = PM.STD.get_("game_player_shots")
        PM.Networking.EventSync.record_event("shots_update", {"laser_id": player_laser_id, "shots":shots},
            PM.Networking.get_server_time_usec())
    
func respawn_finish():
    if PM.STD.get_("game_started") == 1:
        yield(set_player_respawn_vars(),  "completed")
        PM.STD.set_("game_player_alive", true)
        PM.Networking.EventSync.record_event("alive", {}, 
            PM.Networking.get_server_time_usec()
        )
        reload_finish()
        if PM.STD.get_("supply_depot_active"):
            CombatPayTimer.set_paused(false)
        instant_move_panel_out(RespawnPanel)

func notify_game_state_change(mup, args):
    if PM.STD.get_("is_host") >= 1:
        var new_value = args[0]
        PM.SS["GameCommon"].game_state_by_mup[mup] = new_value
#        PM.Log("notify_game_state_change( " + str(new_value) + " ) for mup " 
#                + str(mup) + "  " + str(PM.SS["GameCommon"].game_state_by_mup), "testing")
        if new_value >= 2:
            var mups_reconnected = PM.STD.get_("mups_reconnected")
            mups_reconnected.erase(mup)
            PM.STD.set_("mups_reconnected", mups_reconnected, false, false)
        if not PM.STD.get_("player_kills_by_mup").has(mup):
            update_ingame_with_new_player(mup)
        PM.SS["GameMode"].call_deferred("check_if_enough_ready_to_start", mup)

func update_ingame_with_new_player(mup):
    var player_shots_by_mup = PM.STD.get_("player_shots_by_mup")
    var player_hits_by_mup = PM.STD.get_("player_hits_by_mup")
    var player_kills_by_mup = PM.STD.get_("player_kills_by_mup")
    var player_deaths_by_mup = PM.STD.get_("player_deaths_by_mup")
    var player_medics_by_mup = PM.STD.get_("player_medics_by_mup")
    player_shots_by_mup[mup] = 0
    player_hits_by_mup[mup] = 0
    player_kills_by_mup[mup] = 0
    player_deaths_by_mup[mup] = 0
    PM.STD.set_("player_shots_by_mup", player_shots_by_mup)
    PM.STD.set_("player_hits_by_mup", player_hits_by_mup)
    PM.STD.set_("player_kills_by_mup", player_kills_by_mup)
    PM.STD.set_("player_deaths_by_mup", player_deaths_by_mup)
    PM.STD.set_("player_medics_by_mup", player_medics_by_mup)
    PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_shots_by_mup", 
        "var_val": player_shots_by_mup}, PM.Networking.get_server_time_usec()
    )
    PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_hits_by_mup", 
        "var_val": player_hits_by_mup}, PM.Networking.get_server_time_usec()
    )
    PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_kills_by_mup", 
        "var_val": player_kills_by_mup}, PM.Networking.get_server_time_usec()
    )
    PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_deaths_by_mup", 
        "var_val": player_deaths_by_mup}, PM.Networking.get_server_time_usec()
    )

func tell_server_my_picture_path() :
    while PM.LTD.get_("player_avatar") == null:
        yield(get_tree(), "idle_frame")
    if PM.STD.get_("is_host") >= 1:
        remote_tell_server_my_picture_path(PM.unique_id, 
            [PM.LTD.get_("player_avatar")]
        )
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "remote_tell_server_my_picture_path", "object": "GameCommon", 
            "args": [PM.LTD.get_("player_avatar")]}, 
            PM.Networking.get_server_time_usec()
        )
   
func remote_tell_server_my_picture_path(mup, args):
    if PM.STD.get_("is_host") >= 1:
        var new_path = args[0]
        var player_avatar_by_mup = PM.STD.get_("player_avatar_by_mup")
        player_avatar_by_mup[mup] = new_path
        PM.STD.set_("player_avatar_by_mup", player_avatar_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_avatar_by_mup", 
                "var_val": player_avatar_by_mup}, PM.Networking.get_server_time_usec())
         
func tell_server_my_name():
    while PM.LTD.get_("player_name") == null:
        yield(get_tree(), "idle_frame")
    if PM.STD.get_("is_host") >= 1:
        remote_tell_server_my_name(PM.unique_id, [PM.LTD.get_("player_name")])
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "remote_tell_server_my_name", "object": "GameCommon", 
            "args": [PM.LTD.get_("player_name")]}, 
            PM.Networking.get_server_time_usec()
        )

func remote_tell_server_my_name(mup, args):
    if PM.STD.get_("is_host") >= 1:
        var new_name = args[0]
        var player_names_by_mup = PM.STD.get_("player_name_by_mup")
        player_names_by_mup[mup] = new_name
        PM.STD.set_("player_name_by_mup", player_names_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_name_by_mup", 
                "var_val": player_names_by_mup}, PM.Networking.get_server_time_usec())

func tell_server_my_medic_status():
    while PM.STD.get_("is_medic") == null:
        yield(get_tree(), "idle_frame")
    if PM.STD.get_("is_host") >= 1:
        remote_tell_server_my_medic_status(PM.unique_id, [PM.STD.get_("is_medic")])
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "remote_tell_server_my_medic_status", "object": "GameCommon", 
            "args": [PM.STD.get_("is_medic")]}, 
            PM.Networking.get_server_time_usec()
        )

func remote_tell_server_my_medic_status(mup, args):
    if PM.STD.get_("is_host") >= 1:
        var new_medic_status = args[0]
        var player_medic_status_by_mup = PM.STD.get_("player_medics_by_mup")
        player_medic_status_by_mup[mup] = new_medic_status
        PM.STD.set_("player_medics_by_mup", player_medic_status_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_medics_by_mup", 
                "var_val": player_medic_status_by_mup}, PM.Networking.get_server_time_usec())

func get_next_available_player_laser_id(mup):
    if PM.STD.get_("is_host") >= 1:
        next_available_player_laser_id += 1
        if next_available_player_laser_id > 65:  # Error too many players.
            pass  # FIXME: Maybe one day, not likley to ever happen though.
        var player_laser_id_by_mup = PM.STD.get_("player_laser_id_by_mup")
        player_laser_id_by_mup[mup] = next_available_player_laser_id - 1
        PM.STD.set_("player_laser_id_by_mup", player_laser_id_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_laser_id_by_mup", 
                "var_val": player_laser_id_by_mup}, PM.Networking.get_server_time_usec())
        invert_mups_to_lasers(PM.STD.get_("player_laser_id_by_mup"))
        if mup == PM.unique_id:
            remote_set_player_laser_id([next_available_player_laser_id - 1])
        else:
            PM.Networking.EventSync.record_event("server_request", {"mup": mup, 
                "method": "remote_set_player_laser_id", "args": 
                [next_available_player_laser_id - 1], "object": "GameCommon"}, 
                PM.Networking.get_server_time_usec()
            )

func start_game_start_delay():
    if PM.STD.get_("is_host") >= 1:
        yield(get_tree().create_timer(0.01), "timeout")  # Just to let the network settle out.
        game_start_time = (OS.get_ticks_usec() + (
                PM.STD.get_("start_game_delay")) * 1000000)
        PM.Networking.EventSync.record_event("server_request", {"mup":"all", "method": 
            "remote_start_game_start_delay", "args": [game_start_time], 
            "object": "GameCommon"}, PM.Networking.get_server_time_usec()
        )
        PM.STD.set_("game_status", 1)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status", 
            "var_val": 1}, PM.Networking.get_server_time_usec()
        )
        if PM.STD.get_("is_host_pregame_ready"):
            remote_start_game_start_delay([game_start_time])
    
func remote_start_game_start_delay(args):
    var updated_status_while_waiting = false
    var EmptyNode = PM.get_tree().root.get_node("Container/Scene1/Empty/")
    while game_state < 2:
        if not updated_status_while_waiting:
            if EmptyNode.has_node("WaitingPanel"):
                updated_status_while_waiting = true
                var Label1 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                    "WaitingPanel/CenterContainer/VBoxContainer/Label"
                )
                var Label2 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                    "WaitingPanel/CenterContainer/VBoxContainer/Label2"
                )
                Label1.text = "Your device is still syncing..."
                Label2.text = "Please be patient..."
        yield(get_tree(), "idle_frame")
    var start_at_time = args[0]
    PM.STD.set_("game_start_time", start_at_time)
    EventRecordTimer.start()
    TickTocTimer.connect("timeout", self, "tick_toc_continue")
    TickTocTimer.start()
    var wait_time_secs = (float(start_at_time) - 
            float(PM.Networking.get_server_time_usec())) / 1000000.0
    if wait_time_secs <= 0.0:
        wait_time_secs = 0.001
    StartGameTimer.wait_time = wait_time_secs
    StartGameTimer.connect("timeout", self, "start_the_game")
    StartGameTimer.start()
    if PM.STD.get_("is_host") == 0 or PM.STD.get_("is_host_pregame_ready"):
        TickTocTimer.connect("timeout", self, "update_start_delay_panel")
        update_start_delay_panel()
        instant_move_panel_out(WaitingPanel)
        instant_move_panel_in(StartPanel)
        PM.SS["GameMode"].remote_start_game_start_delay_hook()

func start_the_game():
    PM.STD.set_("game_started", 1)
    game_state = 3
    if PM.STD.get_("is_host") >= 1:
        notify_game_state_change(PM.unique_id, [game_state])
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "notify_game_state_change", "object": "GameCommon", 
            "args": [game_state]}, PM.Networking.get_server_time_usec()
        )
    TimeRemainingTimer.start()
    if PM.STD.get_("supply_depot_active"):
        CombatPayTimer.wait_time = 30
        CombatPayTimer.connect("timeout", self, "assess_accomplishment", ["combat_pay", null])
        CombatPayTimer.start()
    if PM.STD.get_("is_host") == 0 or PM.STD.get_("is_host_pregame_ready"):
        instant_move_panel_out(StartPanel)
        respawn_finish()
    if PM.STD.get_("is_host") >= 1:  # is a host
        PM.STD.set_("game_status", 2)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status",
             "var_val": 2}, PM.Networking.get_server_time_usec()
        )
    PM.Networking.EventSync.record_event("start_game", {}, PM.Networking.get_server_time_usec())  

func start_game_now():
    EventRecordTimer.start()
    TickTocTimer.connect("timeout", self, "tick_toc_continue")
    TickTocTimer.start()
    PM.STD.set_("game_status", 2)
    TimeRemainingTimer.start()
    if PM.STD.get_("is_host") == 0 or PM.STD.get_("is_host_pregame_ready"):
        TickTocTimer.connect("timeout", self, "update_start_delay_panel")
        get_tree().call_group("Container", "next_menu", "0,0")
        instant_move_panel_out(WaitingPanel)
        PM.SS["GameMode"].remote_start_game_start_delay_hook()
        instant_move_panel_out(StartPanel)
        get_tree().call_group("Container", "next_menu", "0,0")
        respawn_finish()
    PM.Networking.EventSync.record_event("start_game", {}, PM.Networking.get_server_time_usec())

func in_game_added_to_tree():
    var execute_in_game_added_to_tree = false
    if PM.STD.get_("is_host") == 0:
        execute_in_game_added_to_tree = true
    else:
        if PM.STD.get_("is_host_pregame_ready"):
            execute_in_game_added_to_tree = true
    if execute_in_game_added_to_tree:
        LPanel = get_tree().root.get_node("Container").current_scene.get_node("LPanel")
        RPanel = get_tree().root.get_node("Container").current_scene.get_node("RPanel")
        Footer = get_tree().root.get_node("Container").current_scene.get_node("Footer")
        SupplyDepot = get_tree().root.get_node("Container").current_scene.get_node("SupplyDepot")
        StartPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("StartGameDelayPanel")
        WaitingPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("WaitingPanel")
        RespawnPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("RespawnPanel")
        call_deferred("move_Panel_in", LPanel)
        call_deferred("move_Panel_in", RPanel)
        call_deferred("move_Panel_in", Footer)
        call_deferred("instant_move_panel_in", WaitingPanel)
        # Show Host End Game Button.
        var EndGameContainer = PM.get_tree().root.get_node("Container/" +
            "UI/StandardHeader/MenuBackground/GUISettingsButtons/VBoxContainer"
        )
        EndGameContainer.show()
        all_in_game_ui_added_to_tree = true
        if PM.STD.get_("is_host_pregame_ready"):
            if PM.STD.get_("is_host") == 1:
                game_state = 2
                notify_game_state_change(PM.unique_id, [game_state])
            else:
                game_state = 1
                notify_game_state_change(PM.unique_id, [game_state])
        else:
            game_state = 1
            PM.Networking.EventSync.record_event("client_request", 
                {"method": "notify_game_state_change", "object": "GameCommon",
                "args": [game_state]}, PM.Networking.get_server_time_usec()
            )
        yield(get_tree().create_timer(0.01), "timeout")
        in_game_added_to_tree_complete = true
        

func instant_move_panel_in(Panel_ref):
    Panel_ref.rect_position.x = 0
    Panel_ref.rect_position.y = 0
       
func move_Panel_in(Panel_ref):
    var continue_motion = true
    var add_to_move_x = true
    var add_to_move_y = false
    if Panel_ref.rect_position.x > 1:
        add_to_move_x = false
    if Panel_ref.rect_position.y > 1:
        add_to_move_y = false
    while continue_motion:
        continue_motion = false
        if add_to_move_x:
            if Panel_ref.rect_position.x < 0:
                Panel_ref.rect_position.x += 10
                continue_motion = true
            else:
                Panel_ref.rect_position.x = 0
        else:
            if Panel_ref.rect_position.x > 0:
                Panel_ref.rect_position.x -= 10
                continue_motion = true
            else:
                Panel_ref.rect_position.x = 0
        if add_to_move_y:
            if Panel_ref.rect_position.y < 0:
                Panel_ref.rect_position.y += 10
                continue_motion = true
            else:
                Panel_ref.rect_position.y = 0
        else:
            if Panel_ref.rect_position.y > 0:
                Panel_ref.rect_position.y -= 10
                continue_motion = true
            else:
                Panel_ref.rect_position.y = 0
        yield(get_tree(), "idle_frame")

func instant_move_panel_out(Panel_ref):
    Panel_ref.rect_position.x = 561
    Panel_ref.rect_position.y = 961
   
func move_panel_out(Panel_ref):
    var continue_motion = true
    while continue_motion:
        continue_motion = false
        if Panel_ref.rect_position.x < 561:
            Panel_ref.rect_position.x += 10
            continue_motion = true
        else:
            Panel_ref.rect_position.x = 561
        if Panel_ref.rect_position.y < 961:
            Panel_ref.rect_position.y += 10
            continue_motion = true
        else:
            Panel_ref.rect_position.y = 961
        yield(get_tree(), "idle_frame")

func tick_toc_continue():
    TickTocTimer.start()
    
func update_start_delay_panel():
    if StartGameTimer.time_left != 0.0:
        var time_minutes = int(StartGameTimer.time_left / 60)
        var time_secs = "%02d" % (StartGameTimer.time_left - time_minutes * 60)
        time_minutes = "%02d" % time_minutes
        var time_text = str(time_minutes) + ":" + str(time_secs)
        StartPanel.get_node("CenterContainer/VBoxContainer/Label2").text = time_text
    else:
        TickTocTimer.disconnect("timeout", self, "update_start_delay_panel")
        
func update_respawn_delay_panel():
    if RespawnTimer.time_left != 0.0:
        TickTocTimer.start()
        var time_minutes = int(RespawnTimer.time_left / 60)
        var time_secs = "%02d" % (RespawnTimer.time_left - time_minutes * 60)
        time_minutes = "%02d" % time_minutes
        var time_text = str(time_minutes) + ":" + str(time_secs)
        RespawnPanel.get_node("CenterContainer/VBoxContainer/RespawnTime").text = time_text
    else:
        RespawnPanel.get_node("CenterContainer/VBoxContainer/RespawnTime").text = "0.00"
        TickTocTimer.disconnect("timeout", self, "update_respawn_delay_panel")

remote func remote_set_player_laser_id(args):
    var new_id = args[0]
    PM.Log("Got Laser ID of " + str(new_id))
    PM.STD.set_("player_laser_id", new_id)
    PM.SS["FreecoiLInterface"].set_laser_id(PM.STD.get_("player_laser_id"))

func delayed_vibrate():
    PM.SS["FreecoiLInterface"].vibrate(200)

func now_entering_game_state_2():
    print("Here")
    if not PM.STD.get_("is_observer"):
        if PM.STD.get_("is_host") >= 1:
            if PM.STD.get_("is_host_pregame_ready"):
                var Label1 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                    "WaitingPanel/CenterContainer/VBoxContainer/Label"
                )
                var Label2 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                    "WaitingPanel/CenterContainer/VBoxContainer/Label2"
                )
                Label1.text = "Your device is still syncing..."
                Label2.text = "Please be patient..."
        else:
            var Label1 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                "WaitingPanel/CenterContainer/VBoxContainer/Label"
            )
            var Label2 = PM.get_tree().root.get_node("Container/Scene1/Empty/" +
                "WaitingPanel/CenterContainer/VBoxContainer/Label2"
            )
            Label1.text = "Your device is still syncing..."
            Label2.text = "Please be patient..."
        while set_player_start_game_vars_comp == false:
            yield(get_tree(), "idle_frame")
        while PM.STD.get_("player_laser_id") == null:
            yield(get_tree(), "idle_frame")
        while not PM.STD.get_("player_mup_by_laser_id").has(
            PM.STD.get_("player_laser_id")
        ):
            yield(get_tree(), "idle_frame")
    while not PM.STD.get_("player_name_by_mup").has(
        PM.unique_id
    ):
        yield(get_tree(), "idle_frame")
    while not PM.STD.get_("player_avatar_by_mup").has(
        PM.unique_id
    ):
        yield(get_tree(), "idle_frame")
    while PM.STD.get_("supply_depot") == null:
        yield(get_tree(), "idle_frame")
    while not in_game_added_to_tree_complete:
        yield(get_tree(), "idle_frame")
    if not PM.STD.get_("is_observer"):
        while not PM.Networking.server_time_deviation_is_acceptable():
            yield(get_tree(), "idle_frame")
        var high_fps_count = 0
        while high_fps_count < 120:  # This forces the fps to smooth out, before we call a start to the game.
            if Engine.get_frames_per_second() > 40:
                high_fps_count += 1
            else:
                high_fps_count -= 1
            yield(get_tree(), "idle_frame")
        game_state = 2
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "notify_game_state_change", "object": "GameCommon",
            "args": [game_state]}, PM.Networking.get_server_time_usec()
        )
        if PM.STD.get_("is_host") >= 1:
            if PM.STD.get_("is_host_pregame_ready"):
                remote_start_game_start_delay([game_start_time])

func game_status_update(new_status):
    if new_status == 5 or new_status == 6 or new_status == 12 or new_status == 13:
        PM.Networking.EventSync.record_event("end_game", 
            {"reason": "host_terminated"}, PM.Networking.get_server_time_usec())

func assess_accomplishment(accomplishment, laser_id):
    if PM.STD.get_("supply_depot_active"):
        PM.Log("assess accomplishments type = " + str(accomplishment), "debug")
        var credits
        #ACCOMPLISHMENTS PROCESSED BY CLIENTS
        #1.1 Credit Drops - To be processed in separate func
        if accomplishment == "kill": #1.2 Eliminating Other Players
            credits = PM.STD.get_("game_player_credits")
            credits += 25
            PM.STD.set_("game_player_credits", credits)
        elif accomplishment == "killed_while_on_killing_spree": #1.2 Eliminate Other Players (High Profile Player)
            PM.Networking.EventSync.record_event("award_credits", {"laser_id":laser_id, "credits":50},
                    PM.Networking.get_server_time_usec())
        #FIXME: Add "If killed player was in single-player contract, then add 75" - #1.3 (Item 1) Contract
        #FIXME: Add "If killed player was in three-player contract, then remove from list - #1.3 (Item 2) Contract
        #       and If list is now empty then add 150 credits"
        #1.3 (Item 3) TBD
        elif accomplishment == "combat_pay" and PM.STD.get_("game_player_alive"): #1.4 Combat Pay
            credits = PM.STD.get_("game_player_credits")
            credits += 10
            PM.STD.set_("game_player_credits", credits)
            CombatPayTimer.start()
        elif accomplishment == "healed_first_aid_kit":
            var supply_depot = PM.STD.get_("supply_depot")
            var reward = supply_depot["Shop5"]["Item1"]["cost"] + 25
            PM.Networking.EventSync.record_event("award_credits", {"laser_id":laser_id, "credits":reward},
                    PM.Networking.get_server_time_usec())
        elif accomplishment == "healed_health_kit":
            var supply_depot = PM.STD.get_("supply_depot")
            var reward = supply_depot["Shop5"]["Item2"]["cost"] + 25
            PM.Networking.EventSync.record_event("award_credits", {"laser_id":laser_id, "credits":reward},
                    PM.Networking.get_server_time_usec())
        #ACCOMPLISHMENTS PROCESSED BY SERVER

func fix_looping_audio_to_not():
    if ReloadSound != null:
        ReloadSound.stream.loop = false
    if EmptyShotSound != null:
        EmptyShotSound.stream.loop = false
    #NiceSound.stream.loop = false
    if GunShotSound != null:
        GunShotSound.stream.loop = false
    if RespawnTimedSound != null:
        RespawnTimedSound.stream.loop = false
    if RespawnZoneSound != null:
        RespawnZoneSound.stream.loop = false
    #TangoDownSound.stream.loop = false

func test_send_to_client():
    PM.Networking.EventSync.record_event("test_client", 
        {"test": "client_got_this_string"}, PM.Networking.get_server_time_usec()
    )
    
func test_send_to_server():
    PM.Networking.EventSync.record_event("test_server", 
        {"test": "server_got_this_string"}, PM.Networking.get_server_time_usec()
    )

func process_event_test_client(event):
    if PM.STD.get_("is_host") == 0:
        if event["additional"]["test"] == "client_got_this_string":
            print("Test Client Recieve worked. " + str(event["time"]))

func process_event_test_server(event):
    if PM.STD.get_("is_host") >= 1:
        if event["additional"]["test"] == "server_got_this_string":
            print("Test server Recieve worked. " + str(event["time"]))

func end_game():
    if PM.STD.get_("is_host") >= 1:  # is a host
        var val = PM.STD.get_("game_status")
        if val == 0 or val == 1 or val ==2 or val == 2 or val == 3 or val == 4:
            PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status",
                "var_val": 5}, PM.Networking.get_server_time_usec()
                )
            PM.STD.set_("game_status", 5)
        elif val == 7 or val == 8 or val == 9 or val == 10 or val == 11:
            PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status",
                "var_val": 12}, PM.Networking.get_server_time_usec()
                )
            PM.STD.set_("game_status", 12)

func the_host_has_left_the_game():
    get_tree().call_group("ToolTip", "show_tooltip", "The Host has left game.")

func host_tx_to_clients_to_reset_for_next_game_mode():
    reset_status = "Telling clients to begin reset."
    var all_mups = PM.STD.get_("mups_to_peers").keys()
    for mup in all_mups:
        if mup != PM.unique_id:  # != self
            var client_rpc_id = PM.STD.get_("mups_to_peers")[mup]
            mups_that_have_ack_reset[mup] =  false
            rpc_id(client_rpc_id, "client_rx_reset_the_game_for_next_game_mode")
    call_deferred("re_transimit_clients_to_reset_for_next_game_mode")
    reset_the_game_for_next_game_mode()

remote func client_rx_reset_the_game_for_next_game_mode():
    notify_host_that_reset_is_in_progress()
    if not reset_in_progress:
        reset_in_progress = true
        call_deferred("reset_the_game_for_next_game_mode")

func reset_the_game_for_next_game_mode():
    reset_status = ("Waiting for all clients to complete the reset. " +
        "Make sure they are in network range."
    )
    PM.Networking.EventSync.pause_sync_var = true
    if PM.Networking.EventSync.is_a_client:
        # Client Reset
        PM.Networking.EventSync.client_unacknowledged_events.clear()
        PM.Networking.EventSync.client_unacknowledged_events_by_id.clear()
        PM.Networking.EventSync.client_unprocessed_events_history.clear()
        PM.Networking.EventSync.client_unprocessed_events_history_by_id.clear()
    else:
        # Server Reset
        PM.Networking.EventSync.server_unprocessed_events.clear()
        PM.Networking.EventSync.server_unprocessed_events_by_id.clear()
        PM.Networking.EventSync.server_unacknowledged_events_by_mup.clear()
        PM.Networking.EventSync.server_unacknowledged_events_by_mup_by_id.clear()
        PM.Networking.EventSync.server_untransmitted_events_history.clear()
        PM.Networking.EventSync.server_untransmitted_events_history_by_id.clear()
    # Common Reset
    PM.Networking.EventSync.events_unsent.clear()
    PM.Networking.EventSync.events_history.clear()
    PM.Networking.EventSync.events_history_by_id.clear()
    PM.Networking.EventSync.event_highest_num_by_mup.clear()
    PM.Networking.EventSync.event_counter = 0
    yield(get_tree(), "idle_frame")
    if PM.Networking.EventSync.is_a_client:
        # Client Reset
        PM.Networking.EventSync.pause_sync_var = false
        reset_in_progress = false
    else:  # Host
        while (PM.STD.get_("peers_to_mups").size() - 1 != 
            mups_that_have_completed_reset.size()
        ):
            yield(get_tree(), "idle_frame")
        reset_status = ("All clients have completed reset, initiating the " +
            "next game mode."
        )
        PM.Networking.EventSync.pause_sync_var = false
        reset_in_progress = false
        print("All clients have completed the reset.")
            
func notify_host_that_reset_is_in_progress():
    rpc_id(1, "host_rx_my_reset_is_in_progress")

remote func host_rx_my_reset_is_in_progress():
    var rpc_id = get_tree().get_rpc_sender_id()
    var mup_id = PM.STD.get_("peers_to_mups")[rpc_id]
    mups_that_have_ack_reset[mup_id] = true

func notify_host_that_reset_is_complete():
    while host_knows_reset_is_complete == false:
        rpc_id(1, "host_rx_my_reset_is_complete")
        yield(get_tree().create_timer(1.0), "timeout")

remote func host_rx_my_reset_is_complete():
    var rpc_id = get_tree().get_rpc_sender_id()
    var mup_id = PM.STD.get_("peers_to_mups")[rpc_id]
    mups_that_have_completed_reset[mup_id] = true
    rpc_id(rpc_id, "client_rx_host_ack_reset_is_complete")

remote func client_rx_host_ack_reset_is_complete():
    host_knows_reset_is_complete = true

func re_transimit_clients_to_reset_for_next_game_mode():
    while true:
        var missing_acknowledgement = false
        for mup in mups_that_have_ack_reset:
            if mups_that_have_ack_reset[mup] == false:
                missing_acknowledgement = true
                var client_rpc_id = PM.STD.get_("mups_to_peers")[mup]
                rpc_id(client_rpc_id, "client_rx_reset_the_game_for_next_game_mode")
        if missing_acknowledgement:
            pass
        else:
            break
        yield(get_tree().create_timer(1.0), "timeout")
