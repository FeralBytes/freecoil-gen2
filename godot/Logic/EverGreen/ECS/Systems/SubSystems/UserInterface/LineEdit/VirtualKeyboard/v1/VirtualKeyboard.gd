extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const COMPONENT_SYSTEM_ID = "UI_LineEdit_VirtualKeyboard"
const VERSION = 1
# All other variables should be defined in your components unless they are
# global for all of the same components.
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
var test_unique_sub = 0
# Declare system global/shared component Export variables here.

func on_ready(component_ref) -> void:
    component_ref.stored_global_position = component_ref.parent_entity.get_global_position()
    component_ref.parent_entity.connect("focus_entered", self, "_on_LineEdit_focus_entered", [component_ref])
    component_ref.parent_entity.connect("focus_exited", self, "_on_LineEdit_focus_exited", [component_ref])
    component_ref.parent_entity.connect("text_entered", self, "_on_LineEdit_text_entered", [component_ref])

func on_process(_delta, _component_ref) -> void:
    pass
    
func on_physics_process(_delta, _component_ref) -> void:
    pass
        
func on_input(_event, _component_ref) -> void:
    pass

func on_unhandled_input(_event, _component_ref) -> void:
    pass

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration(_component_ref):
    return true

func _on_LineEdit_focus_entered(component_ref):
    component_ref.keyboard_height_watching = true
    call_deferred("keyboard_height_watcher", component_ref)
    
func keyboard_height_watcher(component_ref):
    while component_ref.keyboard_height_watching:
        if not is_instance_valid(component_ref.parent_entity):
            break
        if OS.get_virtual_keyboard_height() != component_ref.keyboard_height:
            component_ref.keyboard_height = OS.get_virtual_keyboard_height()
            offset_lineedit_for_keyboard(component_ref.keyboard_height, component_ref)
        yield(PM.get_tree(), "idle_frame")

func offset_lineedit_for_keyboard(actual_soft_keyboard_height, component_ref):
    var height_offset: int
    if actual_soft_keyboard_height <= 0:  # Android 10 can return a negative value.
        height_offset = 0
    else:
        # https://github.com/godotengine/godot/pull/43104/files
        var actual_resolution = OS.get_window_safe_area().size
        var virtual_resolution = Vector2(ProjectSettings.get_setting("display/window/size/width"),
                ProjectSettings.get_setting("display/window/size/height"))
        # Below needed to account for the godot camera offset.
        var quotient = component_ref.parent_entity.get_global_position().y / virtual_resolution.y
        var remainder = (quotient) - int(quotient)
        var virtual_y_position_on_screen = remainder * virtual_resolution.y  
        var height_ratio = virtual_resolution.y / actual_resolution.y
        var width_ratio = virtual_resolution.x / actual_resolution.x
        var virtual_keyboard_size = int(ceil(actual_soft_keyboard_height * width_ratio))
        var new_camera_y_offset = virtual_y_position_on_screen + component_ref.parent_entity.rect_size.y - virtual_resolution.y
        print("new_line_edit_y = " + str(new_camera_y_offset))
        print("virtual_keyboard_size = " + str(virtual_keyboard_size))
        new_camera_y_offset += virtual_keyboard_size + 15  # Pad keyboard size in case of 
        if component_ref.debug:
            print("------------------------------------------------------------")
            print(component_ref.parent_entity.name)
            print("actual_resolution = " + str(actual_resolution))
            print("virtual_resolution = " + str(virtual_resolution))
            print("actual_soft_keyboard_height = " + str(actual_soft_keyboard_height))
            print("virtual_y_position_on_screen = " + str(virtual_y_position_on_screen))
            print("virtual_keyboard_size = " + str(virtual_keyboard_size))
            print("width_ratio = " + str(width_ratio))
            print("height_ratio = " + str(height_ratio))
            print("component_ref.parent_entity.get_global_position() = " + str(component_ref.parent_entity.get_global_position()))
            print("OS.get_window_safe_area() = " + str(OS.get_window_safe_area()))
            print("new_camera_y_offset = "+ str(new_camera_y_offset))
            print("------------------------------------------------------------")
        height_offset = new_camera_y_offset
    component_ref.parent_entity.get_tree().call_group("Camera", "pan_for_virtual_keyboard", height_offset)

func _on_LineEdit_focus_exited(component_ref):
    component_ref.keyboard_height_watching = false
    component_ref.parent_entity.get_tree().call_group("Camera", "pan_for_virtual_keyboard", 0)

func _on_LineEdit_text_entered(new_text, component_ref):
    if new_text == "":
        component_ref.parent_entity.grab_focus()
        component_ref.parent_entity.get_tree().call_group("Camera", "shake_no")
    else:
        pass
