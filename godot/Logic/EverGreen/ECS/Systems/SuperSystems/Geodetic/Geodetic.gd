tool
extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "Geodetic"
var MapDownloader = HTTPRequest.new()

export(int) var editor_yield_duration: int = 60
# Note: Google uses a default Earth radius of 6,378,137 meters
# https://gis.stackexchange.com/a/127949
const EARTH_RADIUS = 6378137.0  # in meters
# Previously was using: 
# const EARTH_RADIUS = 6371000.0  # in meters
# const EARTH_RADIUS = 6372800  # in meters
#-- 6371.0 km is the authalic radius based on/extracted from surface area;
# -- 6372.8 km is an approximation of the radius of the average circumference
#    (i.e., the average great-elliptic or great-circle radius), where the
#     boundaries are the meridian (6367.45 km) and the equator (6378.14 km).
#
#Using either of these values results, of course, in differing distances:
#
# 6371.0 km -> 2886.44444283798329974715782394574671655 km;
# 6372.8 km -> 2887.25995060711033944886005029688505340 km;
# (results extended for accuracy check:  Given that the radii are only
#  approximations anyways, .01' ≈ 1.0621333 km and .001" ≈ .00177 km,
#  practical precision required is certainly no greater than about
#  .0000001——i.e., .1 mm!)
#
#As distances are segments of great circles/circumferences, it is
#recommended that the latter value (r = 6372.8 km) be used (which
#most of the given solutions have already adopted, anyways). 
#When applying these examples in real applications, it is better to use 
#the mean earth radius, 6371 km. This value is recommended by the International 
#Union of Geodesy and Geophysics and it minimizes the RMS relative error between 
#the great circle and geodesic distance.
# https://www.movable-type.co.uk/scripts/latlong.html

var map_origin_lat
var map_origin_long
var map_origin_x
var map_origin_y
var long_meter_plus_to_px  #~1.1 meter is how many pxiels?
var lat_meter_plus_to_px
var center_x
var center_y
var center_lat
var center_long
var center_x_offset  # offset in pixels from the origin
var center_y_offset  # offset in pixels from the origin
signal new_center_location(previous, current)
var player_lat
var player_long
var player_x
var player_y
signal new_player_location(previous, current)
var fi_current_location
var fi_current_location_connection_state #log and track signal connection
# FOR DOWNLOADING #
var  last_downloaded_tile_coords: Array
var download_successful: bool = false
var amount_of_map_group_downloaded = 0
var map_api_key_encoded = [20, 54, 81, 208, 128, 210, 87, 204, 23, 83, 137, 209, 219, 69, 
    247, 147, 114, 130, 79, 56, 185, 4, 247, 212, 35, 217, 71, 163, 5, 66, 80, 
    125, 126, 38, 173, 37, 147, 165, 25, 90, 148, 48, 153, 189, 221, 103, 182, 
    147, 205, 18, 30, 206, 116, 114, 45, 52, 124, 29, 245, 95, 231, 48, 175, 
    201, 85, 186, 156, 8, 105, 130, 214, 58, 40, 111, 115, 212, 124, 112, 36, 
    209, 102, 129, 47, 101, 1, 7, 32, 232, 253, 7, 17, 17, 96, 32, 104, 184, 46, 
    66, 244, 95, 1, 58, 97, 239, 145, 31, 219, 254, 211, 232, 78, 34, 181, 248, 
    1, 95, 74, 53, 18, 5, 168, 23, 82, 108, 235, 104, 145, 31, 40, 51, 63, 229, 
    94, 113, 149, 242, 253, 5, 195, 204, 204, 103, 28, 21, 2, 204, 114, 133, 56, 
    189, 123, 0, 128, 209, 169, 127, 154, 137, 106, 85, 1, 227, 99, 229, 90, 234, 
    95, 165, 123, 16, 4, 150, 196, 122, 58, 160, 71, 154, 208, 10, 13, 2, 2, 94, 
    18, 129, 196, 135, 249, 234, 117, 164, 228, 34, 4, 238, 162, 215, 199, 219, 
    118, 186, 115, 222, 208, 227, 66, 200, 183, 129, 176, 76, 59, 250, 98, 191, 
    179, 192, 9, 182, 204, 48, 43, 160, 92, 51, 103, 253, 25, 105, 46, 46, 132, 
    250, 58, 17, 181, 121, 118, 16, 111, 152, 189, 95, 194, 218, 41, 15, 134, 
    210, 43, 242, 216, 12, 35, 64, 171, 105, 49, 190, 3, 253, 9, 30, 102, 160, 
    118, 121, 234, 146, 211, 182, 40, 113, 82, 59, 69, 57, 137, 65, 209, 160, 
    218, 12, 46, 217, 166, 255, 48, 150, 116, 52, 15, 157, 91, 255, 3, 162, 66, 
    15, 18, 110, 250, 35, 218, 134, 76, 54, 19, 174, 127, 136, 181, 252, 161, 
    50, 230, 56, 5, 64, 112, 76, 84, 34, 222, 177, 211, 176, 95, 28, 89, 12, 102, 
    196, 49, 48, 64, 106, 185, 186, 159, 26, 215, 15, 145, 146, 54, 48, 69, 160, 
    208, 30, 19, 251, 30, 31, 172, 222, 126, 1, 95, 63, 88, 69, 73, 248, 94, 23, 
    8, 211, 93, 3, 69, 189, 154, 137, 38, 157, 60, 175, 114, 119, 210, 189, 144, 
    250, 236, 140, 107, 209, 65, 35, 197, 161, 96, 118, 136, 203, 185, 32, 45, 
    116, 91, 134, 148, 18, 252, 213, 72, 124, 124, 144, 120, 3, 207, 105, 174, 
    33, 248, 28, 48, 14, 171, 169, 126, 95, 121, 86, 53, 130, 102, 73, 236, 19, 
    10, 204, 185, 55, 118, 81, 102, 83, 18, 180, 181, 116, 253, 239, 129, 95, 
    131, 191, 5, 128, 246, 180, 34, 37, 6, 4, 27, 69, 165, 199, 86, 161, 197, 
    84, 220, 165, 113, 97, 98, 196, 61, 201, 8, 31, 203, 34, 66, 60, 109, 95, 
    133, 166, 37, 61, 93, 82, 134, 153, 20, 14, 164, 174, 111, 140, 75, 200, 
    165, 83, 190, 98, 218, 91, 9, 85, 73, 13, 179, 23
]
var map_api_key_decoded: String = ""

# For Unit Testing #
var unit_testing: bool = false
var unit_trigger: bool = false
var unit_trigger2: bool = false
var unit_testing_link: int = 1

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        on_real_ready()

#func _process(delta) -> void:
#    pass
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func on_editor_ready() -> void:
    pass
    
func on_real_ready() -> void:
    PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
    MapDownloader.use_threads = true
    MapDownloader.timeout = 30
    add_child(MapDownloader)
    MapDownloader.connect("request_completed", self, "map_tile_downloaded")
    PM.STD.connect(PM.STD.monitor_(
        "fi_current_location"), self, "update_fi_current_location"
    )
    fi_current_location_connection_state = true

func editor_yield():
    for _i in range(editor_yield_duration):
        yield(get_tree(), "idle_frame")

func haversine_v0(lat1, long1, lat2, long2, radius=EARTH_RADIUS):
    # Version 0 of the formula was fastest in testing.
    var rlat1 = deg2rad(lat1)
    var rlong1 = deg2rad(long1)
    var rlat2 = deg2rad(lat2)
    var rlong2 = deg2rad(long2)
    var delta_rlat = rlat2 - rlat1
    var delta_rlong = rlong2 - rlong1
    var a  = pow(sin(delta_rlat / 2), 2) + cos(rlat1) * cos(rlat2) * pow(sin(delta_rlong / 2), 2)
    var c = 2 * asin(sqrt(a))
    return c * radius  # Distance between the 2 points.

func haversine_v1(lat1, long1, lat2, long2, radius=EARTH_RADIUS):
    var rlat1 = deg2rad(lat1)
    var rlat2 = deg2rad(lat2)
    var delta_lat = deg2rad(lat2 - lat1)
    var delta_long = deg2rad(long2 - long1)
    var a  = sin(delta_lat / 2) * sin(delta_lat / 2) + cos(rlat1) * cos(rlat2) * sin(delta_long / 2) * sin(delta_long / 2)
    var c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return c * radius

func bearing_from_to(lat1, long1, lat2, long2):
    if lat1 == lat2:
        if long1 == long2:
            return 0  # Same Point
    var rlat1 = deg2rad(lat1)
    var rlong1 = deg2rad(long1)
    var rlat2 = deg2rad(lat2)
    var rlong2 = deg2rad(long2)
    var delta_rlong = rlong2 - rlong1
    var x = cos(rlat1) * sin(rlat2) - sin(rlat1) * cos(rlat2) * cos(delta_rlong)
    var y = sin(delta_rlong) * cos(rlat2)
    var bearing_rad = atan2(y, x)
    var bearing_deg = rad2deg(bearing_rad)
    bearing_deg = wrap360(bearing_deg)
    return bearing_deg
    
func wrap360(degrees_beyond):
    degrees_beyond = float(degrees_beyond)
    if 0.0 <= degrees_beyond and degrees_beyond <= 360.0:
        return degrees_beyond
    else:
        return fmod((fmod(degrees_beyond, 360.0) + 360.0), 360.0)
    
func wrap_180_to_minus_180(degrees_beyond):
    degrees_beyond = float(degrees_beyond)
    if -180.0 <= degrees_beyond and degrees_beyond <= 180.0:
        return degrees_beyond
    else:
        return fmod((abs(fmod(degrees_beyond, 360.0/2.0))- 180.0), 360.0)

func wrap_90_to_minus_90(degrees_beyond):
    degrees_beyond = float(degrees_beyond)
    if -90.0 <= degrees_beyond and degrees_beyond <= 90.0:
        return degrees_beyond
    else:
        return fmod((abs(fmod(degrees_beyond, 360.0/4.0)) - 90.0), 360.0)    
        
func midpoint(lat1, long1, lat2, long2):
    var rlat1 = deg2rad(lat1)
    var rlat2 = deg2rad(lat2)
    var delta_rlong = deg2rad(long2) - deg2rad(long1)
    var xb = cos(rlat2) * cos(delta_rlong)
    var yb = cos(rlat2) * sin(delta_rlong)
    var mid_rlat = atan2(sin(rlat1) + sin(rlat2), sqrt(cos(rlat1) + xb) * cos(rlat1) + yb * yb)
    var mid_rlong = atan2(yb, cos(rlat1) + xb)
    return [wrap360(rad2deg(mid_rlat)), wrap360(rad2deg(mid_rlong))]
    
func get_dest_from_bearing_range(start_lat, start_long, distance, bearing):
    var start_rlat = deg2rad(start_lat)
    var start_rlong = deg2rad(start_long)
    distance = distance * 2
    var dest_rlat = asin(sin(start_rlat) * cos(distance / EARTH_RADIUS) + cos(start_rlat) *
        sin(distance / EARTH_RADIUS) * cos(bearing))
    var dest_rlong = start_rlong + atan2(sin(bearing) * sin(distance / EARTH_RADIUS) * 
        cos(start_rlat), cos(distance / EARTH_RADIUS) - sin(start_rlat) * sin(dest_rlat))
    return [wrap_90_to_minus_90(rad2deg(dest_rlat)), wrap_180_to_minus_180(rad2deg(dest_rlong))]
    
func get_meters_per_pixel(zoom_lvl, latitude):
    # https://gis.stackexchange.com/a/127949
    # https://groups.google.com/forum/#!topic/google-maps-js-api-v3/hDRO4oHVSeM
    return 156543.03392 * cos(latitude * PI / 180.0) / pow(2.0, float(zoom_lvl) + 1.0)
    
func get_next_tile_from_center(center_latitude, center_longitude, zoom_lvl, bearing):
    var meters_per_px = get_meters_per_pixel(zoom_lvl, center_latitude)
    var distance = meters_per_px * 640.0
    var next_tile_center = get_dest_from_bearing_range(center_latitude, 
        center_longitude, distance, bearing
    )
    return next_tile_center

func get_neighbor_tile_centers(center_latitude, center_longitude, _zoom_lvl=19):
    return get_neighbor_tile_centers_v2(center_latitude, center_longitude, 
        _zoom_lvl
    )

func get_neighbor_tile_centers_v2(center_latitude, center_longitude, _zoom_lvl):
    var north_tile_center = get_north_tile(center_latitude, center_longitude)
    var north_east_tile_center = get_east_tile(north_tile_center[0], 
        north_tile_center[1]
    )
    var east_tile_center = get_east_tile(center_latitude, center_longitude)
    var south_east_tile_center = get_south_tile(east_tile_center[0], 
        east_tile_center[1]
    )
    var south_tile_center = get_south_tile(center_latitude, center_longitude)
    var south_west_tile_center = get_west_tile(south_tile_center[0], 
        south_tile_center[1]
    )
    var west_tile_center = get_west_tile(center_latitude, center_longitude)
    var north_west_tile_center = get_north_tile(west_tile_center[0], 
        west_tile_center[1]
    )
    return [north_tile_center, north_east_tile_center, east_tile_center, 
        south_east_tile_center, south_tile_center, south_west_tile_center, 
        west_tile_center, north_west_tile_center
    ]

func get_neighbor_tile_centers_v1(center_latitude, center_longitude, zoom_lvl):
    var north_tile_center = get_next_tile_from_center(center_latitude, 
        center_longitude, zoom_lvl, 0
    )
    var north_east_tile_center = get_next_tile_from_center(north_tile_center[0], 
        north_tile_center[1], zoom_lvl, 90
    )
    var east_tile_center = get_next_tile_from_center(center_latitude, 
        center_longitude, zoom_lvl, 90
    )
    var south_east_tile_center = get_next_tile_from_center(east_tile_center[0], 
        east_tile_center[1], zoom_lvl, 180
    )
    var south_tile_center = get_next_tile_from_center(center_latitude, 
        center_longitude, zoom_lvl, 180
    )
    var south_west_tile_center = get_next_tile_from_center(south_tile_center[0], 
        south_tile_center[1], zoom_lvl, 270
    )
    var west_tile_center = get_next_tile_from_center(center_latitude, 
        center_longitude, zoom_lvl, 270
    )
    var north_west_tile_center = get_next_tile_from_center(west_tile_center[0], 
        west_tile_center[1], zoom_lvl, 0
    )
    return [north_tile_center, north_east_tile_center, east_tile_center, 
        south_east_tile_center, south_tile_center, south_west_tile_center, 
        west_tile_center, north_west_tile_center
    ]
   
func get_bearing_n_range(lat1, long1, lat2, long2):
    var distance = haversine_v0(lat1, long1, lat2, long2)  # distance == range
    var bearing = bearing_from_to(lat1, long1, lat2, long2)
    return [bearing, distance]

# The mapping between latitude, longitude and pixels is defined by the web mercator projection.
# https://developers.google.com/maps/documentation/javascript/examples/map-coordinates?hl=ko
# https://developers.google.com/maps/documentation/javascript/coordinates
func apply_projection(lat, long, tile_size=256.0):
    var siny = sin((lat * PI) / 180.0)
    # Truncating to 0.9999 effectively limits latitude to 89.189. This is
    # about a third of a tile past the edge of the world tile.
    if siny < -0.9999:
        siny = -0.9999
    if siny > 0.9999:
        siny = 0.9999
    var long_to_x = tile_size * (0.5 + long / 360.0)
    var lat_to_y = tile_size * (0.5 - log((1.0 + siny) / (1.0 - siny)) / (4.0 * PI))
    return [long_to_x, lat_to_y]


func convert_lat_long_to_pixel(lat, long, zoom):
    var projected_lat_long = apply_projection(lat, long, 256.0)  # 256 is the constant used by Google.
    lat = projected_lat_long[0]
    long = projected_lat_long[1]
    var total_x = int(lat * pow(2, zoom))
    var total_y = int(long * pow(2, zoom))
    return [total_x, total_y]

func convert_pixel_to_projection(x, y, zoom):
    var scale = pow(2, zoom)
    var projection_x = x / scale
    var projection_y = y / scale
    return [projection_x, projection_y]

func convert_pixel_to_projection_attempt_beter(x, y, zoom):
    # Ultimately this failed: time to move on.
    # https://stackoverflow.com/questions/7477003/calculating-new-longitude-latitude-from-old-n-meters
    var scale = pow(2, zoom)
    var lat_quotient = floor(x / 256)
    var long_qoutient = floor(y / 256)
    var lat_whole = lat_quotient * 256
    var long_whole = long_qoutient * 256
    var lat_remains = x - lat_whole
    var long_remains = y - long_whole
    var almost_lat = lat_whole / scale
    var almost_long = long_whole / scale
    # to make all of the extra stuff work below, we would have to accept that 
    # the calculated projection value would always be off, and then we have to 
    # pass out the remain meters and in the final conversion to lat long we 
    # could then add in the missing meters and then maybe it would come out 
    # correctly.
    print("almost_lat = " + str(almost_lat) + " | lat_remains = " + str(lat_remains))
    print("almost_long = " + str(almost_long) + " | long_remains = " + str(long_remains))
    var lat_long = convert_projection_to_lat_long(almost_lat, almost_long, zoom)
    var mets_per_px = get_meters_per_pixel(zoom, lat_long[0])
    print("Meters Per Pixel = %0.8f" % mets_per_px)
    var mets_in_lat = mets_per_px * lat_remains
    # number of km per degree = ~111km (111.32 in google maps, but range varies
    # between 110.567km at the equator and 111.699km at the poles)
    # 1km in degree = 1 / 111.32km = 0.0089
    # 1m in degree = 0.0089 / 1000 = 0.0000089
    var coef = mets_in_lat *  0.000008983
    print("coef = " + str(coef))
    var lat = almost_lat + coef
    print("lat = " + str(lat) + " | almost_lat = " + str(almost_lat))
    return [almost_lat, almost_long]

func total_tiles_at_zoom(zoom):
    return pow(2, zoom) * pow(2, zoom)
    
func get_tile_coordinates_from_x_y(x, y, tile_size):
    return [int(x / tile_size), int(y / tile_size)]

func convert_projection_to_lat_long(projected_x, projected_y, _zoom):  #reverse_projection
    # https://gis.stackexchange.com/questions/66247/what-is-the-formula-for-calculating-world-coordinates-for-a-given-latlng-in-goog
    var g_tile_size  = 256.0
    var long = float(projected_x) / g_tile_size * 360.0 - 180.0
    var n = PI - 2.0 * PI * float(projected_y) / g_tile_size
    var lat = (180.0 / PI * atan(0.5 * (exp(n) - exp(-n))))
    return [lat, long]
    
func convert_pixel_to_lat_long(x, y, zoom):
    var result = convert_pixel_to_projection(x, y, zoom)
    result = convert_projection_to_lat_long(result[0], result[1], zoom)
    return result

func set_map_origin(lat, long, zoom=19):
    map_origin_lat = lat
    map_origin_long = long
    var results = convert_lat_long_to_pixel(lat, long, zoom)
    map_origin_x = results[0]
    map_origin_y = results[1]
    lat_meter_plus_to_px = calc_difference_for_lat_change(zoom)
    long_meter_plus_to_px = calc_difference_for_long_change(zoom)
    center_lat = lat
    center_long = long
    center_x = results[0]
    center_y = results[1]
    center_x_offset = 0
    center_y_offset = 0

func calc_map_movement_from_origin(new_lat, new_long, zoom=19):  
    var results = convert_lat_long_to_pixel(new_lat, new_long, zoom)
    var moved_x = results[0] - map_origin_x
    var moved_y = map_origin_y - results[1]
    return [moved_x * -1, moved_y]

# Long Name: plot_entity_by_lat_long_from_origin_in_px()
func plot_entity(entity_lat, entity_long, zoom):
    var results = convert_lat_long_to_pixel(entity_lat, entity_long, zoom)
    var entity_x = map_origin_x - results[0]
    var entity_y = results[1] - map_origin_y
    return [entity_x * -1, entity_y]

# Below gives you the amount of pixels that have changed when you move 0.00001.
# For more see details below at calc_difference_for_long_change()
func calc_difference_for_lat_change(zoom):
    if map_origin_lat == null:
        return null
    var var_lat = map_origin_lat
    var data_set_of_y = []
    var data_set_avg_deviation = []
    for i in range(-100, 100):
        var_lat = map_origin_lat
        var_lat += 0.00001 * i
        var plot_result_y= plot_entity(var_lat, map_origin_long, zoom)[1]
        data_set_of_y.append(plot_result_y)
    for y_id in range(0, data_set_of_y.size()):
        if y_id + 1 < data_set_of_y.size():
            var deviation = abs(abs(data_set_of_y[y_id]) - 
                abs(data_set_of_y[y_id + 1])
            )
            data_set_avg_deviation.append(deviation)
    var sum = 0
    for index in range(0, data_set_avg_deviation.size()):
        sum += data_set_avg_deviation[index]
    var avg_deviation = sum / float(data_set_avg_deviation.size())
    return avg_deviation

# This gives you the amount of pixels that have changed when you move 0.00001.
# That is 1 hundred thousandths of a Decimal Degree.
# The fifth decimal place is worth up to 1.1 m: it distinguish trees from each 
# other. Accuracy to this level with commercial GPS units can only be achieved 
# with differential correction.
# For this to work we need to calculate at longitude percision of no more than
# 0.00001 because anything less and rounding errors bleed into the average
# deviation results and skews the calculation
func calc_difference_for_long_change(zoom):  
    if map_origin_long == null:
        return null
    var var_long = map_origin_long
    var data_set_of_x = []
    var data_set_avg_deviation = []
    for i in range(-100, 100):
        var_long = map_origin_long
        var_long += 0.00001 * i
        var plot_result_x= plot_entity(map_origin_lat, var_long, zoom)[0]
        data_set_of_x.append(plot_result_x)
    for x_id in range(0, data_set_of_x.size()):
        if x_id + 1 < data_set_of_x.size():
            var deviation = abs(abs(data_set_of_x[x_id]) - 
                abs(data_set_of_x[x_id + 1])
            )
            data_set_avg_deviation.append(deviation)
    var sum = 0
    for index in range(0, data_set_avg_deviation.size()):
        sum += data_set_avg_deviation[index]
    var avg_deviation = sum / float(data_set_avg_deviation.size())
    return avg_deviation
    
func download_map_tile(latitude, longitude, markers=[]):
        # markers are an array of [color, label, lat, long]
        last_downloaded_tile_coords = [latitude, longitude]
        download_successful = false
        if PM.SS["FreecoiLInterface"].API_KEY == "":
            decode_api_key_in_memory()
        if check_if_map_tile_exists_by_coords([latitude, longitude]) != false:
            download_successful = true
            unit_trigger = true
            return
        var http_error
        if unit_testing:
            if unit_testing_link == 1:
                http_error = MapDownloader.request("https://via.placeholder.com/640")
            else:
                http_error = MapDownloader.request("https://pinetools.com/" + 
                    "9e23ed54-debc-4387-94da-2071a6265e5b"
                )
        else:
            var request_str
            if markers.size() > 0:
                var markers_str = ""
                var counter = 0
                for marker in markers:
                    if counter != 0:
                        markers_str += "&"
                    counter += 1
                    markers_str += ("markers=color:" + marker[0] + "|label:" + marker[1]
                        + "|" + str(marker[2]) + "," + str(marker[3])
                    )
                markers_str += "&"
                request_str = ("https://maps.googleapis.com/maps/api/staticmap?center="
                    + str(latitude) + "," + str(longitude) + 
                    "&zoom=19&scale=1&size=640x640&maptype=hybrid&" + markers_str
                    + "key=" + PM.SS["FreecoiLInterface"].API_KEY
                )
            else:
                request_str = ("https://maps.googleapis.com/maps/api/staticmap?center=" + 
                    str(latitude) + "," + str(longitude) + 
                    "&zoom=19&scale=1&size=640x640&maptype=hybrid&" + 
                    #"markers=color:green|label:C|" + str(latitude) + "," + str(longitude) + "&" +
                    "key=" + PM.SS["FreecoiLInterface"].API_KEY
                )
            http_error = MapDownloader.request(request_str)
        if http_error != OK:
            PM.Log("Geodetic: download_map_tile(): An error occurred in the HTTP request. "
                + str(http_error), "error"
            )
            # FIXME: For now we will try again endlessly, but we should let the user know?
            call_deferred("download_map_tile", last_downloaded_tile_coords[0], 
                last_downloaded_tile_coords[1]
            )
            
func map_tile_downloaded(result, response_code, headers, body):
    var image = Image.new()
    var image_error = image.load_png_from_buffer(body)
    if image_error != OK:
        image = null
        PM.Log("Geodetic: map_tile_downloaded(): Image corrupted. " + 
            str(result) + " | " + str(response_code) + " | " + str(headers) +
            " | " + str(body), "error"
        )
        # FIXME: For now we will try again endlessly, but we should let the user know?
        if unit_testing:
            unit_trigger = true
        else:
            call_deferred("download_map_tile", last_downloaded_tile_coords[0], 
                last_downloaded_tile_coords[1]
            )
    else:
        image = null
        save_map_tile(body, last_downloaded_tile_coords)
        download_successful = true
        unit_trigger = true

func map_tile_rxd_from_host(buffer, coords):
    var image = Image.new()
    if buffer == null:
        return
    var image_error = image.load_png_from_buffer(buffer)
    if image_error != OK:
        image = null
    else:
        image = null
        save_map_tile(buffer, coords)

func save_map_tile(map_tile_buffer, tile_coords):
    var dir_check = Directory.new()
    dir_check.open("user://")
    if not dir_check.dir_exists("user://maps/"):
        dir_check.make_dir("user://maps/")
    var map_tile_as_file = File.new()
    var file_name = str(tile_coords[0]) + "," + str(tile_coords[1]) + ".png"
    map_tile_as_file.open("user://maps/" + file_name, File.WRITE)
    map_tile_as_file.store_buffer(map_tile_buffer)
    map_tile_as_file.close()

func download_complete_map_group(center_latitude, center_longitude):
    amount_of_map_group_downloaded = 0
    var close_enough = check_for_saved_close_enough_tile([center_latitude, 
        center_longitude]
    )
    if close_enough[0] != null:
        center_latitude = close_enough[0]
        center_longitude = close_enough[1]
    var neighbor_tiles = get_neighbor_tile_centers(center_latitude, 
        center_longitude, 19
    )
    set_map_origin(center_latitude, center_longitude)
    download_successful = false
    download_map_tile(center_latitude, center_longitude)
    while download_successful == false:
        yield(get_tree(), "idle_frame")
    amount_of_map_group_downloaded += 1
    for tile_coords in neighbor_tiles:
        download_successful = false
        download_map_tile(tile_coords[0], tile_coords[1])
        while download_successful == false:
            yield(get_tree(), "idle_frame")
        amount_of_map_group_downloaded += 1
    unit_trigger2 = true

func load_map_as_buffer_from_drive(lat, long):
    var file = File.new()
    var file_name = str(lat) + "," + str(long) + ".png"
    var full_path = "user://maps/" + file_name
    if file.file_exists(full_path):
        file.open(full_path, File.READ)
        var buffer = file.get_buffer(file.get_len())
        file.close()
        return buffer
    return null

func load_map_from_disk(lat, long):
    var file = File.new()
    var file_name = str(lat) + "," + str(long) + ".png"
    var full_path = "user://maps/" + file_name
    if file.file_exists(full_path):
        var image = Image.new()
        var image_error = image.load(full_path)
        if image_error == OK:
            var texture = ImageTexture.new()
            texture.create_from_image(image)
            return texture
        else:
            return null
    else:
        return null

func get_north_tile(lat, long):
    var north_640px_diff = (640.0/lat_meter_plus_to_px) * 0.00001
    return [lat + north_640px_diff, long]

func get_east_tile(lat, long):
    var east_640px_diff = (640.0 / long_meter_plus_to_px) * 0.00001
    return [lat, long + east_640px_diff]

func get_south_tile(lat, long):
    var south_640px_diff = (640.0 / lat_meter_plus_to_px) * 0.00001
    return [lat - south_640px_diff, long]

func get_west_tile(lat, long):
    var west_640px_diff = (640.0 / long_meter_plus_to_px) * 0.00001
    return [lat, long - west_640px_diff]

func calc_pixel_range(origin_coords, new_coords, zoom=19):  
    var new_coords_google_xy = convert_lat_long_to_pixel(new_coords[0], 
        new_coords[1], zoom
    )
    var origin_coords_google_xy = convert_lat_long_to_pixel(origin_coords[0], 
        origin_coords[1], zoom
    )
    var moved_x = new_coords_google_xy[0] - origin_coords_google_xy[0]
    var moved_y = origin_coords_google_xy[1] - new_coords_google_xy[1]
    return [moved_x * -1, moved_y]

func check_if_coords_inside_of_tile(tile_coords, check_coords) -> bool:
    var px_range = calc_pixel_range(tile_coords, check_coords)
    if abs(px_range[0]) < 320 and abs(px_range[1]) < 320:
        return true
    return false

func check_if_map_tile_exists_by_coords(coords):
    # check exact and plus or minus 1
    var full_path = "user://maps/" + str(coords[0]) + "," + str(coords[1]) + ".png"
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0] + 0.000001) + "," + 
        str(coords[1]) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0]) + "," + 
        str(coords[1] + 0.000001) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0] + 0.000001) + "," + 
        str(coords[1] + 0.000001) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0] - 0.000001) + "," + 
        str(coords[1]) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0]) + "," + 
        str(coords[1] - 0.000001) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    full_path = ("user://maps/" + str(coords[0] - 0.000001) + "," + 
        str(coords[1] - 0.000001) + ".png"
    )
    if PM.Misc.check_if_file_exists(full_path):
        return true
    return false

func get_existing_map_tiles():
    return PM.Misc.list_files_in_directory("user://maps/")

func check_for_saved_close_enough_tile(check_coords):
    var saved_tiles = get_existing_map_tiles()
    var coordinates_only = []
    var coords_split = []
    for tile in saved_tiles:
        coordinates_only.append(tile.rstrip(".png"))
    for coords in coordinates_only:
        coords_split = coords.split(",")
        var lat = float(coords_split[0])
        var long = float(coords_split[1])
        if check_if_coords_inside_of_tile([lat, long], check_coords):
            return [lat, long]
    return [null, null]

func update_fi_current_location(new_value):
    fi_current_location = new_value
    var previous_location = [player_lat, player_long]
    player_lat = new_value["latitude"]
    player_long = new_value["longitude"]
    if map_origin_lat != null and map_origin_long != null:
        move_map_center(player_lat, player_long)
        if fi_current_location_connection_state:
            emit_signal("new_player_location", previous_location, new_value)
    
func decode_api_key_in_memory():
    var crypto = Crypto.new()
    var key = CryptoKey.new()
#    var cert = X509Certificate.new()
    key.load("res://assets/KeysCerts/generated.key")
#    cert.load("res://assets/KeysCerts/generated.crt")
    map_api_key_decoded = crypto.decrypt(key, map_api_key_encoded
        ).get_string_from_utf8()
    PM.SS["FreecoiLInterface"].API_KEY = map_api_key_decoded

func get_new_lat_long_from_px_difference_from_origin(x_diff, y_diff):
    var lat_diff
    var long_diff
    if y_diff == 0:
        lat_diff = 0.0
    else:
        lat_diff = (float(y_diff) / lat_meter_plus_to_px) * 0.00001
    if x_diff == 0:
        long_diff = 0.0
    else:
        long_diff = (float(x_diff) / long_meter_plus_to_px) * 0.00001
    return [map_origin_lat + lat_diff, map_origin_long - long_diff]
    
func move_map_center(new_lat, new_long):
    var prev_lat = center_lat
    var prev_long = center_long
    center_lat = new_lat
    center_long = new_long
    var new_xy_offset = calc_map_movement_from_origin(new_lat, new_long, 19)
    center_x_offset = new_xy_offset[0]
    center_y_offset = new_xy_offset[1]
    var new_xy = convert_lat_long_to_pixel(new_lat, new_long, 19)
    center_x = new_xy[0]
    center_y = new_xy[1]
    emit_signal("new_center_location", [prev_lat, prev_long], [center_lat, center_long])

func toggle_fi_current_location_signal_connection(state):
    if not state == fi_current_location_connection_state: #Do nothing if requested state is already set
        if state:
            PM.STD.connect(PM.STD.monitor_(\
                    "fi_current_location"), self, "update_fi_current_location")
            fi_current_location_connection_state = true
        else:
            PM.STD.disconnect(PM.STD.monitor_(\
                    "fi_current_location"), self, "update_fi_current_location")
            fi_current_location_connection_state = false

# More Conversion Implementations to try:
#https://stackoverflow.com/questions/1591902/converting-long-lat-to-pixel-x-y-given-a-zoom-level
#https://stackoverflow.com/questions/23898964/getting-pixel-coordinated-from-google-static-maps
#https://gist.github.com/netsensei/58d2b145f68aa678e902
#https://stackguides.com/questions/18433835/google-maps-static-pixels-to-latitude-longitude-relative-to-zoom-level
#https://stackoverflow.com/questions/18433835/google-maps-static-pixels-to-latitude-longitude-relative-to-zoom-level
# Alternative Maps
# Open Street Maps
#http://teczno.com/squares/#14/40.78403/-73.64281
