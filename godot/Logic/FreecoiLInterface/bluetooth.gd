extends Resource

func check_bluetooth_status():
    if PM.SS["FreecoiLInterface"].FreecoiL != null:
        PM.STD.set_("fi_bluetooth_status", 
                PM.SS["FreecoiLInterface"].FreecoiL.bluetoothStatus())

func enable_bluetooth():
    if PM.SS["FreecoiLInterface"].FreecoiL!= null:
        PM.SS["FreecoiLInterface"].FreecoiL.enableBluetooth()

func disable_bluetooth():
    if PM.SS["FreecoiLInterface"].FreecoiL!= null:
        PM.SS["FreecoiLInterface"].FreecoiL.disableBluetooth()
