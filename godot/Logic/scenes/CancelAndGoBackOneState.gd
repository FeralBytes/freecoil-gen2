extends Button

func _on_Cancel_pressed():
    disabled = true
    PM.STD.set_("ProgramManager_state_trigger", false)
