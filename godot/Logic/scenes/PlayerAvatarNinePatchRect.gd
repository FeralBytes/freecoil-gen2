extends NinePatchRect

func _ready():
    PM.LTD.connect(PM.LTD.monitor_("player_avatar"), self, "avatar_selected")
    var player_avatar = PM.LTD.get_("player_avatar")
    if player_avatar != null and player_avatar != "":
        avatar_selected(player_avatar)

func avatar_selected(avatar_path):
    PM.BGLoad.load_n_callback(avatar_path, self, "setup_avatar")
    
func setup_avatar(results):
    if results[0] == "finished":
        texture =results[1]
