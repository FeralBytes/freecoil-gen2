extends GridContainer

func _ready():
    # https://godotengine.org/qa/31394/changing-the-width-of-scrollbars-in-scroll-containers
    get_parent().get_v_scrollbar().rect_min_size.x = 48

func deselect_all_others(child_name):
    for child in get_children():
        if child.name != child_name:
            child.deselect()
            
