extends NinePatchRect

var selected = false
var avatar_file_path: String

func _ready():
    var __ = connect("gui_input", self, "_on_gui_input")

# This would not work with out the MOUSE_FILTER being set correctly, see below.
# https://docs.godotengine.org/en/stable/classes/class_control.html#class-control-method-gui-input
 
func _on_gui_input(event):
    if event is InputEventMouseButton:
        if event.button_index == BUTTON_LEFT and event.pressed:
            if not selected:
                selected = true
                PM.LTD.set_("player_avatar", avatar_file_path)
                modulate = Color(1, 0.35, 0, 1)
                get_parent().deselect_all_others(name)

func deselect():
    selected = false
    modulate = Color(1, 1, 1, 1)
