extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "DRY"
var entities: Array = []

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
        on_real_ready()

#func _process(delta) -> void:
#    pass
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func on_editor_ready() -> void:
    pass
    
func on_real_ready() -> void:
    pass

func reset_to_defaults():
    pass

func reset_for_next_game():
    pass

func default_new_profile_data():
    return {"play_mode": "Challenge", "game_total_score": 0, "game_last_lvl_completed": 0, "game_last_lvl_score": 0,
        "Gear": {}
    }

func save_profile_on_success():
    var profile = PM.LTD.get_("profile_" + PM.LTD.get_("current_profile"))
    PM.LTD.set_("profile_" + PM.LTD.get_("current_profile"), profile)

func load_profile():
    var profile_name = PM.LTD.get_("current_profile")
    var profile = PM.LTD.get_("profile_" + profile_name)

func load_lvl_resources(raw_data):
    var parsed_data = JSON.parse(raw_data)
    var resources_to_load: Array = []
    var lvl_data
    if typeof(parsed_data.get_result()) == TYPE_DICTIONARY:
        lvl_data = parsed_data.get_result()
        # Parse your level data for resources to load.
    else:
        printerr("Level data was not a Dictionary!")
        PM.get_tree().quit(16)  # ERR_FILE_CORRUPT = 16 --- File: Corrupt error.
    return [lvl_data, resources_to_load]
