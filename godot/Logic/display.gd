extends Node

func print_display_metrics():
    var disp_mets = get_display_metrics()
    print("                 [Screen Metrics]")
    print("            Display size: ", disp_mets[0])
    print("   Decorated Window size: ", disp_mets[1])
    print("             Canvas size: ", disp_mets[2])
    print("        Project Settings: Width=", disp_mets[3], " Height=", disp_mets[4]) 
    
func get_display_metrics():
    var disp_mets = [OS.get_screen_size(), OS.get_real_window_size(), OS.get_window_size()]
    disp_mets.append(ProjectSettings.get_setting("display/window/size/width"))
    disp_mets.append(ProjectSettings.get_setting("display/window/size/height"))
    return disp_mets
    
