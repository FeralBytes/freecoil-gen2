extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const COMPONENT_SYSTEM_ID = "Template"
const VERSION = 1
# All other variables should be defined in your components unless they are
# global for all of the same components.
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
var test_unique_sub = 0
# Declare system global/shared component Export variables here.
# Always use for the name of the SubSystem, it will be easier to see when it is a unique resource
export(bool) var Template_SubSystem

func on_ready(component_ref) -> void:
    print(COMPONENT_SYSTEM_ID + " on_ready()")
    component_ref.test_ready += 1
    test_unique_sub += 1

func on_process(_delta, component_ref) -> void:
    print(COMPONENT_SYSTEM_ID + " on_process()")
    component_ref.test_process += 1
    
func on_physics_process(_delta, component_ref) -> void:
    print(COMPONENT_SYSTEM_ID + " on_physics_process()")
    component_ref.test_physics_process += 1
        
func on_input(_event, component_ref) -> void:
    print(COMPONENT_SYSTEM_ID + " on_input()")
    component_ref.test_input += 1

func on_unhandled_input(_event, component_ref) -> void:
    print(COMPONENT_SYSTEM_ID + " on_unhandled_input()")
    component_ref.test_unhandled_input += 1

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration(component_ref):
    return true
