# GdUnit generated TestSuite
#warning-ignore-all:unused_argument
#warning-ignore-all:return_value_discarded
#class_name MiscTest
extends GdUnitTestSuite

# TestSuite generated from
const __source = 'res://EverGreen/Misc.gd'

var _spy
var instance

func before_test() -> void:
    instance = auto_free(preload(__source).new())

func test_general_type_of() -> void:
    assert_str(instance.general_type_of(null)).is_equal("Null")
    assert_str(instance.general_type_of(true)).is_equal("Bool")
    assert_str(instance.general_type_of(1)).is_equal("Integer")
    assert_str(instance.general_type_of(0.1)).is_equal("Float")
    assert_str(instance.general_type_of("Test")).is_equal("String")
    assert_str(instance.general_type_of(Vector2(1,1))).is_equal("Vector2")
    assert_str(instance.general_type_of(Rect2(Vector2(1,1), Vector2(1,1)))
    ).is_equal("Rect2")
    assert_str(instance.general_type_of(Vector3(1,1,1))).is_equal("Vector3")
    assert_str(instance.general_type_of(Transform2D(1.0, Vector2(1,1)))
    ).is_equal("Transform2D")
#    assert_str(instance.general_type_of()).is_equal("Plane")
#    assert_str(instance.general_type_of()).is_equal("Quat")
#    assert_str(instance.general_type_of()).is_equal("AABB")
#    assert_str(instance.general_type_of()).is_equal("Basis")
#    assert_str(instance.general_type_of()).is_equal("Transform")
    assert_str(instance.general_type_of(Color(1,1,1,1))).is_equal("Color")
#    assert_str(instance.general_type_of()).is_equal("NodePath")
#    assert_str(instance.general_type_of()).is_equal("RID")
#    assert_str(instance.general_type_of()).is_equal("Object")
    assert_str(instance.general_type_of({"Test":2})).is_equal("Dictionary")
    assert_str(instance.general_type_of([0,1,2,3])).is_equal("Array")
#    assert_str(instance.general_type_of()).is_equal("RawArray")
#    assert_str(instance.general_type_of()).is_equal("IntegerArray")
#    assert_str(instance.general_type_of()).is_equal("FloatArray")
#    assert_str(instance.general_type_of()).is_equal("StringArray")
#    assert_str(instance.general_type_of()).is_equal("Vector2Array")
#    assert_str(instance.general_type_of()).is_equal("Vector3Array")
#    assert_str(instance.general_type_of()).is_equal("ColorArray")
#    assert_str(instance.general_type_of()).is_equal("Max")
    # Should test specific types of objects.
#    assert_str(instance.general_type_of(Node.instance())).is_equal("Object: Node")
#    assert_str(instance.general_type_of()).is_equal("")
#    assert_str(instance.general_type_of()).is_equal("")
#    assert_str(instance.general_type_of()).is_equal("")
#    assert_str(instance.general_type_of()).is_equal("")

func test_join_array_to_str() -> void:
    assert_str(instance.join_array_to_str(["T", "e", "s", "t"])).is_equal("T,e,s,t")
    assert_str(instance.join_array_to_str(["T", "e", "s", "t"], "")).is_equal("Test")

func test_get_error_description() -> void:
    assert_str(instance.get_error_description(OK)).is_equal("OK: No error.")
    assert_str(instance.get_error_description(FAILED)).is_equal(
        "FAILED: Generic error, error uknown."
    )

func test_list_files_in_directory() -> void:
    assert_array(instance.list_files_in_directory(
        "res://test/EverGreen/MiscTestResources/list_dir_contents0/"
    )).is_empty()
    assert_array(instance.list_files_in_directory(
        "res://test/EverGreen/MiscTestResources/list_dir_contents1/"
    )).is_equal(["test1.gd", "test0.gd"])

func test_get_random_key_of_dict() -> void:
    var dict1 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":123456, 
        "type":"Test"
    }
    assert_bool(dict1.has(instance.get_random_key_of_dict(dict1))).is_true()


func test_godot_dict_hash_still_broken():
    var dict1 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":12345, 
        "type":"Test"
    }
    var dict2 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    # The dicts are not the same hash() value because the order is different.
    assert_int(dict1.hash()).is_not_equal(dict2.hash())
    assert_dict(dict1).is_equal(dict2)

func test_dicts_same_keys_n_vals() -> void:
    var dict1 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":12345, 
        "type":"Test"
    }
    var dict2 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":12345, 
        "type":"Test"
    }
    assert_bool(instance.dicts_same_keys_n_vals(dict1, dict2)).is_true()
    var dict3 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    assert_bool(instance.dicts_same_keys_n_vals(dict1, dict3)).is_true()
    dict2["time"] = 0
    dict2["event_num"] = 12345
    assert_bool(instance.dicts_same_keys_n_vals(dict1, dict2)).is_false()
    dict2["time"] = 12345
    dict2["event_num"] = 0
    dict2["extra"] = "slot"
    assert_bool(instance.dicts_same_keys_n_vals(dict1, dict2)).is_false()
    var dict4 = {"additional":{"test":{"deep":{"deeper":{"even_deeper":"Nesting"}}}}, 
        "event_num":0, "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    var dict5 = {"additional":{"test":{"deep":{"deeper":{"even_deeper":"Nesting"}}}},
        "event_num":0, "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    assert_bool(instance.dicts_same_keys_n_vals(dict4, dict5)).is_true()

func test_arrays_have_same_content() -> void:
    var arr1 = [1,2,3,4,5]
    var arr2 = [1,2,3,4,5]
    var arr3 = [5,4,3,2,1]
    var arr4 = [5,0,3,9,1]
    assert_bool(instance.arrays_have_same_content(arr1, arr2)).is_true()
    assert_bool(instance.arrays_have_same_content(arr1, arr3)).is_true()
    assert_bool(instance.arrays_have_same_content(arr1, arr4)).is_false()

func test_check_if_file_exists() -> void:
    assert_bool(instance.check_if_file_exists(
        "res://test/EverGreen/MiscTestResources/list_dir_contents1/test1.gd"
    )).is_true()
    assert_bool(instance.check_if_file_exists(
        "res://test/EverGreen/MiscTestResources/list_dir_contents1/test999.gd"
    )).is_false()

func test_are_floats_equal() -> void:
    assert_bool(0.123456789 == 0.123456789).is_true()
    assert_bool(0.123456789 == 0.12347).is_false()
    assert_bool(0.123456 == 0.123456).is_true()
    assert_bool(0.1 == 0.1).is_true()
    assert_bool(0.0 == 0.0).is_true()
    assert_bool(0.123456 == 0.1234567).is_false()
    assert_bool(0.1234566 == 0.1234567).is_false()
    # Below is the MAX percision distinction on my PC 17 places 
    # after the decimal.
    # I found out that recently Zyllan added support to Godot for Double 
    # Percision floats.
    assert_bool(0.12345678901234561 == 0.12345678901234562).is_false()
    assert_bool(0.12345678901234561 == 0.12345678901234561).is_true()

func test_create_2d_array() -> void:
    assert_array(instance.create_2d_array(2,2)).is_equal([[null, null],[null, null]])

func test_create_3d_array() -> void:
    assert_array(instance.create_3d_array(2,2,2)).is_equal(
        [[[null, null], [null, null]], [[null, null], [null, null]]]
    )

func test_is_divisible_by() -> void:
    assert_bool(instance.is_divisible_by(10, 10)).is_true()
    assert_bool(instance.is_divisible_by(11, 10)).is_false()
    assert_bool(instance.is_divisible_by(9, 10)).is_false()
    assert_bool(instance.is_divisible_by(20, 10)).is_true()
    assert_bool(instance.is_divisible_by(4, 2)).is_true()
    assert_bool(instance.is_divisible_by(5, 2)).is_false()
    assert_bool(instance.is_divisible_by(8, 4)).is_true()
    assert_bool(instance.is_divisible_by(4, 4)).is_true()
    assert_bool(instance.is_divisible_by(100, 10)).is_true()
    assert_bool(instance.is_divisible_by(-100, 10)).is_true()
