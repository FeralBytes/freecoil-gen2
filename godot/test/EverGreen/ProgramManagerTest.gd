# GdUnit generated TestSuite
#warning-ignore-all:unused_argument
#warning-ignore-all:return_value_discarded
#class_name ProgramManagerTest
extends GdUnitTestSuite

# TestSuite generated from
const __source = 'res://EverGreen/ProgramManager.gd'
const __scene = 'res://EverGreen/ProgramManager.tscn'

var _runner :GdUnitSceneRunner
var _spy_scene: Node

func before_test() -> void:
    _spy_scene = spy(__scene)
    _runner = scene_runner(_spy_scene)
    # set time factor to 10 to simulate the scene very fast
    _runner.set_time_factor(10)
    # enable this line to show the running scene during test execution
    _runner.maximize_view()
    verify(_spy_scene, 0).detect_vital_data()

func test_detect_vital_data() -> void:
    yield(_runner.simulate_frames(10, 100), "completed")
    verify(_spy_scene, 1).detect_vital_data()
    if _spy_scene.fake_os == "":
        assert_str(_spy_scene.operating_system).is_equal(OS.get_name())
    if _spy_scene.fake_unique_id == "":
        assert_str(_spy_scene.unique_id).is_equal(OS.get_unique_id())
        
func test_detect_vital_data2() -> void:
    _spy_scene.fake_os = "Android"
    _spy_scene.fake_unique_id = "FAKED8UNIQUE8ID8NOT8REAL8AT8ALL"
    yield(_runner.simulate_frames(10, 100), "completed")
    verify(_spy_scene, 1).detect_vital_data()
    assert_str(_spy_scene.fake_os).is_equal("Android")
    assert_str(_spy_scene.operating_system).is_equal("Android")
    assert_str(_spy_scene.unique_id).is_equal("FAKED8UNIQUE8ID8NOT8REAL8AT8ALL")

func test_container_is_background_loaded_by_the_first() -> void:
    assert_object(PM.TheContainer).is_not_null()
