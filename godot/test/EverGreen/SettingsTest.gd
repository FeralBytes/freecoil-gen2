# GdUnit generated TestSuite
#warning-ignore-all:unused_argument
#warning-ignore-all:return_value_discarded
#class_name SettingsTest
extends GdUnitTestSuite

# TestSuite generated from
const __source = 'res://EverGreen/Settings.gd'

var source
var instance
var _spy

func before_test() -> void:
    if _spy != null:
        _spy.reset_for_testing()
    instance = auto_free(preload(__source).new("Testing"))
    _spy = spy(instance)

func test_set_matches_get():
    #verify_no_interactions(_spy)
    _spy.set_("Test0", 1)
#    verify(_spy, 0).set_("Test", 1)
#    verify(_spy, 1).register_(any_string(), any_int())
#    verify_no_more_interactions(_spy)
    assert_int(_spy.get_("Test0")).is_equal(1)
    _spy.set_("Test0", 2)
    assert_int(_spy.get_("Test0")).is_equal(2)

func test_signal_num_increments_for_1_instance() -> void:
    _spy.set_("Test0", 1)
    assert_str(_spy.monitor_("Test0")).is_equal("Sig0")
    _spy.set_("Test1", 1)
    assert_str(_spy.monitor_("Test1")).is_equal("Sig1")

func test_signal_remains_the_same_for_same_data_name_on_value_change() -> void:
    _spy.set_("Test0", 1)
    assert_str(_spy.monitor_("Test0")).is_equal("Sig0")
    _spy.set_("Test0", 2)
    assert_str(_spy.monitor_("Test0")).is_equal("Sig0")

func test_monitor_nonexistant_data_name_get_signal() -> void:
    assert_str(_spy.monitor_("Tes0")).is_equal("Sig0")
    assert_str(_spy.monitor_("Test1")).is_equal("Sig1")
    assert_str(_spy.get_("Test1")).is_null()

func test_signal_num_increments_for_2_instances() -> void:
    # Using Const Dictionary Hack for Static Variables:
    # https://godotengine.org/qa/48121/how-do-you-write-a-static-variable-in-gdscript
    # This justifies this technique as not a hack and it is acceptable.
    # https://github.com/godotengine/godot/issues/32063
    var _spy0 = spy(auto_free(load(__source).new("Testing")))
    assert_int(_spy0.static_vars.signals_used).is_equal(-1)
    _spy0.set_("Test0", 1)
    var _spy1 = spy(auto_free(load(__source).new("Testing")))
    assert_int(_spy0.static_vars.signals_used).is_equal(0)
    assert_int(_spy1.static_vars.signals_used).is_equal(0)
    assert_str(_spy0.monitor_("Test0")).is_equal("Sig0")
    assert_object(_spy1.get_("Test0")).is_null()
    _spy1.set_("Test0", 1)
    assert_str(_spy1.monitor_("Test0")).is_equal("Sig1")

func test_signal_is_emitted() -> void:
    _spy.set_("Test0", 1)
    verify(_spy).emit_signal("Sig0", 1)
    reset(_spy)
    _spy.register_("Test2", 2)
    verify(_spy).register_("Test2", 2)
    verify(_spy).__create_signal()
    verify_no_more_interactions(_spy)

func test_can_save_data() -> void:
    _spy = spy(auto_free(preload(__source).new("TestingDataSave", "", true, true)))
    assert_bool(_spy.auto_save).is_true()
    _spy.set_("Test0", 0)
    var file = File.new()
    while _spy.__saving:
        yield(get_tree(), "idle_frame")
    assert_bool(file.file_exists("user://" + _spy.save_path + ".json")).is_true()
    if file.file_exists("user://" + _spy.save_path + ".json"):
        file.open("user://" + _spy.save_path + ".json", file.READ)
        var text = file.get_as_text()
        var temp = parse_json(text)
        file.close()
        assert_dict(temp).is_equal(
            {"DATA_VERSION": [0.0, "Sig00"], "Test0":[0.0, "Sig0"]}
        )
        var dir = Directory.new()
        if dir.open("user://") == OK:
            dir.remove("user://" + _spy.save_path + ".json")
    else:
        assert_bool(false).is_true()

func test_load_on_ready_works() -> void:
    _spy = spy(auto_free(preload(__source).new("TestingDataSave", "", true, true)))
    _spy.set_("Test0", 0)
    var file = File.new()
    while _spy.__saving:
        yield(get_tree(), "idle_frame")
    assert_bool(file.file_exists("user://" + _spy.save_path + ".json")).is_true()
    if file.file_exists("user://" + _spy.save_path + ".json"):
        file.open("user://" + _spy.save_path + ".json", file.READ)
        var text = file.get_as_text()
        var temp = parse_json(text)
        file.close()
        assert_dict(temp).is_equal(
            {"DATA_VERSION": [0.0, "Sig00"], "Test0":[0.0, "Sig0"]}
        )
        _spy.reset_for_testing()
        _spy = spy(auto_free(preload(__source).new("TestingDataSave", "", true, true)))
        assert_dict(_spy.__data).is_equal(
            {"DATA_VERSION": [0.0, "Sig00"], "Test0":[0.0, "Sig0"]}
        )
        var dir = Directory.new()
        if dir.open("user://") == OK:
            dir.remove("user://" + _spy.save_path + ".json")
    else:
        assert_bool(false).is_true()

func test_dont_over_write_existing_signals() -> void:
    _spy = spy(auto_free(preload(__source).new("TestingDataSave", "", true, true)))
    _spy.set_("Test0", 0)
    var file = File.new()
    while _spy.__saving:
        yield(get_tree(), "idle_frame")
    assert_bool(file.file_exists("user://" + _spy.save_path + ".json")).is_true()
    if file.file_exists("user://" + _spy.save_path + ".json"):
        file.open("user://" + _spy.save_path + ".json", file.READ)
        var text = file.get_as_text()
        var temp = parse_json(text)
        file.close()
        assert_dict(temp).is_equal(
            {"DATA_VERSION": [0.0, "Sig00"], "Test0":[0.0, "Sig0"]}
        )
        _spy.reset_for_testing()
        var _spy2 = spy(auto_free(preload(__source).new("Testing")))
        _spy2.set_("testing 0", 0)
        _spy2.set_("testing 1", 1)
        assert_int(_spy.static_vars.signals_used).is_equal(1)
        assert_str(_spy2.monitor_("testing 1")).is_equal("Sig1")
        _spy = spy(auto_free(preload(__source).new("TestingDataSave", "", true, true)))
        assert_dict(_spy.__data).is_equal(
            {"DATA_VERSION": [0.0, "Sig00"], "Test0":[0.0, "Sig2"]}
        )
        assert_str(_spy.monitor_("Test0")).is_not_equal("Sig1")
        assert_str(_spy.monitor_("Test0")).is_equal("Sig2")
        assert_int(_spy.static_vars.signals_used).is_equal(2)
        assert_int(_spy2.static_vars.signals_used).is_equal(2)
        var dir = Directory.new()
        if dir.open("user://") == OK:
            dir.remove("user://" + _spy.save_path + ".json")
    else:
        assert_bool(false).is_true()

func test_request_quit_on_max_signals() -> void:
    _spy._being_tested = true
    for i in range(0, 257):
        _spy.set_("Test" + str(i), i)
    assert_bool(_spy.__requesting_quit).is_true()
