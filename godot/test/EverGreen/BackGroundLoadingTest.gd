# GdUnit generated TestSuite
#warning-ignore-all:unused_argument
#warning-ignore-all:return_value_discarded
#class_name BackGroundLoadingTest
extends GdUnitTestSuite

# TestSuite generated from
const __source = 'res://EverGreen/BackGroundLoading.gd'
const _small_scene = "res://scenes/Global/Empty.tscn"
const _small_multi_scene = "res://scenes/Global/Container/TheContainer.tscn"
const _audio_file = "res://assets/audio/music/main_menu_loop.ogg"
const _json_file = "res://test/EverGreen/BackGroundLoadingTestResources/test.json"
const _ext_image = "../.gitlab/ci_scripts/FeralBytes_logo.png"

var instance
var _spy
var test_small_resource = preload(_small_scene)

var callback_happened = false
var callback_happened2 = false
var callback_results = []
var callback_results2 = []
var wait_count = 0
var finished_counter = 0

func call_me_back(results):
    callback_happened = true
    callback_results = results

func call_me_back2(results):
    callback_happened2 = true
    callback_results2 = results

func callback_finished_counter(results):
    if results[0] == "finished":
        finished_counter += 1

func wait_for_callback_or_timeout(wait_for_num_frames=100) -> bool:
    while callback_happened != true:
        wait_count += 1
        if wait_count > wait_for_num_frames:
            return false
        yield(get_tree(), "idle_frame")
    return true

func before_test() -> void:
    instance = auto_free(preload(__source).new())
    _spy = spy(instance)

func after_test() -> void:
    if _spy != null:
        if is_instance_valid(_spy):
            var when_reset_complete = _spy.reset_for_testing()
            if when_reset_complete is GDScriptFunctionState:
                yield(when_reset_complete, "completed")
    PM.current_threads = 0
    PM.max_threads = OS.get_processor_count()
    callback_happened = false
    callback_happened2 = false
    callback_results = []
    callback_results2 = []
    finished_counter = 0
    wait_count = 0
#            print("_spy.__background_threads.size() = ", _spy.__background_threads.size())
#            print("PM.current_threads = ", PM.current_threads)
#            print("_spy.static_vars.next_thread_num = ", _spy.static_vars.next_thread_num)

func test_load_n_callback_returns_next_thread_number() -> void:
    var current_thread_count = _spy.static_vars.next_thread_num
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    assert_int(thread_num1).is_equal(current_thread_count + 1)
    var thread_num2 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    assert_int(thread_num2).is_equal(current_thread_count + 2)


func test__loading_progress_update() -> void:
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var thread_num2 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    _spy._loading_progress_update(thread_num1, 0.1)
    assert_str(_spy.__threads_progress[thread_num1][0]).is_equal("loading")
    assert_float(_spy.__threads_progress[thread_num1][1]).is_equal(0.1)
    _spy._loading_progress_update(thread_num1, 0.999)
    assert_str(_spy.__threads_progress[thread_num1][0]).is_equal("loading")
    assert_float(_spy.__threads_progress[thread_num1][1]).is_equal(0.999)
    _spy._loading_progress_update(thread_num2, 0.9625)
    assert_str(_spy.__threads_progress[thread_num2][0]).is_equal("loading")
    assert_float(_spy.__threads_progress[thread_num2][1]).is_equal(0.9625)

func test_handle_progress_updates_calls_back_on_progress() -> void:
    var current_thread_count = _spy.static_vars.next_thread_num
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    assert_int(thread_num1).is_equal(current_thread_count + 1)
    _spy.handle_progress_update(thread_num1,"loading", 1.0)
    assert_bool(callback_happened).is_true()
    assert_array(callback_results).is_equal(["loading", 0.99])

func test_handle_progress_updates_ignores_other_threads() -> void:
    var current_thread_count = _spy.static_vars.next_thread_num
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    assert_int(thread_num1).is_equal(current_thread_count + 1)
    _spy.handle_progress_update(current_thread_count + 5, "loading", 28.25)
    assert_bool(callback_happened).is_false()

func test_handle_progress_updates_never_returns_100() -> void:
    var current_thread_count = _spy.static_vars.next_thread_num
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    assert_int(thread_num1).is_equal(current_thread_count + 1)
    _spy.handle_progress_update(thread_num1, "loading", 100.0)
    assert_bool(callback_happened).is_true()
    assert_array(callback_results).is_not_equal(["loading", 100.0])

func test_finished_background_loading() -> void:
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var result = wait_for_callback_or_timeout()
    if result is GDScriptFunctionState: 
        result = yield(result, "completed")
    assert_bool(result).is_true()
    assert_array(callback_results).is_equal(["finished", test_small_resource])

func test_load_n_callback_loads_small_scene() -> void:
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var max_wait = 100
    var wait_counter = 0
    var cont_wait_loop = true
    var result = wait_for_callback_or_timeout()
    while cont_wait_loop:
        if result is GDScriptFunctionState: 
            result = yield(result, "completed")
#        print("callback_results = ", callback_results)      
        if callback_results[0] != "finished":
            result = wait_for_callback_or_timeout()            
        else:
            cont_wait_loop = false
        wait_counter += 1
        if wait_counter > max_wait:
            cont_wait_loop = false
            result = false
    assert_bool(result).is_true()
    assert_array(callback_results).is_equal(["finished", test_small_resource])

func test_load_n_callback_loads_medium_scene() -> void:
    var thread_num1 = _spy.load_n_callback(_small_multi_scene, self, "call_me_back")
    var max_wait = 100
    var wait_counter = 0
    var cont_wait_loop = true
    var test_med_res = preload(_small_multi_scene)
    var result = wait_for_callback_or_timeout()
    while cont_wait_loop:
        if result is GDScriptFunctionState: 
            result = yield(result, "completed")  
        if callback_results[0] != "finished":
            result = wait_for_callback_or_timeout()            
        else:
            cont_wait_loop = false
        wait_counter += 1
        if wait_counter > max_wait:
            cont_wait_loop = false
            result = false
    assert_bool(result).is_true()
    assert_array(callback_results).is_equal(["finished", test_med_res])

func test_teardown_happens() -> void:
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var max_wait = 100
    var wait_counter = 0
    var cont_wait_loop = true
    var result = wait_for_callback_or_timeout()
    while cont_wait_loop:
        if result is GDScriptFunctionState: 
            result = yield(result, "completed")
#        print("callback_results = ", callback_results)      
        if callback_results[0] != "finished":
            result = wait_for_callback_or_timeout()            
        else:
            cont_wait_loop = false
        wait_counter += 1
        if wait_counter > max_wait:
            cont_wait_loop = false
            result = false
    assert_bool(result).is_true()
    assert_bool(_spy.__background_threads.has(thread_num1)).is_false()

func test_force_slow_threads() -> void:
    _spy.__testing_force_slow_threads = true
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    yield(get_tree().create_timer(0.001), "timeout")
    assert_bool(_spy.__background_threads.has(thread_num1)).is_true()

func test_too_many_threads_single_core() -> void:
    _spy.__testing_force_slow_threads = true
    PM.max_threads = 1
    var thread_nums = []
    for i in range(0, PM.max_threads + 2):
        var thread_num = _spy.load_n_callback(_small_scene, self, "call_me_back")
        thread_nums.append(thread_num)
    assert_int(_spy.__background_threads.size()).is_less_equal(PM.max_threads)
    assert_int(PM.current_threads).is_equal(PM.max_threads)
    assert_int(_spy.__queued_threads.size()).is_equal(2)

func test___threaded_background_loader_returns_resource() -> void:
    _spy.__testing_dont_start_thread = true
    var thread_num = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var resource = _spy.__threaded_background_loader([_small_scene, thread_num])
    assert_object(resource).is_equal(preload(_small_scene))
    _spy.__threads_progress.erase(thread_num)

func test___threaded_background_loader_returns_resource_when_forced_slow() -> void:
    _spy.__testing_dont_start_thread = true
    _spy.__testing_force_slow_threads = true
    var thread_num = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var resource = _spy.__threaded_background_loader([_small_scene, thread_num])
    if resource is GDScriptFunctionState: 
        resource = yield(resource, "completed")
    assert_object(resource).is_equal(preload(_small_scene)) 
    _spy.__threads_progress.erase(thread_num)
    
func test_threads_do_finish_over_time() -> void:
    _spy.__testing_force_slow_threads = true
    PM.max_threads = 1
    var small_scene = preload(_small_scene)
    var thread_num1 = _spy.load_n_callback(_small_scene, self, "call_me_back")
    var thread_num2 = _spy.load_n_callback(_small_scene, self, "call_me_back2")
    assert_int(_spy.__background_threads.size()).is_less_equal(PM.max_threads)
    assert_int(PM.current_threads).is_equal(PM.max_threads)
    assert_int(_spy.__queued_threads.size()).is_equal(1)
    var wait = true
    var thread1_res = false
    var thread2_res = true
    while wait:
        if callback_results.size() != 0:
            if callback_results[0] == "finished":
                thread1_res = true
        if callback_results2.size() != 0:
            if callback_results2[0] == "finished":
                thread2_res = true
        if thread1_res and thread2_res:
            wait = false
        yield(get_tree(), "idle_frame")
    assert_array(callback_results).is_equal(["finished", small_scene])
                        

func test_cleanup_is_complete() -> void:
    var thread_nums = []
    for i in range(0, 2):
        var thread_num = _spy.load_n_callback(_small_scene, self, 
            "callback_finished_counter"
        )
        thread_nums.append(thread_num)
    var wait = true
    var wait_counter = 0
    var max_wait = 100
    while wait:
        wait_counter += 1
        if wait_counter >= max_wait:
            assert_int(finished_counter).is_less_equal(max_wait)
            break
        else:
            yield(get_tree(), "idle_frame")
            if _spy.__queued_threads.size() == 0:
                if _spy.__background_threads.size() == 0:
                    if _spy.__threads_progress.size() == 0:
                        wait = false
    for thread in thread_nums:
        assert_bool(_spy.__loaded_res.has(thread)).is_false()
        assert_bool(_spy.__threads_progress.has(thread)).is_false() 

func test_atleast_1_thread_will_run() -> void:
    _spy.__testing_force_slow_threads = true
    PM.max_threads = 1
    PM.current_threads = 2
    var thread_nums = []
    for i in range(0, PM.max_threads + 1):
        var thread_num = _spy.load_n_callback(_small_scene, self, "call_me_back")
        thread_nums.append(thread_num)
    assert_int(_spy.__background_threads.size()).is_less_equal(PM.max_threads)
    assert_int(PM.current_threads).is_equal(3)
    assert_int(_spy.__queued_threads.size()).is_equal(1)

func test_load_audio_resource() -> void:
    if PM.ci_testing:
        return  # This test does not work without .import directory
    var thread_num1 = _spy.load_n_callback(_audio_file, self, "call_me_back")
    var max_wait = 100
    var wait_counter = 0
    var cont_wait_loop = true
    var result = wait_for_callback_or_timeout()
    while cont_wait_loop:
        if result is GDScriptFunctionState: 
            result = yield(result, "completed")  
        if callback_results[0] != "finished":
            result = wait_for_callback_or_timeout()            
        else:
            cont_wait_loop = false
        wait_counter += 1
        if wait_counter > max_wait:
            cont_wait_loop = false
            result = false
    assert_bool(result).is_true()
    assert_object(callback_results[1]).is_class("AudioStreamOGGVorbis")

func test_load_json_file() -> void:
    var thread_num1 = _spy.load_n_callback(_json_file, self, "call_me_back")
    var max_wait = 1000
    var wait_counter = 0
    var cont_wait_loop = true
    var result = wait_for_callback_or_timeout(max_wait)
    while cont_wait_loop:
        if result is GDScriptFunctionState: 
            result = yield(result, "completed")  
        if callback_results[0] != "finished":
            result = wait_for_callback_or_timeout(max_wait)            
        else:
            cont_wait_loop = false
        wait_counter += 1
        if wait_counter > max_wait:
            cont_wait_loop = false
            result = false
    assert_int(wait_counter).is_less(max_wait)
    assert_str(callback_results[0]).is_equal("finished")
    if callback_results[0] == "finished":
        assert_dict(callback_results[1]).is_equal({
            "States": [
                {
                "name": "MainMenu",
                "links_to": [
                    "NewGame",
                    "Settings",
                    "ExitGame"
                ]
                },
            ]
        })

# Then test loading external image file
# There is a lot to test for this though:
# is the path relative, then it needs to be converted.
# is the path absolute
# is the path a user:// path
# is the path equal to cache_dir or config_dir?
#func test_load_ext_image_file() -> void:
#    var thread_num1 = _spy.load_n_callback(_ext_image, self, "call_me_back")
#    var max_wait = 300
#    var wait_counter = 0
#    var cont_wait_loop = true
#    var result = wait_for_callback_or_timeout(max_wait)
#    while cont_wait_loop:
#        if result is GDScriptFunctionState: 
#            result = yield(result, "completed")  
#        if callback_results[0] != "finished":
#            result = wait_for_callback_or_timeout(max_wait)            
#        else:
#            cont_wait_loop = false
#        wait_counter += 1
#        if wait_counter > max_wait:
#            cont_wait_loop = false
#            result = false
#    assert_int(wait_counter).is_less(max_wait)

# The test loading text files (txt, md)

# Then Test loading image file

#func test_too_many_threads_results_in_queued_threads() -> void:
#    _spy.__verbose = true
#    _spy.__testing_force_slow_threads = true
#    var thread_nums = []
#    for i in range(0, PM.max_threads + 5):
#        var thread_num = _spy.load_n_callback(_small_scene, self, "call_me_back")
#        thread_nums.append(thread_num)
#        print(thread_num)
#    assert_int(_spy.__background_threads.size()).is_less_equal(PM.max_threads)
#    assert_int(PM.current_threads).is_equal(PM.max_threads)
#    assert_int(_spy.__queued_threads.size()).is_equal(5) 
