# GdUnit generated TestSuite
#warning-ignore-all:unused_argument
#warning-ignore-all:return_value_discarded
#class_name PmStateAndLoaderTest
extends GdUnitTestSuite

# TestSuite generated from
const __source = 'res://EverGreen/PMStateAndLoader.gd'
const _scene_source = "res://logic/ProgramManagerStates/#Example#.tscn"
var runner

func before_test():
    runner = scene_runner(_scene_source)

func test_are_export_arrays_duplicated() -> void:
    # To test that export arrays are still broken, as in all the same array in memory
    # you have to comment out the lines that do the duplication in on_real_ready()
    assert_array(runner._current_scene.resources_to_preload).is_empty()
    assert_array(runner._current_scene.other_screens_to_load).is_empty()
    runner._current_scene.resources_to_preload.append(2)
    assert_array(runner._current_scene.other_screens_to_load).is_empty()
    assert_array(runner._current_scene.resources_to_preload).is_equal([2])
    

