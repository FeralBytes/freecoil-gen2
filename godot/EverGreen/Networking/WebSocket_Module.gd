extends Resource

var websocket_server = null
var websocket_client = null

var websockets_client_init_comp = false
var websockets_test_server_connected = false
var websockets_test_client_connected = false
var websockets_test_server_client_id = null
var websockets_test_server_rx_data = false
var websockets_test_client_rx_data = false
var websockets_test_client_requested_closed = false
var websockets_test_server_disconnected = false
var websockets_test_client_disconnected = false

func init_websocket_server():
    websocket_server = WebSocketServer.new()
    websocket_server.connect("client_connected", self, "websocket_server_connected")
    websocket_server.connect("client_disconnected", self, "websocket_server_disconnected")
    websocket_server.connect("client_close_request", self, "websocket_server_client_close_request")
    websocket_server.connect("data_received", self, "websocket_server_on_data")
    websocket_server.listen(58888)
    PM.Networking.network_loops.append("websocket_server_poll")
    
func websocket_server_connected(id, _proto):
    PM.Log("Network: Websockets: Websocket server new connection with " + str(id))
    if PM.Networking.testing:
        websockets_test_server_connected = true
    
func websocket_server_disconnected(id, _was_clean=false):
    PM.Log("Network: Websockets: Websocket server disconnected from id " + str(id))
    if PM.Networking.testing:
        websockets_test_server_disconnected = true
    
func websocket_server_client_close_request(id, code, reason):
    PM.Log("Network: Websockets: Websocket server, client close request from " + str(id)
        + " with code of " + str(code) + " reason of " + str(reason))
    if PM.Networking.testing:
        websockets_test_client_requested_closed = true
    
func websocket_server_on_data(id):
    if PM.Networking.testing:
        websockets_test_server_client_id = id
        websockets_test_server_rx_data = websocket_server.get_peer(id).get_packet()
    
func websocket_server_poll():
    websocket_server.poll()

func websocket_server_send_data(id, data):
    websocket_server.get_peer(id).put_packet(data)


func init_websocket_client():
    websocket_client = WebSocketClient.new()
    websocket_client.connect("connection_established", self, "websocket_client_established")
    websocket_client.connect("connection_error", self, "websocket_client_error")
    websocket_client.connect("connection_closed", self, "websocket_client_closed")
    websocket_client.connect("data_received", self, "websocket_client_on_data")
    PM.Networking.network_loops.append("websocket_client_poll")
    PM.Networking.set_process(true)
    PM.Log("Network: Websockets: Websockets Initialization Completed.")
    PM.STD.set_data("websockets_client_init_comp", true)

func websocket_client_established(_proto=""):
    PM.Log("Network: Websockets: Websocket client connected.")
    PM.STD.set_data("websockets_client_connected", true)
    if PM.Networking.testing:
        websockets_test_client_connected = true

func websocket_client_error(_was_clean=false):
    PM.Log("Network: Websockets: Encountered an error while attempting websocket client communication.")
    pass

func websocket_client_closed(was_clean=false):
    PM.Log("Network: Websockets: Websocket client disconnected was clean = " + str(was_clean))
    if PM.Networking.testing:
        websockets_test_client_disconnected = true
    
func websocket_client_on_data():
    var temp = websocket_client.get_peer(1).get_packet()
    PM.STD.set_data("websocket_client_data", temp)
    PM.Log("Network: Websockets: Data recieved = " + str(temp.get_string_from_utf8()))
    if PM.Networking.testing:
        websockets_test_client_rx_data = websocket_client.get_peer(1).get_packet()

func websocket_client_poll():
    websocket_client.poll()

func websocket_client_connect_to_url(url, additional_headers):
    var err
    if additional_headers == null:
        err = websocket_client.connect_to_url(url, [], false)
    else:
        err = websocket_client.connect_to_url(url, [], false, additional_headers)
    if err != OK:
        PM.Log("Network: Websockets: Unable to connect; error = " + str(err), "error")
    else:
        PM.Log("Network: Websockets: Connection made.")

func websocket_client_send_data(data):
    websocket_client.get_peer(1).put_packet(data)

func websocket_client_disconnect():
    websocket_client.disconnect_from_host()
