extends Node

var UDP_Module: Resource = preload("res://EverGreen/Networking/UDP_Module.tres")
var Server_Module: Resource = preload("res://EverGreen/Networking/Server_Module.tres")
var Client_Module: Resource = preload("res://EverGreen/Networking/Client_Module.tres")
var WebSocket_Module: Resource = preload("res://EverGreen/Networking/WebSocket_Module.tres")
var EventSync = preload("res://EverGreen/Networking/EventSync_Module.tscn")
var unique_persistant_id = 1
var rng = RandomNumberGenerator.new()
var upnp = null
var network_loops = []
var test_all_mups_were_ready = false
var testing = false
var validate_ipv4 = RegEx.new()
var netsync_sent_time: int
var netsync_offset_array: Array = []
var netsync_latency_array: Array = []
var netsync_avg_offset_from_server: float = 0.0
var netsync_last_server_time: int = 0
var netsync_avg_latency: float 
var number_of_pings_unanswered = 0
var AutoReconnectTimer
var NetSyncTimer
    
func invert_mups_to_peers(mups_to_peers):
    if PM.get_tree().get_network_peer() == null:
        var peers_to_mups = {}
        for key in mups_to_peers:
            peers_to_mups[mups_to_peers[key]] = key
        # NOTE: For JSON Objects must have keys that are strings not Integers.
        # Invert players and do not store in JSON.
        PM.STD.set_("peers_to_mups", peers_to_mups)
    elif PM.get_tree().is_network_server():
        var peers_to_mups = {}
        for key in mups_to_peers:
            peers_to_mups[mups_to_peers[key]] = key
        # NOTE: For JSON Objects must have keys that are strings not Integers.
        # Invert players and do not store in JSON.
        PM.STD.set_("peers_to_mups", peers_to_mups)

# Called when the node enters the scene tree for the first time.
func _init():
    call_deferred("deferred_init")
    
func deferred_init():
    name = "Networking"
    add_to_group("Network")
    AutoReconnectTimer = Timer.new()
    NetSyncTimer = Timer.new()
    EventSync = EventSync.instance()
    add_child(EventSync)
    testing = false
    # warning-ignore:return_value_discarded
    PM.get_tree().connect("network_peer_connected", Server_Module, "_client_connected")
    # warning-ignore:return_value_discarded
    PM.get_tree().connect("network_peer_disconnected", Server_Module, "_client_disconnected")
    # warning-ignore:return_value_discarded
    PM.get_tree().connect("connected_to_server", Client_Module, "_connected_ok")
    # warning-ignore:return_value_discarded
    PM.get_tree().connect("connection_failed", Client_Module, "_connection_failed")
    # warning-ignore:return_value_discarded
    PM.get_tree().connect("server_disconnected", Client_Module, "_server_disconnected")
    #FIXME: We can only handle IPv4 at this time not IPv6.
    validate_ipv4.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" +
            "\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
    PM.STD.register_("server_ip", "")
    PM.STD.register_("server_ignore_list", [])
    #PM.STD.register("mup_id", PM.unique_id)  # mup_id = my_unique_persistant_id
    PM.STD.register_("mups_to_peers", {PM.unique_id: 1})
    invert_mups_to_peers(PM.STD.get_("mups_to_peers"))
    PM.STD.register_("mups_ready", {})
    PM.STD.register_("peers_minimum", 2)
    PM.STD.register_("mups_status", {PM.unique_id: "do_not_connect"})
    #Settings.Session.connect(PM.STD.monitor_("server_possible_ips"), self, "setup_server_part2")
    PM.STD.connect(PM.STD.monitor_("mups_to_peers"), self, "invert_mups_to_peers")
    PM.STD.connect(PM.STD.monitor_("mups_ready"), self, "check_if_all_mups_ready")
    PM.STD.set_("websockets_init_comp", false)
    PM.STD.set_("websockets_client_connected", false)
    PM.STD.set_("all_ready", false)
    PM.STD.set_("server_invite", false)
    # connection_status can be one of ["do_not_connect", "connected", "disconnected", 
    # "connecting", "reconnecting", "reconnected", "identifying"]
    PM.STD.set_("connection_status", "do_not_connect")
    PM.STD.set_("mups_reconnected", [])
    rng.randomize()
    UDP_Module.host_udp_broadcast_uid = PM.unique_id
    PM.Log("Network: UDP: UDP broadcast uid = " + str(UDP_Module.host_udp_broadcast_uid))
    PM.STD.connect(PM.STD.monitor_("connection_status"), self, "auto_reconnect")
    PM.add_child(AutoReconnectTimer)
    AutoReconnectTimer.one_shot = true
    AutoReconnectTimer.wait_time = 3.0
    AutoReconnectTimer.connect("timeout", self, "auto_reconnect_by_timer")
    NetSyncTimer.one_shot = false
    NetSyncTimer.wait_time = 1.0
    PM.add_child(NetSyncTimer)
    NetSyncTimer.connect("timeout", self, "start_net_sync")
    NetSyncTimer.start()
    set_process(false)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(__):
    for loop in network_loops:
        if "udp" in loop:
            UDP_Module.call_deferred(loop)
        else:
            call_deferred(loop)

remote func sync_var(classname, var_name, var_val=null):   # class_name is a reserved word in GDScript
    PM.Log("RPC: 'sync_var' sender_id = " + str(PM.get_tree().get_rpc_sender_id()) +
                " classname=" + str(classname) + " var_name=" + str(var_name) + "  var_val=" + str(var_val), "debug")
    match classname:
        "Network":
            PM.STD.set_(var_name, var_val, true)

func test_ip_address():
    var local_ips = IP.get_local_addresses()
    var valid_ipv4_matches = []
    for ip in local_ips:
        var a_match = validate_ipv4.search(ip)
        if a_match != null:
            valid_ipv4_matches.append(a_match.get_strings()[0])
    valid_ipv4_matches.erase("127.0.0.1")
    PM.STD.set_("server_possible_ips", valid_ipv4_matches)
    if valid_ipv4_matches.size() == 0:
        PM.Log("Network: No valid IPv4 addresses.", "error")
        PM.STD.set_("server_ip_issue", "none")
    elif valid_ipv4_matches.size() == 1:
        PM.STD.set_("server_ip", valid_ipv4_matches[0])
        PM.STD.set_("device_ip", valid_ipv4_matches[0])
        PM.Log("Network: Valid IP Address found of " + 
                str(valid_ipv4_matches[0]) + ".", "info")
        PM.STD.set_("server_ip_issue", "all_good")
    else:  # > 1 AKA multiple valid external IP Address to choose from.
        if typeof(PM.STD.get_("server_selected_ip")) == TYPE_STRING:
            PM.STD.set_("server_ip", 
                PM.STD.get_("server_selected_ip")
            )
            PM.STD.set_("device_ip", 
                PM.STD.get_("server_ip")
            )
            PM.Log("Network: Valid IP Address selected " + 
                    PM.STD.get_("server_selected_ip") + ".", "info")
            PM.STD.set_("server_ip_issue", "all_good")
        else:
            PM.Log("Network: Too many valid IPv4 addresses.", "error")
            PM.Log("    Please choose one.", "error")
            PM.Log(PM.STD.get_("server_possible_ips"), "error")
            PM.STD.set_("server_ip_issue", "multiple")

func reset_networking():
    if Server_Module.Server != null:
        Server_Module.Server.close_connection()
        yield(PM.get_tree().create_timer(0.1), "timeout")
        Server_Module.Server = null
    if Client_Module.Client != null:
        Client_Module.Client.close_connection()
        yield(PM.get_tree().create_timer(0.1), "timeout")
        Client_Module.Client = null
    PM.get_tree().set_network_peer(null)
    # Reset Network Variables:
    network_loops.clear()
    UDP_Module.host_udp_broadcast = false
    PM.STD.set_("server_ip", "")
    PM.STD.set_("server_ignore_list", [])
    PM.STD.set_("mups_to_peers", {PM.unique_id: 1})
    PM.STD.set_("mups_ready", {}, false, false)
    PM.STD.set_("peers_minimum", 2)
    PM.STD.set_("mups_status", {PM.unique_id: "do_not_connect"})
    PM.STD.set_("websockets_init_comp", false)
    PM.STD.set_("websockets_client_connected", false)
    PM.STD.set_("all_ready", false, false, false)
    PM.STD.set_("server_invite", false)
    PM.STD.set_("connection_status", "do_not_connect")
    PM.STD.set_("mups_reconnected", [], false, false)
    PM.STD.set_("player_team", 0)
    PM.STD.set_("ui_team_being_viewed", 0)
    yield(PM.get_tree(), "idle_frame")  # Yield at least one time to be a coroutine.
    
remote func identify(prev_mup_id, godot_peer_id):
    var real_rpc_id = PM.get_tree().get_rpc_sender_id()
    if real_rpc_id != godot_peer_id:
        # Spoofing or Bad Client
        PM.Log("Network: Server: Error: Remote Client is Spoofing. Real rpc_id: " 
                + str(real_rpc_id) + " Spoofed rpc_id: " + str(godot_peer_id), "warning")
        Server_Module.Server.disconnect_peer(real_rpc_id, true)
        return
    else:
        if PM.get_tree().is_network_server():
            var mups_status = PM.STD.get_("mups_status")
            var mups_to_peers = PM.STD.get_("mups_to_peers")
            mups_to_peers[prev_mup_id] = godot_peer_id
            PM.STD.set_("mups_to_peers", mups_to_peers)
            if not mups_status.has(prev_mup_id):
                mups_status[prev_mup_id] = "connected"
            else:
                PM.Log("Player already identified with the same rpc_id.")
                mups_status[prev_mup_id] = "reconnected"
                var mups_reconnected = PM.STD.get_("mups_reconnected").duplicate()
                mups_reconnected.append(prev_mup_id)
                PM.STD.set_("mups_reconnected", mups_reconnected)
            PM.STD.set_("mups_status", mups_status)
            PM.STD.sync_peer(godot_peer_id)

func tell_server_i_am_ready(ready_or_not):
    if PM.get_tree().is_network_server():
        remote_tell_server_i_am_ready(ready_or_not)
    else:
        rpc_id(1, "remote_tell_server_i_am_ready", ready_or_not)

remote func remote_tell_server_i_am_ready(ready_or_not):
    if PM.get_tree().is_network_server():
        var sender_peer_id = PM.get_tree().get_rpc_sender_id()
        if sender_peer_id == 0:  # Was not called as a rpc, but was called on the server, by the server.
            sender_peer_id = 1
        var mup_id = PM.STD.get_("peers_to_mups")[sender_peer_id]
        var mups_ready = PM.STD.get_("mups_ready").duplicate()
        mups_ready[mup_id] = ready_or_not
        PM.STD.set_("mups_ready", mups_ready)
        # Above triggers: check_if_all_mups_ready()

func check_if_all_mups_ready(mups_ready):
    if PM.get_tree().is_network_server():
        var all_ready = true
        if mups_ready.size() == PM.STD.get_("mups_to_peers"
                ).size() and mups_ready.size() >= PM.Settings.MIN_PLAYERS:
            for mup in mups_ready:
                if mups_ready[mup] == false:
                    all_ready = false
        elif PM.Settings.FLAG_ALLOW_SINGLE_PLAYER:
            if mups_ready.size() >= 1:
                for mup in mups_ready:
                    if mups_ready[mup] == false:
                        all_ready = false
        else:
            all_ready = false
        if all_ready:
            PM.STD.set_("all_ready", true)
            
func unready_all_mups():
    if PM.get_tree().is_network_server():
        PM.STD.set_("all_ready", false, false, false)
        var mups_ready = PM.STD.get_("mups_ready").duplicate()  # Reuse
        for mups_id in mups_ready:
            mups_ready[mups_id] = false
        PM.STD.set_("mups_ready", mups_ready)
        # Above triggers: check_if_all_mups_ready()

func client_disconnect(quiet=false):
    if quiet:
        PM.get_tree().set_network_peer(null)
        Client_Module.Client = null
    else:
        Client_Module.Client.close_connection()
        PM.get_tree().set_network_peer(null)
    PM.STD.set_("connection_status", "do_not_connect")  
  
func auto_reconnect(connection_status):
    if connection_status == "disconnected":
        PM.Log("Disconnected from the server!", "warning")
        if Server_Module.Server == null:  # Make sure we are not the server.
            Client_Module.call_deferred("setup_as_client")
            AutoReconnectTimer.start()
        else:
            pass  # Not yet handling the server issue.

func auto_reconnect_by_timer():
    var connection_status = PM.STD.get_("connection_status")
    if connection_status == "disconnected":
        if not PM.get_tree().is_network_server():  # Make sure we are not the server.
            Client_Module.call_deferred("setup_as_client")
            AutoReconnectTimer.start()
        else:
            pass  # Not yet handling the server issue.
        

#### UPNP
func _init_upnp():
    upnp = UPNP.new()
    upnp.discover(2000, 2, "InternetGatewayDevice")
    upnp.add_port_mapping(PM.STD_LAN_PORT)
    upnp.delete_port_mapping(PM.STD_LAN_PORT)
    
#### Websocket Communication ####

func start_net_sync():
    if (PM.STD.get_("connection_status") == "connected" or 
            PM.STD.get_("connection_status") == "reconnected"):
        if Server_Module.Server == null:
            netsync_sent_time = OS.get_ticks_usec()
            rpc_id(1, "rx_ping", netsync_sent_time)
            number_of_pings_unanswered += 1
#            if number_of_pings_unanswered > 4:
#                PM.STD.set_("connection_status", "disconnected")
    
remote func rx_ping(time_sent):
    if Server_Module.Server != null:
        var client_offset = OS.get_ticks_usec() - time_sent
        var rpc_sender_id = PM.get_tree().get_rpc_sender_id()
        rpc_id(rpc_sender_id, "rx_pong", client_offset, OS.get_ticks_usec())
    
remote func rx_pong(server_stated_offset, server_time_stamp):
    var last_latency = (OS.get_ticks_usec() - netsync_sent_time) / 2.0
    netsync_last_server_time = server_time_stamp
    netsync_offset_array.append(server_stated_offset)
    if netsync_offset_array.size() == 8:
        var total: int = 0
        for val in netsync_offset_array:
            total += val
        netsync_avg_offset_from_server = float(total) / float(netsync_offset_array.size())
        netsync_offset_array.pop_front()
    netsync_latency_array.append(last_latency)
    if netsync_latency_array.size() == 8:
        var total: int = 0
        for val in netsync_latency_array:
            total += val
        netsync_avg_latency = float(total) / float(netsync_latency_array.size())
        netsync_latency_array.pop_front()
    number_of_pings_unanswered = 0
        
func get_server_time_usec():
    if Server_Module.Server == null:
        return OS.get_ticks_usec() + netsync_avg_offset_from_server - netsync_avg_latency
    else:
        return OS.get_ticks_usec()

func get_server_time_msec():
    return get_server_time_usec() / 1000

func get_round_trip_latency():
    if Server_Module.Server == null:
        return (netsync_sent_time + netsync_avg_offset_from_server) - netsync_last_server_time
    else:
        pass
        # FIXME: Need to calculate this for the server. 
    

func get_one_way_latency():
    if Server_Module.Server == null:
        return netsync_avg_latency
    else:
        pass
        # FIXME: Need to calculate this for the server. 

func estimated_deviation_from_server_time_usec():
    if Server_Module.Server == null:
        return (netsync_offset_array[-1] - netsync_avg_offset_from_server - netsync_avg_latency)
    else:
        return 0
    
func estimated_deviation_from_server_time_msec():
    if Server_Module.Server == null:
        return estimated_deviation_from_server_time_usec() / 1000
    else:  # division by zero otherwise.
        return 0

func server_time_deviation_is_acceptable():
    if Server_Module.Server == null:
        if netsync_offset_array.empty():
            return false
        var server_time_deviation = estimated_deviation_from_server_time_msec()
        # Increased the deviation allowable, slower phones may be the host.
        if server_time_deviation > 200:
            return false
        else:
            return true
    else:
        return true
