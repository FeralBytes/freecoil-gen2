extends Resource

var Client


func _connected_ok():
    PM.Log("Network: Client: Connection Established to the server, my peer id is: " + 
            str(PM.Networking.get_tree().get_network_unique_id()), "info")
    PM.Networking.rpc_id(1, "identify", PM.unique_id, 
            PM.Networking.get_tree().get_network_unique_id())
    PM.Networking.number_of_pings_unanswered = 0
    PM.STD.set_("connection_status", "connected")
    
func _connection_failed():
    PM.Log("Network: Client: Connection failed.", "warning")
    PM.Networking.get_tree().set_network_peer(null)
    PM.STD.set_("connection_status", "disconnected")
    # Attempt to reconnect.
    
func _server_disconnected():
    PM.Log("Network: Client: Connection terminated by the server.", "warning")
    PM.Networking.get_tree().set_network_peer(null)
    PM.STD.set_("connection_status", "disconnected")
    
func setup_as_client():
    if PM.STD.get_("connection_status") == "disconnected":
        PM.STD.set_("connection_status", "reconnecting")
    else:
        PM.STD.set_("connection_status", "connecting")
    PM.Log("Network: Client: Setting up as a client with server address of " + 
            PM.STD.get_("server_ip") + ":" + 
            str(PM.STD.get_("server_port")), "info")
    Client = NetworkedMultiplayerENet.new()
    Client.create_client(PM.STD.get_("server_ip"), int(PM.STD.get_("server_port")))
    PM.Networking.get_tree().set_network_peer(Client)
