extends Resource

var Server = null

func setup_server_part1():
    PM.Networking.reset_networking()
    PM.STD.set_("mup_id", "1")  # mup_id != peer id.
    PM.Networking.test_ip_address()
    if PM.STD.get_("server_ip_issue") == "all_good":
        PM.Log("Network: Server: Valid IP Address found of " + str(
                PM.STD.get_("server_ip"), "info") + ".")
        call_deferred("setup_server_part2")
        
func setup_server_part2():
    PM.Log("Network: Server: Setting up the Server now.", "info")
    PM.STD.set_("server_port", 8818)
    Server = NetworkedMultiplayerENet.new()
    Server.set_bind_ip(PM.STD.get_("server_ip"))
    Server.create_server(PM.STD.get_("server_port"), 
            62)
    PM.get_tree().set_network_peer(Server)
    PM.STD.set_("mups_status", {PM.unique_id: "connected"})
    PM.STD.set_("connection_status", "connecting")
    PM.Networking.UDP_Module.host_udp_broadcast = true
    PM.Networking.UDP_Module.broadcast_to_peers()
    PM.Networking.set_process(true)
    PM.Log("Network: Server: Server created and sending out UDP invites" +
            " with a UID of " + str(PM.Networking.UDP_Module.host_udp_broadcast_uid) + ".", "info")
            
func _client_connected(godot_peer_id):  # Client Equals Another Player
    if PM.Networking.get_tree().is_network_server():
        PM.Log("Network: Server: Peer Connected with peer id: " + str(godot_peer_id), "info")
        if godot_peer_id in PM.STD.get_data("mups_to_peers").values():
            var peers_to_mups = PM.STD.get_data("peers_to_mups")
            var mups_status = PM.STD.get_data("mups_status")
            mups_status[peers_to_mups[godot_peer_id]] = "identifying"
            PM.STD.set_("mups_status", mups_status)
        if PM.STD.get_("connection_status") != "connected":
            PM.STD.set_("connection_status", "connected")
    
func _client_disconnected(godot_peer_id):  # Client Equals Another Player
    if PM.Networking.get_tree().is_network_server():
        PM.Log("Network: Server: Peer Disconnected with peer id: " + str(godot_peer_id))
        if godot_peer_id in PM.STD.get_data("mups_to_peers").values():
            var peers_to_mups = PM.STD.get_data("peers_to_mups")
            var mups_status = PM.STD.get_data("mups_status")
            mups_status[peers_to_mups[godot_peer_id]] = "disconnected"
            PM.STD.set_("mups_status", mups_status)
            var all_mups_disconnected = true
            for mup in mups_status:
                if mup != "1":
                    if mups_status[mup] == "connected":
                        all_mups_disconnected = false
                    elif mups_status[mup] == "reconnected":
                        all_mups_disconnected = false
            if all_mups_disconnected:
                PM.STD.set_("connection_status", "disconnected")
