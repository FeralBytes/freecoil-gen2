extends Node

var unit_testing: bool = false
var ut_server_mup_id: String = "939972095cf1459c8b22cc608eff85da"
var ut_server_rpc_id: int = 1
var ut_client1_mup_id: String = "888972095cf1459c8b22cc608eff888"
var ut_client1_rpc_id: int = 54321
var ut_client2_mup_id: String = "f1459c8b21ef15e5e1a2adf15df5ece"
var ut_client2_rpc_id: int = 65351

var is_a_client: bool = true
var server_unprocessed_events: Array = []
var server_unprocessed_events_by_id: Array = []
var server_unacknowledged_events_by_mup: Dictionary = {}
var server_unacknowledged_events_by_mup_by_id: Dictionary = {}
var server_untransmitted_events_history: Array = []
var server_untransmitted_events_history_by_id: Array = []
var events_unsent: Array = []
var client_unacknowledged_events: Array = []
var client_unacknowledged_events_by_id: Array = []
var client_unprocessed_events_history: Array = []
var client_unprocessed_events_history_by_id: Array = []
var events_history: Array = []
var events_history_by_id: Array = []
var event_highest_num_by_mup: Dictionary = {}
var last_time_highest_num_was_txd: int = 0
var event_counter: int = 0
const event_template: Dictionary = {"time": null, "event_num": null, "type": null, 
            "rec_by": null, "additional": {}}
var processed_event_loop_once: bool = false
var processing_start_time: int
var processing_load: int = 0
var event_processing_hooks_refs: Dictionary = {}
var sync_var_newest_update: Dictionary = {}
var pause_sync_var = false

# Prioritized Packets: Below
var pregame_ready_event_id
var pregame_ready_event
var game_started_event_id
var game_started_event

var first_event_time: int = 0
var event_time_caught_up: int
var repeated_events = {}

func _ready():
    PM.STD.connect(PM.STD.monitor_data("mups_status"), 
        self, "on_new_identified_connection"
    )
    PM.STD.connect(PM.STD.monitor_(
        "mups_reconnected"), self, "on_mup_reconnected"
    )

func _process(_delta):
    if not pause_sync_var:
        if not repeated_events.empty():
            repeated_events = {}
        if is_a_client:
            client_process_loop()
        else:
            server_process_loop()

func server_process_loop():
    processed_event_loop_once = false
    processing_load = 0
    processing_start_time = OS.get_ticks_usec()
    if events_unsent.size() > 0:
        server_transfer_event_for_processing()
    if server_unprocessed_events.size() > 0:
        server_process_an_unprocessed_event()
    if server_untransmitted_events_history.size() > 0:
        server_tx_an_event_history_to_clients()
    if OS.get_ticks_usec() - last_time_highest_num_was_txd > 50000:
        last_time_highest_num_was_txd = OS.get_ticks_usec()
        if server_unacknowledged_events_by_mup.size() > 0:
            server_tx_unack_event_to_clients()
    processed_event_loop_once = true
    
func client_process_loop():
    processed_event_loop_once = false
    processing_load = 0
    processing_start_time = OS.get_ticks_usec()
    if events_unsent.size() > 0:
        client_tx_event_to_server()
    if client_unprocessed_events_history.size() > 0:
        client_process_an_event_history()
    if OS.get_ticks_usec() - last_time_highest_num_was_txd > 50000:
        last_time_highest_num_was_txd = OS.get_ticks_usec()
        if client_unacknowledged_events.size() > 0:
            client_tx_unack_event_to_server()
    processed_event_loop_once = true

func server_process_an_unprocessed_event():
    processing_load += 1
    var event = server_unprocessed_events.pop_front()
    var event_id = server_unprocessed_events_by_id.pop_front()
    if not events_history_by_id.has(event_id):
        if not unit_testing:
            if PM.STD.get_("is_host") == 2:
                PM.Log("Server Processing Event : " + str(event), "testing")
        if event["type"] == "sync_var":
            pass  # The server creates sync_var events, it does not process them.
        elif event["type"] == "server_request":
            pass
        elif event["type"] == "client_request":
            var object = event["additional"]["object"]
            if event_processing_hooks_refs.has(object):
                var object_ref = event_processing_hooks_refs[object]
                object_ref.call_deferred("process_event_client_request", event)
        else:
            for object in event_processing_hooks_refs:
                var object_ref = event_processing_hooks_refs[object]
                object_ref.call_deferred("process_event_all_other_events", event)
        # Now put the event into events_history.
        events_history.append(event)
        events_history_by_id.append(event_id)
        # Make sure we add it to the server_untransmitted_events_history
        server_untransmitted_events_history.append(event)
        server_untransmitted_events_history_by_id.append(event_id)
       
func client_process_an_event_history():
    processing_load += 1
    var event = client_unprocessed_events_history.pop_front()
    var event_id = client_unprocessed_events_history_by_id.pop_front()
    if event["type"] == "sync_var":
        process_event_sync_var(event)
    elif event["type"] == "client_request":
        pass
    elif event["type"] == "server_request":
        var mup = event["additional"]["mup"]
        var apply_the_request = false
        if mup == "all":
            apply_the_request = true
        elif mup == PM.unique_id:
            apply_the_request = true
        if apply_the_request:   
            var object = event["additional"]["object"]
            if event_processing_hooks_refs.has(object):
                var object_ref = event_processing_hooks_refs[object]
                object_ref.call("process_event_server_request", event)
    else:
        for object in event_processing_hooks_refs:
            var object_ref = event_processing_hooks_refs[object]
            object_ref.call("process_event_all_other_events", event)
    # Now put the event into events_history.
    if not events_history_by_id.has(event_id):
        events_history.append(event)
        events_history_by_id.append(event_id)
                
func get_event_id_from_event(event):
    return event["rec_by"] + "-" + str(event["event_num"])

func record_event(event_type, event_additional, event_time):
    processing_load += 1
    var new_event = event_template.duplicate()
    new_event["time"] = event_time
    new_event["event_num"] = event_counter
    event_counter += 1
    new_event["type"] = event_type
    new_event["additional"] = event_additional
    new_event["rec_by"] = PM.unique_id
    if unit_testing:
        new_event["rec_by"] = ut_server_mup_id
    if new_event["additional"].has("var_name"):
        if new_event["additional"]["var_name"] == "pregame_settings_completed":
            pregame_ready_event_id = get_event_id_from_event(new_event)
            pregame_ready_event = new_event
    elif new_event["additional"].has("method"):
        if new_event["additional"]["method"] == "remote_start_game_start_delay":
            game_started_event_id = get_event_id_from_event(new_event)
            game_started_event = new_event
    events_unsent.append(new_event)

func client_tx_event_to_server():
    processing_load += 1
    var event = events_unsent.pop_front()
    var event_id = get_event_id_from_event(event)
    if not unit_testing:
        if (PM.get_tree().network_peer.get_connection_status() == 
            PM.get_tree().network_peer.CONNECTION_CONNECTED
        ):
            if 1 in get_tree().get_network_connected_peers():
                rpc_id(1, "server_rx_event_from_client", event)
    client_unacknowledged_events.append(event)
    client_unacknowledged_events_by_id.append(event_id)
    
remote func server_rx_event_from_client(event):
    processing_load += 1
    var rpc_id
    var mup_id
    if unit_testing:
        rpc_id = ut_client1_rpc_id
        mup_id = ut_client1_mup_id
    else:
        rpc_id = get_tree().get_rpc_sender_id()
        mup_id = PM.STD.get_data("peers_to_mups")[rpc_id]
    var already_have_it = false
    var event_id = get_event_id_from_event(event)
    if repeated_events.has(event_id):
        repeated_events[event_id] += 1
    else:
        repeated_events[event_id] = 1
    if mup_id != event["rec_by"]:  # Forged event, so toss.
        already_have_it = true
    #PM.Log("Server Recieving Event : " + str(event), "testing")
    if not already_have_it:
        if server_unprocessed_events_by_id.has(event_id):
            already_have_it = true
        if not already_have_it:
            if events_history_by_id.has(event_id):
                already_have_it = true
    if not already_have_it:
        server_unprocessed_events.append(event)
        server_unprocessed_events_by_id.append(event_id)
    if not unit_testing:
        if (PM.get_tree().network_peer.get_connection_status() == 
            PM.get_tree().network_peer.CONNECTION_CONNECTED
        ):
            if rpc_id in get_tree().get_network_connected_peers():
                rpc_id(rpc_id, "client_rx_ackn_from_server", event_id)
     
remote func client_rx_ackn_from_server(event_id):
    processing_load += 1
    if client_unacknowledged_events_by_id.has(event_id):
        var index = client_unacknowledged_events_by_id.find(event_id)
        client_unacknowledged_events_by_id.remove(index)
        client_unacknowledged_events.remove(index)

func server_transfer_event_for_processing():
    processing_load += 1
    var event = events_unsent.pop_front()
    var event_id = get_event_id_from_event(event)
    server_unprocessed_events.append(event)
    server_unprocessed_events_by_id.append(event_id)

func server_tx_an_event_history_to_clients():
    processing_load += 1
    var event = server_untransmitted_events_history.pop_front()
    var event_id = server_untransmitted_events_history_by_id.pop_front()
    var already_added_to_unacknowledged = false
    var all_mups
    var server_mup
    server_mup = PM.unique_id
    if unit_testing:
        all_mups = [ut_server_mup_id, ut_client1_mup_id, ut_client2_mup_id]
        server_mup = ut_server_mup_id
    elif PM.run_ITM and PM.ITM_act_as_server:
        all_mups = PM.STD.get_data("mups_to_peers").keys()
    else:
        all_mups = PM.STD.get_data("mups_to_peers").keys()
    for mup in all_mups:
        if mup != server_mup:
            if not server_unacknowledged_events_by_mup.has(mup):
                server_unacknowledged_events_by_mup[mup] = [event]
                server_unacknowledged_events_by_mup_by_id[mup] = [event_id]
            else:
                if not server_unacknowledged_events_by_mup_by_id[mup].has(event_id):
                    server_unacknowledged_events_by_mup[mup].append(event)
                    server_unacknowledged_events_by_mup_by_id[mup].append(event_id)
                else:
                    already_added_to_unacknowledged = true
    if not unit_testing:
        if not already_added_to_unacknowledged:
            if get_tree().network_peer != null:
                if (PM.get_tree().network_peer.get_connection_status() == 
                    PM.get_tree().network_peer.CONNECTION_CONNECTED
                ):
                    rpc("client_rx_an_event_history_from_server", event)
    
remote func client_rx_an_event_history_from_server(event):
    processing_load += 1
    var rpc_id
    if unit_testing:
        rpc_id = ut_server_rpc_id
    else:
        rpc_id = get_tree().get_rpc_sender_id()
    if rpc_id != 1:  # Not the server.
        return
    var already_have_it = false
    var event_id = get_event_id_from_event(event)
    if repeated_events.has(event_id):
        repeated_events[event_id] += 1
    else:
        repeated_events[event_id] = 1
    #PM.Log("Client Recieving an Event : " + str(event), "testing")
    if client_unprocessed_events_history_by_id.has(event_id):
        already_have_it = true
    if not already_have_it:
        if events_history_by_id.has(event_id):
            already_have_it = true
    if not already_have_it:
        client_unprocessed_events_history.append(event)
        client_unprocessed_events_history_by_id.append(event_id)
    if not unit_testing:
        if (PM.get_tree().network_peer.get_connection_status() == 
            PM.get_tree().network_peer.CONNECTION_CONNECTED
        ):
            if rpc_id in get_tree().get_network_connected_peers():
                rpc_id(1, "server_rx_ackn_from_client", event_id)

remote func server_rx_ackn_from_client(event_id):
    processing_load += 1
    var rpc_id
    var mup_id
    if unit_testing:
        rpc_id = ut_client1_rpc_id
        mup_id = ut_client1_mup_id
    else:
        rpc_id = get_tree().get_rpc_sender_id()
        mup_id = PM.STD.get_data("peers_to_mups")[rpc_id]
    if server_unacknowledged_events_by_mup_by_id.has(mup_id):
        if server_unacknowledged_events_by_mup_by_id[mup_id].has(event_id):
            var index = server_unacknowledged_events_by_mup_by_id[mup_id].find(event_id)
            server_unacknowledged_events_by_mup_by_id[mup_id].remove(index)
            server_unacknowledged_events_by_mup[mup_id].remove(index)
            if server_unacknowledged_events_by_mup[mup_id].size() == 0:
                # warning-ignore:RETURN_VALUE_DISCARDED
                server_unacknowledged_events_by_mup.erase(mup_id)
                # warning-ignore:RETURN_VALUE_DISCARDED
                server_unacknowledged_events_by_mup_by_id.erase(mup_id)

func client_tx_unack_event_to_server():
    # This function perserves the order of the events.
    processing_load += 1
    if not unit_testing:
        if (PM.get_tree().network_peer.get_connection_status() == 
            PM.get_tree().network_peer.CONNECTION_CONNECTED
        ):
            if 1 in get_tree().get_network_connected_peers():
                rpc_id(1, "server_rx_event_from_client", client_unacknowledged_events[0])

func server_tx_unack_event_to_clients():
    processing_load += 1
    # This function perserves the order of the events.
    for mup in server_unacknowledged_events_by_mup.keys():
        var rpc_id = PM.STD.get_data("mups_to_peers")[mup]
        var event = server_unacknowledged_events_by_mup[mup][0]
        if (PM.get_tree().network_peer.get_connection_status() == 
            PM.get_tree().network_peer.CONNECTION_CONNECTED
        ):
            if rpc_id in get_tree().get_network_connected_peers():
                rpc_id(rpc_id, "client_rx_an_event_history_from_server", event)

func process_event_sync_var(event):
    var discard = false
    if PM.STD.get_("is_host") == 0:
        if sync_var_newest_update.has(event["additional"]["var_name"]):
            if event["time"] < sync_var_newest_update[event["additional"]["var_name"]]:
                discard = true
        if not discard:
            PM.Log("process_event_syc_var() " + str(event))
            sync_var_newest_update[event["additional"]["var_name"]] = event["time"]
            PM.STD.set_(event["additional"]["var_name"], 
                    event["additional"]["var_val"])

func on_mup_reconnected(mups_reconnected):
    var execute = false
    if PM.STD.get_("is_host") != null:
        if PM.STD.get_("is_host") >= 1:
            execute = true
    if execute:
        var mup_id
        if mups_reconnected.size() < 1:
            execute = false
        if execute:
            mup_id = mups_reconnected[0]
        if PM.run_ITM and PM.ITM_act_as_server:
            if mup_id == PM.unique_id:
                execute = false
        elif mup_id == PM.unique_id:
            execute = false
        if mup_id == null:
            execute = false
        if server_unacknowledged_events_by_mup.has(mup_id):
            execute = false
        if execute:
            var events_hist_dup = events_history.duplicate(true)
            var events_hist_by_id_dup = events_history_by_id.duplicate(true)
            server_unacknowledged_events_by_mup[mup_id] = events_hist_dup
            server_unacknowledged_events_by_mup_by_id[mup_id] = events_hist_by_id_dup

func on_new_identified_connection(mups_status):
    var execute = false
    if PM.STD.get_("is_host") != null:
        if PM.STD.get_("is_host") >= 1:
            execute = true
    if execute:
        for mup_id in mups_status.keys():
            if PM.run_ITM and PM.ITM_act_as_server:
                if mup_id == PM.unique_id:
                    execute = false
            elif mup_id == PM.unique_id:
                execute = false
            if server_unacknowledged_events_by_mup.has(mup_id):
                execute = false
            if execute:
                var events_hist_dup = events_history.duplicate(true)
                var events_hist_by_id_dup = events_history_by_id.duplicate(true)
                # Below is the re-ordering of events being sent to prioritize 
                # the pre-game being ready and the start of the game having happened.
                if events_hist_by_id_dup.has(game_started_event_id):
                    var index = events_hist_by_id_dup.find(game_started_event_id)
                    events_hist_by_id_dup.remove(index)
                    events_hist_dup.remove(index)
                    events_hist_by_id_dup.push_front(game_started_event_id)
                    events_hist_dup.push_front(game_started_event)
                if events_hist_by_id_dup.has(pregame_ready_event_id):
                    var index = events_hist_by_id_dup.find(pregame_ready_event_id)
                    events_hist_by_id_dup.remove(index)
                    events_hist_dup.remove(index)
                    events_hist_by_id_dup.push_front(pregame_ready_event_id)
                    events_hist_dup.push_front(pregame_ready_event)
                server_unacknowledged_events_by_mup[mup_id] = events_hist_dup
                server_unacknowledged_events_by_mup_by_id[mup_id] = events_hist_by_id_dup
            execute = true
