extends Node

#"state_has" variables are for the types of functions that would be used in this state
export(bool) var state_has_on_process:bool = false #Should be "false" if not used
export(bool) var state_has_on_physics_process:bool = false #Should be "false" if not used
#Should be "false" if not used, May be used for mouse or button tracking if PC use is expanded in the future
export(bool) var state_has_on_input:bool = false
#Should be "false" if not used, Used for more global inputs (i.e. screen swipe that 
# aren't directly associated with a specific control/button)
export(bool) var state_has_on_unhandled_input:bool = false 
export(String) var STATE_ID:String = "Template" #Name of the scene that corresponds to this State Class
export(Array, String) var transitionable_states: Array = [] 
export(Array, String, FILE) var resources_to_preload: Array = []
export(String, FILE, "*.tscn,*.scn") var my_screen_file: String
# other_screens_to_load will get put into Container current_scene
export(Array, String, FILE, "*.tscn,*.scn") var other_screens_to_load: Array = []  
export(Array, String, FILE, "*.tscn,*.scn") var super_systems_to_load: Array = []  
export(Array, String, FILE) var other_resources_to_load: Array = []
export(GDScript) var custom_event_overrides
export(int) var editor_yield_duration: int = 60
var required_for_state_is_complete = false #Initial value, Guard variable
var loading_next_state_already = false #Initial value, Guard variable
var started_background_loading = false #Initial value, Guard variable
var my_screen_offset: String = "0,0" #Initial value
var my_screen_name: String = ""
var my_screen_ref: Node
var next_state: String
var thread_num
var resources_not_preloaded: Array
var other_screens_not_loaded: Array
var super_systems_not_loaded: Array
var other_resources_not_loaded: Array
# Loading stages
var total_stages:float = 0
var completed_stages:float = 0
var last_file_loaded: String

func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        on_real_ready()

func _process(delta) -> void:
    if state_has_on_process:
        var val = on_process_event(delta)
        if val != STATE_ID:
            PM.call_deferred("evaluate_state_change", val)

func _physics_process(delta):
    if state_has_on_physics_process:
        var val = on_physics_process_event(delta)
        if val != STATE_ID:
            PM.call_deferred("evaluate_state_change", val)

func _input(event):
    if state_has_on_input:
        var val = on_input_event(event)
        if val != STATE_ID:
            PM.call_deferred("evaluate_state_change", val)

func _unhandled_input(event):
    if state_has_on_unhandled_input:
        var val = on_unhandled_input_event(event)
        if val != STATE_ID:
            PM.call_deferred("evaluate_state_change", val)

func on_editor_ready() -> void:
    pass

func on_real_ready() -> void:
    name = STATE_ID
    custom_event_overrides = custom_event_overrides.new()
    custom_event_overrides.state_ref = self
    # The below code is to work around this bug:
    # https://github.com/godotengine/godot/issues/16478
    if resources_to_preload.empty():
        resources_to_preload = [].duplicate()
    if other_screens_to_load.empty():
        other_screens_to_load = [].duplicate()
    if super_systems_to_load.empty():
        super_systems_to_load = [].duplicate()
    if other_resources_to_load.empty():
        other_resources_to_load = [].duplicate()
    # End workaround
    var val = custom_event_overrides.on_ready()
    if val != STATE_ID:
            PM.call_deferred("evaluate_state_change", val)

func editor_yield():
    for _i in range(editor_yield_duration):
        yield(get_tree(), "idle_frame") 
       
func on_enter(_previous_state) -> void:
    #state_num is used for on_process_event()->on_unhandeled_input_event(), etc returned values to PM
    PM.Log("Program Manager State = " + STATE_ID, "info") #Info only
    var val = custom_event_overrides.on_enter(_previous_state)
    if val != STATE_ID:
        PM.call_deferred("evaluate_state_change", val)
        return
    PM.STD.connect(PM.STD.monitor_(
            "ProgramManager_state_trigger"), self, "state_trigger") #General use (generic) trigger
    #Control to prevent the user from navigating to other screens without the State Class being aware
    PM.STD.connect(PM.STD.monitor_("current_screen"), self, "screen_change")
    load_required_for_state()
    screen_change(null)

func on_exit() -> void: #Perform general cleanup of this State Class
    #Remove connections that were used for this State Class
    if PM.STD.is_connected(
        PM.STD.monitor_("current_screen"), self, "screen_change"
    ):
        PM.STD.disconnect(
            PM.STD.monitor_("current_screen"), self, "screen_change"
        )
    if PM.STD.is_connected(PM.STD.monitor_(
            "ProgramManager_state_trigger"), self, "state_trigger"
    ):
        PM.STD.disconnect(PM.STD.monitor_(
                "ProgramManager_state_trigger"), self, "state_trigger"
        )
    custom_event_overrides.on_exit()
    #Perform general cleanup
    call_deferred("cleanup")

func state_trigger(new_value): #Function for general-use trigger
    if new_value != null:
        var val = custom_event_overrides.on_state_trigger(new_value)
        if val != STATE_ID:  # Change states.
            PM.call_deferred("evaluate_state_change", val)

func on_process_event(delta) -> String:
    if custom_event_overrides.has_method("on_process_event"):
        return custom_event_overrides.on_process_event(delta)
    return STATE_ID
    
func on_physics_process_event(delta) -> String:
    if custom_event_overrides.has_method("on_physics_process_event"):
        return custom_event_overrides.on_physics_process_event(delta)
    return STATE_ID
        
func on_input_event(event) -> String:
    if custom_event_overrides.has_method("on_input_event"):
        return custom_event_overrides.on_input_event(event)
    return STATE_ID

func on_unhandled_input_event(event) -> String:
    if custom_event_overrides.has_method("on_unhandled_input_event"):
        return custom_event_overrides.on_unhandled_input_event(event)
    return STATE_ID

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    # Guard against switching and screen switch fighting if we have not yet entered this state.
    if PM.current_state != STATE_ID:
        pass
    else:
        if current_screen == null:
            if my_screen_offset == "":
                pass
            else:
                if PM.STD.get_("current_screen") != my_screen_offset:
                    PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        else:
            if my_screen_offset == "":
                pass
            else:
                if PM.STD.get_("current_screen") != my_screen_offset:
                    PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        
func load_required_for_state():
    if not started_background_loading: 
        started_background_loading = true
        calc_total_stages()
        if custom_event_overrides.has_method("on_preloading_start"):
            var potential_function_state = custom_event_overrides.on_preloading_start()
            if potential_function_state is GDScriptFunctionState: # Still working.
                potential_function_state = yield(potential_function_state, "completed")
            calc_total_stages()
            update_progress()
        call_deferred("load_preload_resources")
    
func load_preload_resources():    
    if resources_to_preload.size() > 0:
        resources_not_preloaded = resources_to_preload.duplicate()
        call_deferred("loop_preload_resources")
    else:
        if custom_event_overrides.has_method("on_preloading_complete"):
            var potential_function_state = custom_event_overrides.on_preloading_complete()
            if potential_function_state is GDScriptFunctionState: # Still working.
                potential_function_state = yield(potential_function_state, "completed")
        update_progress()
        call_deferred("load_my_screen")

func loop_preload_resources():
    if resources_not_preloaded.size() > 0:
        last_file_loaded = resources_not_preloaded.pop_front()
        PM.LoadingScreen.item_name(last_file_loaded)
        PM.BGLoad.load_n_callback(last_file_loaded, self, "setup_preloaded_resource")
    else:
        if custom_event_overrides.has_method("on_preloading_complete"):
            var potential_function_state = custom_event_overrides.on_preloading_complete()
            if potential_function_state is GDScriptFunctionState: # Still working.
                potential_function_state = yield(potential_function_state, "completed")
        update_progress()
        call_deferred("load_my_screen")

func setup_preloaded_resource(results):
    if results[0] == "finished":
        completed_stages += 1
        var loaded_resource = results[1]
        if custom_event_overrides.has_method("on_resource_preloaded"):
            custom_event_overrides.on_resource_preloaded(completed_stages, 
                total_stages, last_file_loaded, loaded_resource
            )
        calc_total_stages()
        update_progress()
        call_deferred("loop_preload_resources") # Wait 1 frame for on_ready and drawing

func load_my_screen():
    if my_screen_file == "":
        call_deferred("load_other_screens")
    else:
        #Loads the scene that corresponds to this State Class
        PM.LoadingScreen.item_name(my_screen_file)
        last_file_loaded = my_screen_file
        PM.LoadingScreen.item_name(last_file_loaded)
        PM.BGLoad.load_n_callback(last_file_loaded, self, "setup_my_screen")  

#When the resource is ready, set it up and draw it to screen  
func setup_my_screen(results): 
    if results[0] == "finished":
        completed_stages += 1
        update_progress()
        custom_event_overrides.on_stage_loaded(completed_stages, total_stages, last_file_loaded)
        var loaded_scene = results[1].instance()
        my_screen_ref = loaded_scene
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.TheContainer.current_scene.add_child(
                my_screen_ref) #Adding scene to the view and running the screen/scene
        call_deferred("load_other_screens") #Wait 1 frame for on_ready and drawing

func load_other_screens():
    if other_screens_to_load.size() > 0:
        other_screens_not_loaded = other_screens_to_load.duplicate()
        loop_load_other_screens()
    else:
        load_super_systems()

func loop_load_other_screens():
    if other_screens_not_loaded.size() > 0:
        last_file_loaded = other_screens_not_loaded.pop_front()
        PM.LoadingScreen.item_name(last_file_loaded)
        PM.BGLoad.load_n_callback(last_file_loaded, self, "setup_other_screen")
    else:
        load_super_systems()

func setup_other_screen(results):
    if results[0] == "finished":
        completed_stages += 1
        update_progress()
        custom_event_overrides.on_stage_loaded(completed_stages, total_stages, last_file_loaded)
        var loaded_scene = results[1].instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene) #Adding scene to the view and running the screen/scene
        call_deferred("loop_load_other_screens") #Wait 1 frame for on_ready and drawing
    
func load_super_systems():
    if super_systems_to_load.size() > 0:
        super_systems_not_loaded = super_systems_to_load.duplicate()
        loop_load_super_systems()
    else:
        load_other_resources()

func loop_load_super_systems():
    if super_systems_not_loaded.size() > 0:
        last_file_loaded = super_systems_not_loaded.pop_front()
        PM.LoadingScreen.item_name(last_file_loaded)
        PM.BGLoad.load_n_callback(last_file_loaded, self, "setup_super_system")
    else:
        load_other_resources()

func setup_super_system(results):
    if results[0] == "finished":
        completed_stages += 1
        update_progress()
        custom_event_overrides.on_stage_loaded(completed_stages, total_stages, last_file_loaded)
        var loaded_scene = results[1].instance()
        if not PM.SS.has(loaded_scene.SuperSystemID):
            PM.get_tree().root.get_node("PM/SS").add_child(
                    loaded_scene) #Adding scene to the view and running the screen/scene
        call_deferred("loop_load_super_systems") #Wait 1 frame for on_ready and drawing 
       
func load_other_resources():
    if other_resources_to_load.size() > 0:
        other_resources_not_loaded = other_resources_to_load.duplicate()
        call_deferred("loop_load_other_resources")
    else:
        var potential_state_change = custom_event_overrides.on_loading_complete()
        if potential_state_change is GDScriptFunctionState: # Still working.
            potential_state_change = yield(potential_state_change, "completed")
        update_progress(100.0)
        PM.LoadingScreen.fade_out()
        required_for_state_is_complete = true
        yield(PM.get_tree(), "idle_frame")
        screen_change(null)
        if potential_state_change != STATE_ID:
            next_state = potential_state_change
            PM.call_deferred("evaluate_state_change", next_state)

func loop_load_other_resources():
    if other_resources_not_loaded.size() > 0:
        last_file_loaded = other_resources_not_loaded.pop_front()
        PM.LoadingScreen.item_name(last_file_loaded)
        PM.BGLoad.load_n_callback(last_file_loaded, self, "setup_other_resource")
    else:
        update_progress(100.0)
        PM.LoadingScreen.fade_out()
        required_for_state_is_complete = true
        yield(PM.get_tree(), "idle_frame")
        screen_change(null)
        var potential_state_change = custom_event_overrides.on_loading_complete()
        if potential_state_change != STATE_ID:
            next_state = potential_state_change
            PM.call_deferred("evaluate_state_change", next_state)

func setup_other_resource(results):
    if results[0] == "finished":
        completed_stages += 1
        update_progress()
        var loaded_resource = results[1]
        custom_event_overrides.on_resource_loaded(completed_stages, total_stages, 
                last_file_loaded, loaded_resource)
        call_deferred("loop_load_other_resources") # Wait 1 frame for on_ready and drawing     
        
# This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
func transition_to_next_state():  
    PM.call_deferred("evaluate_state_change", next_state)

func cleanup():
    custom_event_overrides.on_cleanup()
    yield(PM.get_tree(), "idle_frame")
    #Deletes the screen/scene
    if my_screen_file != "":
        
        if PM.get_tree().root.get_node("Container").current_scene.has_node(my_screen_name):
            if my_screen_ref != null:
                my_screen_ref.queue_free() 
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
    queue_free()

func calc_total_stages():
    var myscreen_count = 0
    if my_screen_file != "":
        myscreen_count = 1
    total_stages = (resources_to_preload.size() + other_screens_to_load.size() + 
        super_systems_to_load.size() + other_resources_to_load.size() + 
        myscreen_count
    )

func update_progress(forced_val=-1):
    if forced_val == -1:
        var stages_percent_comp = 0.0
        if total_stages != 0:
            stages_percent_comp = (completed_stages / total_stages * 100.0)
        PM.LoadingScreen.progress(stages_percent_comp - 
            custom_event_overrides.minus_load_percentage
        )
    else:
        PM.LoadingScreen.progress(forced_val)
