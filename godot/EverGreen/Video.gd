extends Resource

func get_display_metrics():
    var disp_mets = {"Display":OS.get_screen_size(), 
        "DecoratedWindow": OS.get_real_window_size(), 
        "WindowDrawArea": OS.get_window_size()
    }
    disp_mets["ProjectWidth"] = ProjectSettings.get_setting("display/window/size/width")
    disp_mets["ProjectHeight"] = ProjectSettings.get_setting("display/window/size/height")
    disp_mets["ProjectTestWidth"] = ProjectSettings.get_setting("display/window/size/test_width")
    disp_mets["ProjectTestHeight"] = ProjectSettings.get_setting("display/window/size/test_height")
    disp_mets["DisplaySafeArea"] = OS.get_window_safe_area()
    disp_mets["ViewPortRect"] = PM.get_viewport_rect()
    disp_mets["ViewPortSize"] = Vector2(disp_mets["ViewPortRect"].size.x, 
        disp_mets["ViewPortRect"].size.y
    )
    return disp_mets

func print_display_metrics():
    var disp_mets = get_display_metrics()
    print("    [Display Metrics]")
    print("    Actual Hardware device display size: ", disp_mets["Display"])
    print("    Device Display Safe Area as defined by the OS: ", 
        disp_mets["DisplaySafeArea"]
    )
    print("    Current View Port Rect that Program Manager is rendered in: ",
        disp_mets["ViewPortRect"]
    )
    print("    Current View Port Size that Program Manager is rendered in:",
        disp_mets["ViewPortSize"]
    )
    print("    Decorated Window size: ", disp_mets["DecoratedWindow"])
    print("    Window Draw Area size: ", disp_mets["WindowDrawArea"])
    print("    Project Settings: Width=", disp_mets["ProjectWidth"], 
        " Height=", disp_mets["ProjectHeight"]
    ) 
    print("    Project Test Settings: Width=", disp_mets["ProjectTestWidth"], 
        " Height=", disp_mets["ProjectTestHeight"]
    )
