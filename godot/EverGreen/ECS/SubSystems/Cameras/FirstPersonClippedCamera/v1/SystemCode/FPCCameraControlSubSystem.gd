extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const COMPONENT_SYSTEM_ID = "FPCCameraControl"
const VERSION = 2
# All other variables should be defined in your components unless they are
# global for all of the same components.
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
var trigger_one_time = false

export(bool) var FirstPersonClippedCameraControl_SubSystem
# Declare system global/shared component Export variables here.

func on_ready(component_ref) -> void:
    if component_ref.TheCamera.current:
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    component_ref.CameraSpring.spring_length = component_ref.CameraRig.translation.z
    component_ref.CameraRig.translation.z = 0
    component_ref.CameraSpring.rotation_degrees.x = component_ref.CameraRig.rotation_degrees.x
    component_ref.CameraRig.rotation_degrees.x = 0

func on_process(delta, component_ref) -> void:
    if component_ref.TheCamera.current:
        if not component_ref.is_current:
            component_ref.is_current = true
            if component_ref.character_model_ref != null:
                component_ref.character_model_ref.set_layer_mask_bit(0, false)
                component_ref.character_model_ref.set_layer_mask_bit(19, true)
            if component_ref.first_person_model_ref != null:
                component_ref.first_person_model_ref.set_layer_mask_bit(0, true)
                component_ref.first_person_model_ref.set_layer_mask_bit(19, false)
        if component_ref._input_relative.length() > 0:
            update_rotation(component_ref._input_relative * 
                    component_ref.mouse_sensitivity * delta, delta, component_ref)
            component_ref._input_relative = Vector2.ZERO
        else:
            var look_direction := get_look_direction()
            if look_direction.length() > 0:
                update_rotation(look_direction * component_ref.gamepad_sensitivity 
                        * delta, delta, component_ref)
        component_ref.CameraRig.rotation.y = wrapf(component_ref.CameraRig.rotation.y, -PI, PI)
    else:
        if component_ref.is_current:
            component_ref.is_current = false
            if component_ref.character_model_ref != null:
                component_ref.character_model_ref.set_layer_mask_bit(0, true)
                component_ref.character_model_ref.set_layer_mask_bit(19, false)
            if component_ref.first_person_model_ref != null:
                component_ref.first_person_model_ref.set_layer_mask_bit(0, false)
                component_ref.first_person_model_ref.set_layer_mask_bit(19, true)
    
func on_physics_process(_delta, _component_ref) -> void:
    pass
        
func on_input(_event, _component_ref) -> void:
    pass

func on_unhandled_input(event, component_ref) -> void:
    if component_ref.TheCamera.current:
        if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
            component_ref._input_relative += event.get_relative() 
        if Input.is_action_pressed("ui_cancel"):
            if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
                Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    if not component_ref.is_the_only_camera:
        if Input.is_action_pressed("scroll_up"):
            component_ref.TheCamera.current = true
        if Input.is_action_pressed("scroll_down"):
            component_ref.TheCamera.current = false

func update_rotation(amount_to_rotate: Vector2, _delta: float, component_ref) -> void:
    component_ref.parent_entity.orthonormalize()
    # Rotates around the center point of the body.
    component_ref.parent_entity.rotate_y(-amount_to_rotate.x)  
    component_ref.CameraRig.orthonormalize()
    component_ref.PitchRotation.orthonormalize()
    if component_ref.invert_look_up_down:
        component_ref.PitchRotation.rotation.x += amount_to_rotate.y
    else:
        component_ref.PitchRotation.rotate_x(amount_to_rotate.y * -1.0)
    component_ref.PitchRotation.rotation.x = clamp(component_ref.PitchRotation.rotation.x, 
                    deg2rad(component_ref.MAX_LOOK_DOWN_ANGLE), deg2rad(component_ref.MAX_LOOK_UP_ANGLE))
    component_ref.PitchRotation.rotation.z = 0
    

static func get_look_direction() -> Vector2:
    return Vector2(Input.get_action_strength("look_right") - Input.get_action_strength("look_left"), 
        Input.get_action_strength("look_up") - Input.get_action_strength("look_down")).normalized()

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration(component_ref):
    if not component_ref.parent_entity.has_node("FPCCameraRig"):
        print("First Person Clipped Camera Component: Parent entity node must" +
            " have the 'FPCCameraRig.tscn' node as a child. It is located at " +
            "'res://Logic/Components/Cameras/FirstPersonClippedCamera/v1/FPC" +
            "CameraRig.tscn', Please add it.")
        return false
    else:
        component_ref.CameraRig = component_ref.parent_entity.get_node("FPCCameraRig")
        component_ref.PitchRotation = component_ref.parent_entity.get_node("FPCCameraRig/PitchRotation")
        component_ref.CameraSpring = component_ref.parent_entity.get_node("FPCCameraRig/PitchRotation/SpringArm")
        component_ref.TheCamera = component_ref.parent_entity.get_node("FPCCameraRig/PitchRotation/SpringArm/"+
                "ClippedCamera")
        if component_ref.make_currently_active_camera:
            component_ref.TheCamera.current = true
            if component_ref.parent_entity.has_node(component_ref.character_model):
                component_ref.character_model_ref = component_ref.parent_entity.get_node(
                        component_ref.character_model)
                component_ref.character_model_ref.set_layer_mask_bit(0, false)
                component_ref.character_model_ref.set_layer_mask_bit(19, true)
            if component_ref.parent_entity.has_node(component_ref.first_person_model):
                component_ref.first_person_model_ref = component_ref.parent_entity.get_node(
                        component_ref.first_person_model)
                component_ref.first_person_model_ref.set_layer_mask_bit(0, true)
                component_ref.first_person_model_ref.set_layer_mask_bit(19, false)
                
    return true
