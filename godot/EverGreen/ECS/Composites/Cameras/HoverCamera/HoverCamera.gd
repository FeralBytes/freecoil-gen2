extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const ID = "HoverCamera"
const VERSION = 1
const has_on_ready: bool = true
const has_on_process: bool = true
const has_on_physics_process: bool = false
const has_on_input: bool = false
const has_on_unhandled_input: bool = true
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
var __input_relative: Vector2 = Vector2.ZERO
# Declare system global/shared component Export variables here.
export(bool) var invert_look_up_down = false
export(bool) var invert_look_left_right = false
export(Vector2) var mouse_sensitivity: Vector2 = Vector2(0.1, 0.1)
export(Vector2) var gamepad_sensitivity: Vector2 = Vector2(1.7, 1.7)

func on_ready(par) -> void:
    if par.current:
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func on_process(par, delta) -> void:
    if par.current:
        if __input_relative.length() > 0:
            update_rotation(par, __input_relative * 
                    mouse_sensitivity * delta, delta)
            __input_relative = Vector2.ZERO
        else:
            var look_direction := get_look_direction()
            if look_direction.length() > 0:
                update_rotation(par, look_direction * gamepad_sensitivity 
                        * delta, delta)
        par.rotation.y = wrapf(par.rotation.y, -PI, PI)
    
func on_physics_process(par, _delta) -> void:
    print(ID + " on_physics_process()")
        
func on_input(par, _event) -> void:
    print(ID + " on_input()")

func on_unhandled_input(par, event) -> void:
    if par.current:
        if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
            __input_relative += event.get_relative() 
        if Input.is_action_just_pressed("ui_cancel"):
            if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
                Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
            else:
                Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration(par):
    return true

func update_rotation(par, amount_to_rotate: Vector2, _delta: float) -> void:
    par.orthonormalize()
    # Rotates around the center point of the body.
    if invert_look_left_right:
        par.rotate_y(amount_to_rotate.x)
    else:
        par.rotate_y(-amount_to_rotate.x)  
    if invert_look_up_down:
        par.rotation.x += amount_to_rotate.y
    else:
        par.rotation.x += (amount_to_rotate.y * -1.0)
    par.rotation.z = 0
    

static func get_look_direction() -> Vector2:
    return Vector2(Input.get_action_strength("look_right") - Input.get_action_strength("look_left"), 
        Input.get_action_strength("look_up") - Input.get_action_strength("look_down")).normalized()
