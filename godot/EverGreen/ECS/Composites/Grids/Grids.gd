extends Node
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const ID = "Grids"
const VERSION = 1
const has_on_ready: bool = true
const has_on_process: bool = true
const has_on_physics_process: bool = false
const has_on_input: bool = false
const has_on_unhandled_input: bool = false
const unique_composite: bool = true
var par: Node
# All other variables should be defined in your components unless they are
# global for all of the same components.
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.

var __grid: Array
var size
var _total_size
var world_start

func _ready():
    on_ready()

func on_ready() -> void:
    PM.SS[ID] = self

func on_process(_delta) -> void:
    pass
    
func on_physics_process(_delta) -> void:
    print(ID + " on_physics_process()")
        
func on_input(_event) -> void:
    print(ID + " on_input()")

func on_unhandled_input(_event) -> void:
    print(ID + " on_unhandled_input()")

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration():
    return true

func create_grid(width, world_start_pos):
    # Each point given can be split into a float, but we use the points as the
    # four corners of the grid. The center location is the center of the face 
    # of the grid.
    world_start =  world_start_pos
    size = width
    __grid = PM.Misc.create_2d_array(width, width)
    for x in range(0, __grid.size()):
        for y in range(0, __grid[x].size()):
#            var world_center = Vector2(world_start.x + x + 0.5, world_start.y + y + 0.5)  # Compute as needed.
            __grid[x][y] = {"id": Vector2(x,y), "on_tile":[], "tile_type": null}

func get_tile_world_pos(tile:Vector2) -> Vector2:
    var world_center = Vector2(world_start.x + tile.x + 0.5, world_start.y + tile.y + 0.5)
    return world_center

func get_neighbor_tiles_coords(tile:Vector2) -> Array:
    # North, East, South, West
    var nw = Vector2()
    var n = Vector2()
    var ne = Vector2()
    var e = Vector2()
    var se = Vector2()
    var s = Vector2()
    var sw = Vector2()
    var w = Vector2()
    if tile.y == 0:
        nw = null
        n = null
        ne = null
    else:
        nw = Vector2(tile.x - 1, tile.y - 1)
        n = Vector2(tile.x, tile.y - 1)
        ne = Vector2(tile.x + 1, tile.y - 1)
    if tile.x == 0:
        nw = null
        w = null
        sw = null
    else:
        nw = Vector2(tile.x - 1, tile.y - 1)
        w = Vector2(tile.x - 1, tile.y)
        sw = Vector2(tile.x - 1, tile.y + 1)
    if tile.x == size - 1:
        ne = null
        e = null
        se = null
    else:
        e = Vector2(tile.x + 1, tile.y)
        se = Vector2(tile.x + 1, tile.y + 1)
    if tile.y == size - 1:
        se = null
        s = null
        sw = null
    else:
        s = Vector2(tile.x, tile.y + 1)
    return [nw, n, ne, e, se, s, sw, w]
    
func _get_nw_neighbor(tile:Vector2) -> Vector2:
    return Vector2(tile.x - 1, tile.y - 1)

func get_ring_at_distance(center_tile:Vector2, distance:int, 
    only_if_on_tile_not_empty:bool=false
) -> Array:
    var ring:Array = []
    var max_size_int = size - 1
    if distance == 0:
        if only_if_on_tile_not_empty:
            if not get_tile(center_tile)["on_tile"].empty():
                return [center_tile]
            else:
                return []
        else:
            return [center_tile]
    else:
        var num_to_return = distance * 8
        var starting_nw_tile = _get_nw_neighbor(center_tile)
        if distance > 1:
            for i in range(1, distance):
                starting_nw_tile = _get_nw_neighbor(starting_nw_tile)
        var num_per_ring_edge = (num_to_return - 4) / 4  # Not counting the 4 corners.
        var nw = 0
        var ne = num_per_ring_edge + 1
        var se = ne + num_per_ring_edge + 1
        var sw = se + num_per_ring_edge + 1
        var nw_t:Vector2   # NorthWest tile
        var ne_t:Vector2
        var se_t:Vector2
        var sw_t:Vector2
        for i in range(num_to_return):
            if i == nw:
                nw_t = starting_nw_tile
                var append = true
                if nw_t.x > max_size_int or nw_t.y > max_size_int or nw_t.x < 0 or nw_t.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(starting_nw_tile)["on_tile"].empty():
                            ring.append(starting_nw_tile)
                    else:
                        ring.append(starting_nw_tile)
            elif i > nw and i < ne:
                var tile = Vector2(nw_t.x + i - nw, nw_t.y) 
                var append = true
                if tile.x > max_size_int or tile.y > max_size_int or tile.x < 0 or tile.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(tile)["on_tile"].empty():
                            ring.append(tile)
                    else:
                        ring.append(tile)
            elif i == ne:
                ne_t = Vector2(nw_t.x + i - nw, nw_t.y)
                var append = true
                if ne_t.x > max_size_int or ne_t.y > max_size_int or ne_t.x < 0 or ne_t.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(ne_t)["on_tile"].empty():
                            ring.append(ne_t)
                    else:
                        ring.append(ne_t)
            elif i > ne and i < se:
                var tile = Vector2(ne_t.x, ne_t.y + i - ne)
                var append = true
                if tile.x > max_size_int or tile.y > max_size_int or tile.x < 0 or tile.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(tile)["on_tile"].empty():
                            ring.append(tile)
                    else:
                        ring.append(tile)
            elif i == se:
                se_t = Vector2(ne_t.x, ne_t.y + i - ne)
                var append = true
                if se_t.x > max_size_int or se_t.y > max_size_int or se_t.x < 0 or se_t.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(se_t)["on_tile"].empty():
                            ring.append(se_t)
                    else:
                        ring.append(se_t)
            elif i > se and i < sw:
                var tile = Vector2(se_t.x - i + se, se_t.y)
                var append = true
                if tile.x > max_size_int or tile.y > max_size_int or tile.x < 0 or tile.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(tile)["on_tile"].empty():
                            ring.append(tile)
                    else:
                        ring.append(tile)
            elif i == sw:
                sw_t = Vector2(se_t.x - i + se, se_t.y)
                var append = true
                if sw_t.x > max_size_int or sw_t.y > max_size_int or sw_t.x < 0 or sw_t.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(sw_t)["on_tile"].empty():
                            ring.append(sw_t)
                    else:
                        ring.append(sw_t)
            elif i > sw:
                var tile = Vector2(sw_t.x, sw_t.y - i + sw)
                var append = true
                if tile.x > max_size_int or tile.y > max_size_int or tile.x < 0 or tile.y < 0:
                    append = false
                if append:
                    if only_if_on_tile_not_empty:
                        if not get_tile(tile)["on_tile"].empty():
                            ring.append(tile)
                    else:
                        ring.append(tile)
    return ring

func get_area_tiles_at_distance(center_tile:Vector2, distance:int,
    only_if_on_tile_not_empty:bool=false
) -> Array:
    var area = []
    for i in range(0, distance + 1):
        if only_if_on_tile_not_empty:
            area.append_array(get_ring_at_distance(center_tile, i, true))
        else:
            area.append_array(get_ring_at_distance(center_tile, i))
    return area

func get_tile(tile:Vector2) -> Dictionary:
    return __grid[tile.x][tile.y]

func get_tile_center_as_world(tile:Vector2) -> Vector2:
    return Vector2(tile.x + 0.5, tile.y + 0.5)
