extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const ID = "HoverWASD"
const VERSION = 1
const has_on_ready: bool = true
const has_on_process: bool = false
const has_on_physics_process: bool = true
const has_on_input: bool = false
const has_on_unhandled_input: bool = false
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
var velocity: = Vector3.ZERO
var _states:Dictionary
var __cur_state = "Idle"
var __prev_state
# Declare system global/shared component Export variables here.
export(bool) var invert_left_right = false
export(bool) var invert_forward_backward = false
export var MAX_WALK_SPEED: float = 24.0
export var WALK_ACCELERATION: float = 6.0
export var MAX_RUN_SPEED: float = 96.0
export var RUN_ACCELERATION: float = 24.0

func on_ready(par) -> void:
    _states = {"Idle": Idle.new(), 
    "WalkForward": WalkForward.new().configure(self), 
    "WalkBackward": WalkBackward.new().configure(self),
    "WalkRight": WalkRight.new().configure(self),
    "WalkLeft": WalkLeft.new().configure(self),
    "RunForward": RunForward.new().configure(self)
}
#    print(ID + " on_ready()")

func on_process(par, _delta) -> void:
    print(ID + " on_process()")
    
func on_physics_process(par, delta) -> void:
    __prev_state == __cur_state
    __cur_state = _states[__cur_state].on_physics_process(par, delta)
    if __cur_state != __prev_state:
#        print_debug(__cur_state)
        _states[__cur_state]._apply_physics(par, delta)
        
func on_input(par, event) -> void:
    pass

func on_unhandled_input(par, _event) -> void:
    print(ID + " on_unhandled_input()")

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration(par):
    return true

class Move:
    var base
    
    func configure(container_class):
        base = container_class
        return self
    
    func get_left_right_action_strength():
        var left_right_val : float = 0.0
        if base.invert_left_right:
            left_right_val = (Input.get_action_strength("move_left") -
                    Input.get_action_strength("move_right"))
        else:
            left_right_val =  (Input.get_action_strength("move_right") -
                Input.get_action_strength("move_left"))
        return left_right_val
        
    func get_forward_backward_action_strength():
        var forward_backward_val : float = 0.0
        if base.invert_forward_backward:
            forward_backward_val = (Input.get_action_strength("move_forward") -
                    Input.get_action_strength("move_backward"))
        else:
            forward_backward_val = (Input.get_action_strength("move_backward") - 
                Input.get_action_strength("move_forward"))
        return forward_backward_val

    func accelerate(par, velocity_current:Vector3, move_direction:Vector3, 
        delta:float, faster:bool
    ) -> Vector3:
        var velocity_new :Vector3
        var speed
        var max_speed
        if faster:
            speed = base.RUN_ACCELERATION
            max_speed = base.MAX_RUN_SPEED
        else:
            speed = base.WALK_ACCELERATION
            max_speed = base.MAX_WALK_SPEED 
        velocity_new = (move_direction * speed
                + velocity_current)
        if velocity_new.length() > max_speed:
            velocity_new = velocity_new.normalized() * max_speed
        velocity_new = velocity_new * delta
        par.transform.origin += velocity_new
        return velocity_new
    
    func _apply_physics(par, delta):
        var input_vector = Vector3(get_left_right_action_strength(), 
                0, get_forward_backward_action_strength()
        ).normalized()
        input_vector.y = 0
        var forwards_diff: Vector3 = par.global_transform.basis.z * input_vector.z
        var right_diff: Vector3 = par.global_transform.basis.x * input_vector.x
        var move_vector: = forwards_diff + right_diff
        if move_vector.length() > 1.0:
            move_vector = move_vector.normalized()
        base.velocity = accelerate(par, base.velocity, move_vector, delta, false)

class Idle:
    func on_physics_process(par, _delta):
        if Input.is_action_pressed("move_forward"):
            if Input.is_action_pressed("faster"):
                return "RunForward"
            else:
                return "WalkForward"
        elif Input.is_action_pressed("move_backward"):
            return "WalkBackward"
        elif Input.is_action_pressed("move_right"):
            return "WalkRight"
        elif Input.is_action_pressed("move_left"):
            return "WalkLeft"
        return "Idle"
    
    func _apply_physics(par, _delta):
        pass
    
class WalkForward extends Move:
    func on_physics_process(par, delta):
        if Input.is_action_pressed("move_forward"):
            if Input.is_action_pressed("faster"):
                return "RunForward"
            else:
                _apply_physics(par, delta)
                return "WalkForward"
        else:
            return "Idle"

class WalkBackward extends Move:
    func on_physics_process(par, delta):
        if Input.is_action_pressed("move_backward"):
            _apply_physics(par, delta)
            return "WalkBackward"
        else:
            return "Idle"

class WalkRight extends Move:
    func on_physics_process(par, delta):
        if Input.is_action_pressed("move_right"):
            _apply_physics(par, delta)
            return "WalkRight"
        else:
            return "Idle"

class WalkLeft extends Move:
    func on_physics_process(par, delta):
        if Input.is_action_pressed("move_left"):
            _apply_physics(par, delta)
            return "WalkLeft"
        else:
            return "Idle"         

class RunForward extends Move:
    func on_physics_process(par, delta):
        if Input.is_action_pressed("move_forward") and Input.is_action_pressed("faster"):
            _apply_physics(par, delta)
            return "RunForward"
        else:
            return "Idle"
    
    func _apply_physics(par, delta):
        var input_vector = Vector3(get_left_right_action_strength(), 
                0, get_forward_backward_action_strength()
        ).normalized()
        input_vector.y = 0
        var forwards_diff: Vector3 = par.global_transform.basis.z * input_vector.z
        var right_diff: Vector3 = par.global_transform.basis.x * input_vector.x
        var move_vector: = forwards_diff + right_diff
        if move_vector.length() > 1.0:
            move_vector = move_vector.normalized()
        base.velocity = accelerate(par, base.velocity, move_vector, delta, true)
