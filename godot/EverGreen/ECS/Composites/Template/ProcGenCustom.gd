extends Resource
# Technically this extends Resource, but that is the default if empty.
# All Systems must have the below vars to be valid systems.
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const ID = "Template"
const VERSION = 1
const has_on_ready: bool = true
const has_on_process: bool = true
const has_on_physics_process: bool = true
const has_on_input: bool = true
const has_on_unhandled_input: bool = true
const unique_composite: bool = true
var par: Node
# All other variables should be defined in your components unless they are
# global for all of the same components.
# Make your edits below this line.
# Declare constants here.
# Declare your system global/shared component variables here.
# Declare system global/shared component Export variables here.

func on_ready() -> void:
    print(ID + " on_ready()")

func on_process(_delta) -> void:
    print(ID + " on_process()")
    
func on_physics_process(_delta) -> void:
    print(ID + " on_physics_process()")
        
func on_input(_event) -> void:
    print(ID + " on_input()")

func on_unhandled_input(_event) -> void:
    print(ID + " on_unhandled_input()")

# Add any configuration warning customization below:
#func _get_configuration_warning(parent):
#    return ""

func evaluate_proper_configuration():
    return true
