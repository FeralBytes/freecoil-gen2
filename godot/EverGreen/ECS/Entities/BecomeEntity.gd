extends Node

var ut_active: bool = false
###############################################################################
# Become Entity: Simply attach this script as the main script of any Node and it
#               will be setup to be an entity for you to attach other components
#               and subsystems too as needed.

var scratchpad_storage: Dictionary = {}
var parent_entity: Node
# When exporting arrays do not set the value to = [] or all exported arrays will
# be the same array in memory.
# https://github.com/godotengine/godot/issues/20436
export(Array, Resource) var components: Array
var components_by_id: Array
export(Array, Resource) var subsystems: Array
export(Dictionary) var traits: Dictionary = {}
var subsystems_by_id: Array
var components_n_subsystems_init: bool = false
var time_passed: float = 0.0
# Below was for when I was experimenting with this being a tool and having tool scripts. 
# In the end I found that tool scripts caused more problems than they solved when 
# mixed with the ECS that I am making.
export(float) var update_rate: float = 30.0
###############################################################################
# WARNING: This is not a component, it is a script that allows the attached node
# to become an Entity.
###############################################################################

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        pass
    if not Engine.editor_hint:
        # Code to execute in game.
        for component in components:
            components_by_id.append(component.COMPONENT_SYSTEM_ID)
        for subsystem in subsystems:
            if subsystem != null:
                subsystems_by_id.append(subsystem.COMPONENT_SYSTEM_ID)
        on_initialize_components_and_subsystems()
        if components_n_subsystems_init:
            on_ready()

func _process(delta) -> void:
    if Engine.editor_hint:
        if time_passed > update_rate:
            time_passed = 0
            call("update_configuration_warning")
        else:
            time_passed += delta
    if not Engine.editor_hint:
        if components_n_subsystems_init:
            on_process(delta)
    
func _physics_process(delta):
    if Engine.editor_hint:
        pass
    else:
        if components_n_subsystems_init:
            on_physics_process(delta)

func _input(event):
    if components_n_subsystems_init:
        on_input(event)
        
func _unhandled_input(event):
    if components_n_subsystems_init:
        on_unhandled_input(event)

#func _get_configuration_warning():
#    on_initialize_components_and_subsystems(get_parent(), self)
#    if components.size() > 0:
#        if components[0] == null:
#            return "Needs at least 1 component, not an empty value. (Update Rate = " + str(update_rate) + ")"
#        var val = ""
#        for subsystem in subsystems:
#            if subsystem.has_method("_get_configuration_warning"):
#                var temp = subsystem.call("_get_configuration_warning", get_parent())
#                if temp != "":
#                    val = temp
#                    return val + " (Update Rate = " + str(update_rate) + ")"
#        return ""
#    return "Needs at least 1 component. (Update Rate = " + str(update_rate) + ")"

func on_editor_ready(_parent, _component) -> void:
    print("on_editor_ready called.")
    on_ready()
    
func on_ready() -> void:
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if component.subsystem_has_on_ready:
                    component.subsystem_ref.on_ready(component)

func on_process(delta) -> void:
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if component.subsystem_has_on_process:
                    
                    component.subsystem_ref.on_process(delta, component)
    
func on_physics_process(delta) -> void:
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if component.subsystem_has_on_physics_process:
                    component.subsystem_ref.on_physics_process(delta, component)
        
func on_input(event) -> void:
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if component.subsystem_has_on_input:
                    component.subsystem_ref.on_input(event, component)
                
func on_unhandled_input(event) -> void:
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if component.subsystem_has_on_unhandled_input:
                    component.subsystem_ref.on_unhandled_input(event, component)

func on_initialize_components_and_subsystems() -> void:
    parent_entity = self
    parent_entity.add_to_group("ECS_Entities")
    make_subsystems_unique()
    make_components_unique()
    set_other_components_parent_ref()
    set_components_to_have_subsystem_ref()
    set_other_components_to_have_ref_to_self()
    if not ut_active:
        if check_system_configurations():
            components_n_subsystems_init = true
    else:
        components_n_subsystems_init = true

func make_components_unique() -> void:  # As Needed, almost always desired.
    if len(components) > 0:
        var counter: int = 0
        for component in components:
            if component.unique_component:
                components[counter] = component.duplicate()
            counter += 1
                
func make_subsystems_unique() -> void:  # Hopefully Not Needed, but just incase.
    if len(subsystems) > 0:
        var counter: int = 0
        for subsystem in subsystems:
            if subsystem != null:
                for component in components:
                    if component.COMPONENT_SYSTEM_ID == subsystem.COMPONENT_SYSTEM_ID:
                        if component.unique_subsystem:
                            subsystems[counter] = subsystem.duplicate()
            counter += 1
    
func set_other_components_parent_ref() -> void:
    if len(components) > 0:
        for component in components:
            component.parent_entity = parent_entity
      
func set_components_to_have_subsystem_ref() -> void:
    if len(subsystems) > 0:
        for subsystem in subsystems:
            if subsystem != null:
                for component in components:
                    if component.COMPONENT_SYSTEM_ID == subsystem.COMPONENT_SYSTEM_ID:
                        component.subsystem_ref = subsystem
                        break

func set_other_components_to_have_ref_to_self():
    if len(components) > 0:
        for component in components:
            component.become_make_ref = self
            
func check_system_configurations():
    if len(components) > 0:
        for component in components:
            if component.subsystem_ref != null:
                if not component.subsystem_ref.evaluate_proper_configuration(component):
                    parent_entity.get_tree().quit()
                    return false
    return true
    
func get_ref_to_component(component_id):
    var stats_ref
    var component_index = components_by_id.find(component_id)
    if component_index != -1:
        stats_ref = components[component_index]
    return stats_ref

