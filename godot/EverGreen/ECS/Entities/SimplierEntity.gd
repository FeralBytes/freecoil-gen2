extends Node

###############################################################################
# Become Entity: Simply attach this script as the main script of any Node and it
#               will be setup to be an entity for you to attach other components
#               and subsystems too as needed.

var scratchpad_storage: Dictionary = {}
var par: Node = self  # parent_entity
# When exporting arrays do not set the value to = [] or all exported arrays will
# be the same array in memory.
# https://github.com/godotengine/godot/issues/20436
export(Array, Resource) var Composites: Array
export(Dictionary) var data: Dictionary = {}
var composites_by_id: Array
var composites_by_instance_id: Array
var composites_inited: bool = false
var time_passed: float = 0.0
# Below was for when I was experimenting with this being a tool and having tool scripts. 
# In the end I found that tool scripts caused more problems than they solved when 
# mixed with the ECS that I am making.
export(float) var update_rate: float = 30.0
###############################################################################
# WARNING: This is not a component, it is a script that allows the attached node
# to become an Entity.
###############################################################################

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    data = data.duplicate(true)
    if Engine.editor_hint:
        # Code to execute in editor.
        pass
    if not Engine.editor_hint:
        # Code to execute in game.
        for composite in Composites:
            if composite != null:
                composites_by_id.append(composite.ID)
        on_initialize_composites()
        if composites_inited:
            on_ready()

func _process(delta) -> void:
    if Engine.editor_hint:
        if time_passed > update_rate:
            time_passed = 0
            call("update_configuration_warning")
        else:
            time_passed += delta
    if not Engine.editor_hint:
        if composites_inited:
            on_process(delta)
    
func _physics_process(delta):
    if Engine.editor_hint:
        pass
    else:
        if composites_inited:
            on_physics_process(delta)

func _input(event):
    if composites_inited:
        on_input(event)
        
func _unhandled_input(event):
    if composites_inited:
        on_unhandled_input(event)

#func _get_configuration_warning():
#    on_initialize_components_and_subsystems(get_parent(), self)
#    if components.size() > 0:
#        if components[0] == null:
#            return "Needs at least 1 component, not an empty value. (Update Rate = " + str(update_rate) + ")"
#        var val = ""
#        for subsystem in subsystems:
#            if subsystem.has_method("_get_configuration_warning"):
#                var temp = subsystem.call("_get_configuration_warning", get_parent())
#                if temp != "":
#                    val = temp
#                    return val + " (Update Rate = " + str(update_rate) + ")"
#        return ""
#    return "Needs at least 1 component. (Update Rate = " + str(update_rate) + ")"
    
func on_ready() -> void:
    if len(Composites) > 0:
        for composite in Composites:
            if composite.has_on_ready:
                composite.on_ready(par)

func on_process(delta) -> void:
    if len(Composites) > 0:
        var counter = 0
        for composite in Composites:
            if composite.has_on_process:
                composite.on_process(par, delta)
            counter += 1
    
func on_physics_process(delta) -> void:
    if len(Composites) > 0:
        for composite in Composites:
            if composite.has_on_physics_process:
                composite.on_physics_process(par, delta)
        
func on_input(event) -> void:
    if len(Composites) > 0:
        for composite in Composites:
            if composite.has_on_input:
                composite.on_input(par, event)
                
func on_unhandled_input(event) -> void:
    if len(Composites) > 0:
        for composite in Composites:
            if composite.has_on_unhandled_input:
                composite.on_unhandled_input(par, event)

func on_initialize_composites() -> void:
    par.add_to_group("ECS_Entities")
    if check_system_configurations():
        composites_inited = true

# Can not make composites be guarranteed to be unique.
            
func check_system_configurations():
    if len(Composites) > 0:
        for composite in Composites:
            if not composite.evaluate_proper_configuration(par):
                get_tree().quit()
                return false
    return true
    
func get_composite_ref(id):
    var ref
    var composite_index = composites_by_id.find(id)
    if composite_index != -1:
        ref = Composites[composite_index]
    return ref

