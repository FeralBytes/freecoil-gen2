tool
extends Resource
# Technically this extends Resource, but that is the default if empty.
# All components must have the below vars to be valid components.
var parent_entity: Node
var subsystem_ref: Resource
var become_make_ref: Node  # With great power comes great responsibility.
# Below determines if the component is unique. You pretty much always want true.
const unique_component: bool = true
# Be sure to adjust the booleans below to reflect what your subsystem needs.
const has_subsystem: bool = true
# For efficiency, subsystems should not be unique! Also Make below a constant.
var unique_subsystem: bool = false
const subsystem_has_on_ready: bool = true
const subsystem_has_on_process: bool = true
const subsystem_has_on_physics_process: bool = true
const subsystem_has_on_input: bool = true
const subsystem_has_on_unhandled_input: bool = true
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const COMPONENT_SYSTEM_ID = "FPCCameraControl"
const VERSION = 2
# Make your edits below this line.
# Declare constants here.
# Declare your component specific vars here.
var CameraRig: Spatial
var PitchRotation: Spatial
var TheCamera: Camera
var CameraSpring: SpringArm
var _input_relative: Vector2 = Vector2.ZERO
var is_current = false
var character_model_ref: MeshInstance
var first_person_model_ref: MeshInstance

# Resources are preloaded in as objects, not resources if you preload.
# var make_entity_subsystem:Object = preload("res://systems/make_entity_subsystem.tres")
export(bool) var FirstPersonClippedCameraControl_Component
# Declare Component Export variables here.
export(float) var MAX_LOOK_UP_ANGLE = 60
export(float) var MAX_LOOK_DOWN_ANGLE= -80
export(bool) var invert_look_up_down: bool = false
export(Vector2) var mouse_sensitivity: Vector2 = Vector2(0.2, 0.2)
export(Vector2) var gamepad_sensitivity: Vector2 = Vector2(1.7, 1.7)
export(bool) var make_currently_active_camera: bool = true
export(bool) var is_the_only_camera: bool = true
export(NodePath) var character_model: NodePath
export(NodePath) var first_person_model: NodePath

# Remember components are just for data, any logic or functions should be 
# executed by a super system or a sub system.

# Declare functions that may be used by the system, typically this is useful
# for state machines that share functions via a component.

# Components are not Nodes, so they never technically enter the scene, 
# so _ready() is never triggered. Additionally most of these sort of functions 
# are never triggered.
#func _process():
#    pass # Never gets called by the scene tree on a resource..
