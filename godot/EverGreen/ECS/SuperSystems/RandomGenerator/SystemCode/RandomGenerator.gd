tool
extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "Dice"
var entities: Array = []

var RNG = RandomNumberGenerator.new()
export(int) var editor_yield_duration: int = 60

export(bool) var editor_roll_dice: bool = false setget editor_roll_dice_set
export(int) var int_result: int = 0
export(int) var int_die_lowest_num: int = 1
export(int) var int_die_highest_num: int = 20

export(bool) var editor_roll_crafted_dice: bool = false setget editor_roll_crafted_dice_set
export(String) var crafted_result: String = ""
export(Array, String) var crafted_die_values: Array = []

export(bool) var editor_draw_card:  bool = false setget editor_draw_card_set
export(String) var card_draw_result: String = ""
export(Array, String) var card_values: Array = []

export(bool) var editor_draw_lottery_item: bool = false setget editor_draw_a_lottery_item
export(String) var lottery_result: String = ""
export(String) var item_was_probability: String = ""
export(Array, Resource) var lottery_items: Array = []
var sum_of_chance:int = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        if not PM.SS.has(SuperSystemID):
            PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
            on_real_ready()

#func _process(delta) -> void:
#    pass
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func on_editor_ready() -> void:
    RNG.randomize()
    
func on_real_ready() -> void:
    RNG.randomize()

func editor_yield():
    for _i in range(editor_yield_duration):
        yield(get_tree(), "idle_frame")

func editor_roll_dice_set(new_val):
    if new_val == true:
        editor_roll_dice = true
        int_result = roll_int_die(int_die_lowest_num, int_die_highest_num)
        property_list_changed_notify()
        yield(editor_yield(), "completed")
        editor_roll_dice = false
        property_list_changed_notify()

func editor_roll_crafted_dice_set(new_val):
    if new_val == true:
        editor_roll_crafted_dice = true
        crafted_result = roll_crafted_die(crafted_die_values)
        property_list_changed_notify()
        yield(editor_yield(), "completed")
        editor_roll_crafted_dice = false
        property_list_changed_notify()
        
func editor_draw_card_set(new_val):
    if new_val == true:
        pass
        
func roll_int_die(lowest, highest):
    return RNG.randi_range(lowest, highest)

func roll_crafted_die(crafted_values):
    return crafted_values[RNG.randi() % crafted_values.size()]

func editor_draw_a_lottery_item(new_val):
    if new_val == true:
        editor_draw_lottery_item = true
        var item = draw_a_lottery_item()
        if item != null:
            lottery_result = item.named_value
            item_was_probability = str(item.pick_chance) + " of " + str(sum_of_chance)
            property_list_changed_notify()
            yield(editor_yield(), "completed")
        editor_draw_lottery_item = false
        property_list_changed_notify()

func draw_a_lottery_item():
    sum_of_chance = 0
    if lottery_items.size() > 0:
        for item in lottery_items:
            if item.can_be_chosen:
                sum_of_chance += item.pick_chance
        var random_num: int = RNG.randi() % sum_of_chance
        var offset: int = 0
        for item in lottery_items:
            if item.can_be_chosen:
                if random_num < item.pick_chance + offset:
                    return item
                else:
                    offset += item.pick_chance
    else:
        return null

func draw_from_lottery():
    return draw_a_lottery_item().named_value
