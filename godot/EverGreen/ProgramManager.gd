extends Node2D

const GODOT_MAX_INT: int = 9223372036854775807

# Major.Minor.Micro-alpha, beta, rc, preview, dev
var VERSION = "0.4.0-alpha10"
var VERSION_INT = 4  # Increment any time the micro changes.
var version_mismatch = false
var update_boot_image: bool = false
const DEBUG_LEVELS = ["not_set", "debug", "info", "warning", "error", 
    "critical", "testing"
]

var playing_custom_scene = false
var running_as_export = false
var debug_level = 3
var debug_output_to_gui = false
var ci_testing = false

var operating_system: String = ""
var max_threads: int = 1  # Always tries to subtract one for the main thread.
var current_threads: int = 0  # Does not count the main thread.
var display_metrics: Dictionary = {}
var unique_id: String = ""

var erase_user_dir:bool = false
var ran_erase_user_data_dir: bool = false
var artificial_delay_between_states: float = 0
var fake_os: String = ""
var fake_unique_id: String = ""
var run_unit_tests: bool = false
var run_specified_test: bool = false

var States: Dictionary = {}
var BGLoad: Object = preload("res://EverGreen/BackGroundLoading.gd").new()
var LTD
var STD 
var Video: Resource 
var Audio: Resource 
var Misc: Resource 
var SS: Dictionary = {}  # SuperSystems Name Space
var TheContainer: Node
var Networking: Node 

var BackAndEscape: Label
var LoadingScreen: ReferenceRect
var InitialLoadingOutput: Label
var InitialLoadingSpinner

var current_state: String = ""
var current_state_ref: Node
var previous_state: String = ""
var the_state_stack: Array = []

var screenshot_finished: bool = false
var visual_loading_stopped_for_screenshot: bool = false

var splash_screen_loaded: bool = false
var submodules_ready: bool = false
var helpers_loaded: bool = false
var back_request_counter: int = 0
var continue_spinner_updates: bool = true
var theme_loaded: bool = false
var command_line_args: Dictionary = {}
var parsed_command_line_args_once: bool = false

var background_threads: Dictionary = {}
var background_loaded_resources: Dictionary = {}
var thread_counter = 0
var queued_threads: Dictionary = {}

var one_time_draw_check = false

const static_vars = {the_first = false, }  # Use Sparringly and watch out for name conflicts.

func _ready():
    TheContainer = get_tree().root.get_node_or_null("Container")
    if not static_vars.the_first:
            static_vars.the_first = true
    
        

func _draw():
    if not one_time_draw_check:
        one_time_draw_check = true
        call_deferred("load_submodule_ltd_n_std")

func _process(_delta) -> void:
    pass

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        pass
    if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        pass
    if what == MainLoop.NOTIFICATION_APP_RESUMED:
        pass
    if what == MainLoop.NOTIFICATION_APP_PAUSED:
        pass

func detect_vital_data():
    if OS.get_processor_count() - 1 < 1:
        max_threads = 1
    else:
        max_threads = OS.get_processor_count() - 1
    if fake_os == "":
        operating_system = OS.get_name()
    else:
        operating_system = fake_os
    if fake_unique_id == "":
        unique_id = OS.get_unique_id()
    else:
        unique_id = fake_unique_id
    display_metrics = Video.get_display_metrics()
    set_container_n_get_export_vars()
    
func Log(to_print, level="debug"):
    if DEBUG_LEVELS.find(level) >= debug_level:
        if DEBUG_LEVELS.find(level) < 3:
            print(to_print)
        else:
            printerr(to_print)
        if debug_output_to_gui:
            PM.get_tree().call_group("DebugOutput", "put", to_print)
        elif level == "testing":
            PM.get_tree().call_group("DebugOutput", "put", to_print)

func ensure_container_start_vital_data():
    if TheContainer == null:
        playing_custom_scene = true
        if OS.get_environment("CI_TESTING") != "":
            ci_testing = true
        # Load the container here.
        BGLoad.load_n_callback(
            "res://scenes/Global/Container/TheContainer.tscn", self, 
            "add_container_to_scene_tree"
        )
    else:
        splash_screen_loaded = true
        call_deferred("detect_vital_data")

func add_container_to_scene_tree(result):
    if result[0] == "finished":
        var container_instance = result[1].instance()
        yield(get_tree(), "idle_frame")
        get_tree().root.add_child(container_instance)
        splash_screen_loaded = true
        call_deferred("detect_vital_data")

func set_container_n_get_export_vars():
    TheContainer = get_tree().root.get_node_or_null("Container")
    #Backup/Copy the variables across
    fake_os = TheContainer.fake_os
    fake_unique_id = TheContainer.fake_unique_id
    erase_user_dir = TheContainer.erase_user_dir
    run_unit_tests = TheContainer.run_unit_tests
    run_specified_test = TheContainer.run_specified_test
    artificial_delay_between_states = TheContainer.artificial_delay_between_states
    debug_level = TheContainer.debug_level
    if VERSION != TheContainer.VERSION:
        version_mismatch = true
    VERSION = TheContainer.VERSION
    VERSION_INT = TheContainer.VERSION_INT
    update_boot_image = TheContainer.update_boot_image
    LoadingScreen = TheContainer.get_node("Loading/Loading")
    if OS.has_feature("standalone"):
        running_as_export = true
    else:
        if version_mismatch:
            update_boot_image = true
            var file = File.new()
            if file.file_exists("res://EverGreen/ProgramManager.gd"):
                file.open("res://EverGreen/ProgramManager.gd", file.READ_WRITE)
                var file_lines_array = []
                var version_found = false
                var version_int_found = false
                while not file.eof_reached(): 
                    var line = file.get_line()
                    if not version_found:
                        if "var VERSION = " in line:
                            line = 'var VERSION = "' + TheContainer.VERSION + '"'
                            version_found = true
                    if not version_int_found:
                        if "var VERSION_INT = " in line:
                            line = ("var VERSION_INT = " + str(TheContainer.VERSION_INT) +
                                "  # Increment any time the micro changes."
                            )
                            version_int_found = true
                    file_lines_array.append(line)
                var joined_strings = PoolStringArray(file_lines_array).join("\n")
                file.seek(0)
                file.store_string(joined_strings)
                file.close()
        if update_boot_image:
            # Take new screenshot:
            # Must yield to allow _ready() for TheContainer to execute.
            Log("Performance Warning: Updating Boot Image Screenshot.", "warning")
            while not splash_screen_loaded:
                yield(get_tree(), "idle_frame")  
            LoadingScreen.Version.text = VERSION
            LoadingScreen.Spinner.modulate.a = 0
            LoadingScreen.GameHint.modulate.a = 0
            LoadingScreen.ItemOutput.modulate.a = 0
            LoadingScreen.BootSpeed.modulate.a = 0
            # Wait for the frame to re-draw for the changes made.
            yield(get_tree(), "idle_frame")
            var clear_mode = get_viewport().get_clear_mode()
            get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
            # Wait until the frame has finished before getting the texture.
            yield(VisualServer, "frame_post_draw")
            # Retrieve the captured image.
            var img = get_viewport().get_texture().get_data()
            get_viewport().set_clear_mode(clear_mode)
            # Flip it on the y-axis (because it's flipped).
            img.flip_y()
            img.save_png("res://assets/images/boot_splash.png")
            LoadingScreen.Spinner.modulate.a = 1
            LoadingScreen.GameHint.modulate.a = 1
            LoadingScreen.ItemOutput.modulate.a = 1
            LoadingScreen.BootSpeed.modulate.a = 1
    populate_state_names()

func load_submodule_ltd_n_std():
    BGLoad.load_n_callback(
            "res://EverGreen/Settings.gd", self, 
            "load_submodule_video"
        )

func load_submodule_video(result):
    if result[0] == "finished":
        PM.LTD = result[1].new("LongTermData", "", true, true)
        yield(get_tree(), "idle_frame")
        PM.STD = result[1].new("ShortTermData")
        BGLoad.load_n_callback(
            "res://EverGreen/Video.gd", self, 
            "load_submodule_audio"
        )

func load_submodule_audio(result):
    if result[0] == "finished":
        PM.Video = result[1].new()
        BGLoad.load_n_callback(
            "res://EverGreen/Audio.gd", self, 
            "load_submodule_misc"
        )

func load_submodule_misc(result):
    if result[0] == "finished":
        PM.Audio = result[1].new()
        BGLoad.load_n_callback(
            "res://EverGreen/Misc.gd", self, 
            "load_submodule_networking"
        )

func load_submodule_networking(result):
    if result[0] == "finished":
        PM.Misc = result[1].new()
        BGLoad.load_n_callback(
            "res://EverGreen/Networking/Networking.gd", self, 
            "finish_submodule_loading"
        )

func finish_submodule_loading(result):
    if result[0] == "finished":
        PM.Networking = result[1].new()
        call_deferred("ensure_container_start_vital_data")

func populate_state_names():
    if not visual_loading_stopped_for_screenshot:
        LoadingScreen.item_name("ProgramManager States")
    Log("Program Manager: Populating states...")
    var dir = Directory.new()
    dir.open("res://logic/ProgramManagerStates/")
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            # https://github.com/godotengine/godot/issues/42507
            if file.matchn("*.tscn"):
                var file_path = "res://logic/ProgramManagerStates/" + file
                file = file.replace(".tscn", "")
                if not visual_loading_stopped_for_screenshot:
                    LoadingScreen.item_name("ProgramManager State - " + file)
                States[file] = file_path
                Log("Program Manager: Populated: " + file)
    dir.list_dir_end()
    Log("Program Manager: State Loading Completed.")
    submodules_ready = true
    if not playing_custom_scene:
        evaluate_state_change("PostSplash")
    else:
        LoadingScreen.fade_out()

func evaluate_state_change(next_state):
    if next_state == "#GoBack#":
        the_state_stack.pop_back()
        if the_state_stack.size() > 0:
            next_state = the_state_stack[-1]
#    elif next_state == "#ALL_UNIT#":
#        next_state = "AllUnitTests"
#    elif run_unit_tests:
#        next_state = "AllUnitTests"
    elif next_state != current_state:
        if next_state in States:
            the_state_stack.append(next_state)
        else:
            Log("State not found: " + str(next_state), "critical")
            get_tree().quit(FAILED)
            next_state = current_state
    if next_state != current_state:
        if current_state != "":
            get_node(current_state).on_exit()
        Log("State Transition: from: " + str(current_state) + " -> " + str(next_state))
        previous_state = current_state
        STD.set_("ProgramManager_state_trigger", null)
        current_state = next_state
        LoadingScreen.fade_in()
        LoadingScreen.progress(1.0)
        LoadingScreen.item_name(current_state)
        BGLoad.load_n_callback(
                States[current_state], self, "finish_state_change")

func finish_state_change(results):
    if results[0] == "finished":
        var loaded_scene = results[1].instance()
        if artificial_delay_between_states != 0:
            yield(get_tree().create_timer(artificial_delay_between_states), "timeout")
        add_child(loaded_scene)
        current_state_ref = loaded_scene
        if current_state_ref.name == "#Example#":
            printerr("PM: Error: The State ID of the ProgramManagerState has not been changed " +
                "from #Example# to something else! It can not be '#Example#."
            )
            get_tree().quit(ERR_UNCONFIGURED)
        get_node(current_state).on_enter(previous_state)
    elif results[0] == "loading":
        LoadingScreen.progress(results[1] * 100.0)

################################################################################
# Application Life-cycle States for EverGreen tracked via current and previous
# previous == "Quitting" then Starting->Running
# previous == "ForegroundPaused"or"BackgroundPaused" then Resuming->ForegroundPaused




