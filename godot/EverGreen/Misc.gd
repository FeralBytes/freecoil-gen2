extends Resource

const FLOAT_EPSILON = 0.00000

var time_start: int = 0

static func general_type_of(obj):
    var typ = typeof(obj)
    # https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html
    var builtin_type_names = ["Null", "Bool", "Integer", "Float", "String", "Vector2", "Rect2", "Vector3", 
        "Transform2D", "Plane", "Quat", "AABB",  "Basis", "Transform", "Color", "NodePath",
        "RID", "Object", "Dictionary", "Array", "RawArray", "IntegerArray", "FloatArray",
        "StringArray", "Vector2Array", "Vector3Array", "ColorArray", "Max"]
    if(typ == TYPE_OBJECT):
       return "Object: " + str(obj.get_property_list()[1]["name"])
    else:
        return builtin_type_names[typ]
        
static func join_array_to_str(arr, delimiter=","):
    var str_arr = ""
    for item in arr:
        if str_arr == "":
            str_arr = str(item)
        else:
            str_arr += delimiter + str(item)
    return str_arr

static func get_error_description(err_num):
    var error_array = ["OK: No error.", "FAILED: Generic error, error uknown.", 
            "ERR_UNAVAILABLE: Unavailable error, resource unavailable.", 
            "ERR_UNCONFIGURED: Resource unconfigured.",
            "ERR_UNAUTHORIZED: Resource returned unauthorized error.", 
            "ERR_PARAMETER_RANGE_ERROR", "ERR_PARAMETER_RANGE_ERROR", "ERR_FILE_NOT_FOUND",
            "ERR_FILE_BAD_DRIVE", "ERR_FILE_BAD_PATH", "ERR_FILE_NO_PERMISSION", 
            "ERR_FILE_ALREADY_IN_USE", "ERR_FILE_CANT_OPEN", "ERR_FILE_CANT_WRITE",
            "ERR_FILE_CANT_READ", "ERR_FILE_UNRECOGNIZED", "ERR_FILE_CORRUPT", 
            "ERR_FILE_MISSING_DEPENDENCIES", "ERR_FILE_EOF", 
            ("ERR_CANT_OPEN: Can't open the resource. Typically a file but from" +
            " a higher level library like ConfigFile or JSON.")]
    return error_array[err_num]
    
func list_files_in_directory(path):
    var files = []
    var dir = Directory.new()
    if not dir.dir_exists(path):
        return []
    dir.open(path)
    dir.list_dir_begin(true)
    var file = dir.get_next()
    while file != "":
        if dir.current_is_dir():
            pass
        else:
            files.append(file)
        file = dir.get_next()
    return files

func get_random_key_of_dict(dict):
  var a = dict.keys()
  return a[randi() % a.size()]

# This method was created because of how bad dict.has() is.
# https://github.com/godotengine/godot/issues/27615
func dicts_same_keys_n_vals(dict1, dict2):
    var are_the_same = true  # Not checking for order.
    if dict1.size() != dict2.size():
        are_the_same = false
    if are_the_same:
        var dict1_keys = dict1.keys()
        var dict1_vals = dict1.values()
        var dict2_keys = dict2.keys()
        var dict2_vals = dict2.values()
        for i in range(0, dict1_keys.size()):
            var needs_sorting = false
            if typeof(dict1_keys[i]) != typeof(dict2_keys[i]):
                needs_sorting = true
            if dict1_keys[i] != dict2_keys[i]:
                needs_sorting = true
            if needs_sorting:
                var position = dict2_keys.find(dict1_keys[i]) 
                var key = dict2_keys[position]
                var value = dict2_vals[position]
                dict2_keys.erase(key)
                dict2_vals.erase(value)
                dict2_keys.insert(i, key)
                dict2_vals.insert(i, value)
        for i in range(0, dict1_keys.size()):
            if dict1_keys[i] != dict2_keys[i]:
                are_the_same = false
            if dict1_vals[i] != dict2_vals[i]:
                if (typeof(dict1_vals[i]) == TYPE_DICTIONARY and 
                    typeof(dict2_vals[i]) == TYPE_DICTIONARY
                ):
                    are_the_same = dicts_same_keys_n_vals(dict1_vals[i], dict2_vals[i])
                else:
                    are_the_same = false
    return are_the_same

func arrays_have_same_content(a1, a2):
    if a1.size() != a2.size(): return false
    for item in a1:
        if !a2.has(item): return false
        if a1.count(item) != a2.count(item): return false
    return true
    
func get_dir_contents(rootPath: String) -> Array:
    var files = []
    var directories = []
    var dir = Directory.new()
    if dir.open(rootPath) == OK:
        dir.list_dir_begin(true, false)
        _add_dir_contents(dir, files, directories)
    else:
        push_error("An error occurred when trying to access the path.")
    return [files, directories]
    
func _add_dir_contents(dir: Directory, files: Array, directories: Array):
    var file_name = dir.get_next()
    while (file_name != ""):
        var path = dir.get_current_dir().plus_file(file_name)
        if dir.current_is_dir():
            var subDir = Directory.new()
            subDir.open(path)
            subDir.list_dir_begin(true, false)
            directories.append(path)
            _add_dir_contents(subDir, files, directories)
        else:
            files.append(path)
        file_name = dir.get_next()
    dir.list_dir_end()

func check_if_file_exists(file_path):
    return File.new().file_exists(file_path)

func create_2d_array(width, height):
    var array_2d = []
    array_2d.resize(width)    # X-dimension
    for x in width:    # this method should be faster than range since it uses a real iterator iirc
        array_2d[x] = []
        array_2d[x].resize(height)    # Y-dimension
    return array_2d

func create_3d_array(width, height, depth):
    var array_3d = []
    array_3d.resize(width)    # X-dimension
    for x in width:    # this method should be faster than range since it uses a real iterator
        array_3d[x] = []
        array_3d[x].resize(height)    # Y-dimension
        for y in height:
            array_3d[x][y] = []
            array_3d[x][y].resize(depth)    # Z-dimension
    return array_3d

func is_divisible_by(val, divisor):
    if val % divisor == 0.0:
        return true
    else:
        return false
    
func time_start_msec():
    time_start = OS.get_ticks_msec()
    
func time_end_msec():
    return OS.get_ticks_msec() - time_start
