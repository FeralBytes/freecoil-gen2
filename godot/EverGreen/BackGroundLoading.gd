#class_name BackGroundLoading
extends Resource

const static_vars = {next_thread_num = 0, __std_initialized = false}

var is_loading:bool = false
var __background_threads:Dictionary = {}
var __queued_threads: Array = []
var __threads_progress: Dictionary = {} 
var __testing_force_slow_threads = false
var __testing_dont_start_thread = false
var __verbose = false
# __threads_progress = {thread_num: [state, state_val, resource_path, 
# finished_callback_obj, finished_callback_meth, progress_callback_obj, 
# progress_callback_meth}
var __loaded_res: Dictionary = {}   # Temporary storage for resources.

func load_n_callback(resource_path, callback_object, callback_method, 
    callback_object_progress=null, callback_method_progress:String=""
) -> int:
    is_loading = true
    static_vars.next_thread_num += 1
    if callback_method_progress == "":
        callback_object_progress = callback_object
        callback_method_progress = callback_method
    __threads_progress[static_vars.next_thread_num] = ["queued", null, 
        resource_path, callback_object, callback_method, callback_object_progress,
        callback_method_progress]
    if PM.current_threads >= PM.max_threads:
        if __background_threads.size() == 0:
             __start_a_thread(static_vars.next_thread_num, resource_path, 
                callback_object, callback_method, callback_object_progress,
                callback_method_progress
            )
        else:
            __queued_threads.append(static_vars.next_thread_num)
    else:
        __start_a_thread(static_vars.next_thread_num, resource_path, 
            callback_object, callback_method, callback_object_progress,
            callback_method_progress
        )
    return static_vars.next_thread_num
    
func __start_a_thread(thread_num, resource_path, callback_object, callback_method, 
    callback_object_progress=null, callback_method_progress:String=""
) -> int:
    is_loading = true
    PM.current_threads += 1
    var new_thread = Thread.new()
    __background_threads[thread_num] = new_thread
    if not __testing_dont_start_thread:
        var start_success = new_thread.start(self, "__threaded_background_loader", 
            [resource_path, thread_num]
        )
        if start_success != OK:
#                print("WTF!")
            pass
        else:
            pass
    return thread_num

func reset_for_testing():
#    print("Reset for testing called!")
    var duplicant = __background_threads.duplicate()
    for thread_num in duplicant:
        if not __background_threads[thread_num].is_active():
            #print("Background thread was not active for thread num ", thread_num)
            __background_threads.erase(thread_num)
            PM.current_threads -= 1
        else:
            pass
#            __background_threads[thread_num].wait_to_finish()
#            __background_threads.erase(thread_num)
#    print(__threads_progress)
    var wait = true
    var yielded = false
    var every_100 = 0
    while wait:
        if __threads_progress.size() == 0:
            if __background_threads.size() == 0:
                if __queued_threads.size() == 0:
                    wait = false
        every_100 += 1
        if every_100 > 99:
            every_100 = 0
            print(__threads_progress)
            print(__background_threads)
            print(__queued_threads)
        yield(PM.get_tree(), "idle_frame")
        yielded = true
    if not yielded:
        yield(PM.get_tree(), "idle_frame")
#    print("Reset Finished.")
    

func _loading_progress_update(thread_num, progress) -> void:
    if not __threads_progress.has(thread_num):
        return
    if __threads_progress[thread_num][0] == "finished":
        return
    if __threads_progress[thread_num][1] != null:
        if __threads_progress[thread_num][1] > progress:
            return
    __threads_progress[thread_num][0] = "loading"
    __threads_progress[thread_num][1] = progress
    handle_progress_update(thread_num, "loading", progress)

func _finished_background_loading(thread_num) -> void:
    var obj = __threads_progress[thread_num][3]
    var meth = __threads_progress[thread_num][4]
    if not __testing_dont_start_thread:
        obj.call(meth, ["finished", __loaded_res[thread_num]])
    __threads_progress.erase(thread_num)
    if not __testing_dont_start_thread:
        __loaded_res.erase(thread_num)
    
func _failed_background_loading(thread_num, error_num, resource_path) -> void:
    var err_msg = ("BackGroundLoading: A background thread, thread number " + 
        str(error_num) + ", failed to load resource:'" + resource_path + 
        "' with " + "an error number of " + str(error_num) + ".")
    printerr(err_msg)
    PM.get_tree().quit(ERR_CANT_ACQUIRE_RESOURCE)

func handle_progress_update(thread_num, state, state_val) -> void:
    if __threads_progress.has(thread_num):
        if state == "loading":
            if __threads_progress[thread_num][0] != "loading":
                var obj = __threads_progress[thread_num][5]
                var meth = __threads_progress[thread_num][6]
                if state_val > 0.99:
                    state_val = 0.99
                obj.call(meth, [state, state_val])

func move_resource_from_background_to_main(thread_num):
    if __verbose:
        print("move_resource_from_background_to_main(", thread_num, ")")
    if __threads_progress.has(thread_num):
        __threads_progress[thread_num][0] = "loading"
        __threads_progress[thread_num][1] = 0.99
        var obj = __threads_progress[thread_num][5]
        var meth = __threads_progress[thread_num][6]
        obj.call(meth, ["loading", 0.99])
        if __background_threads.has(thread_num):
            if __background_threads[thread_num].is_active():
                __loaded_res[thread_num] = __background_threads[thread_num].wait_to_finish()
            else:
                printerr("Background thread is not active " + str(thread_num) +
                ":  " + str(__threads_progress[thread_num]))
                
        else:
            if not __testing_dont_start_thread:
                printerr("Background threads does not have " + str(thread_num) +
                    ":  " + str(__threads_progress[thread_num]))
        tear_down_thread(thread_num)
        _finished_background_loading(thread_num)
    
func tear_down_thread(thread_num) -> void:
    var __ = __background_threads.erase(thread_num)
    PM.current_threads -= 1
    if __queued_threads.size() > 0:
        if PM.current_threads >= PM.max_threads:
            if __background_threads.size() == 0:
                __start_a_queued_thread()
        else:
            __start_a_queued_thread()

func __start_a_queued_thread():
    var next = __queued_threads.pop_front()
    var resource_path = __threads_progress[next][2]
    var callback_object = __threads_progress[next][3]
    var callback_method = __threads_progress[next][4] 
    var callback_object_progress = __threads_progress[next][5]
    var callback_method_progress = __threads_progress[next][6]
    __start_a_thread(next, resource_path, callback_object, 
        callback_method, callback_object_progress, 
        callback_method_progress
    )

func __threaded_background_loader(data):
    var resource_path = data[0]
    var my_num = data[1]
    var progress = 0.01
    call_deferred("_loading_progress_update", my_num, progress)
    var internal = true
    if not "res://" in resource_path:
        internal = false
    if internal:
        if ".json" in resource_path:
            return __internal_json_loader(my_num, resource_path, progress) 
#        elif ".txt" in resource_path or ".md" in resource_path:
#            pass  # Unsupported right now.
        return __internal_resource_loader(my_num, resource_path, progress)
    else:
        
        call_deferred("_failed_background_loading", my_num, 
            ERR_FILE_UNRECOGNIZED, resource_path
        )
        
func __internal_resource_loader(my_num, resource_path, progress):
    var start_time = OS.get_ticks_msec()
    var loader = ResourceLoader.load_interactive(resource_path)
    if loader == null:
        call_deferred("_failed_background_loading", my_num, 
            ERR_FILE_UNRECOGNIZED, resource_path
        )
    var err = OK
    while err == OK:
        err = loader.poll()
        if err == OK:
            progress = float(loader.get_stage()) / float(loader.get_stage_count())
        else:
            progress=0.98
        call_deferred("_loading_progress_update", my_num, progress)
    if err == ERR_FILE_EOF:
        var resource = loader.get_resource()
        if __testing_force_slow_threads:
            var current_time = OS.get_ticks_msec()
            while start_time + 20 > current_time:
                OS.delay_msec(1)
                current_time = OS.get_ticks_msec()
        if not __testing_dont_start_thread:
            call_deferred("move_resource_from_background_to_main", my_num)
        return resource
    else:
        call_deferred("_failed_background_loading", my_num, err, resource_path)

func __internal_json_loader(my_num, resource_path, progress):
    var file = File.new()
    if file.file_exists(resource_path):
        file.open(resource_path, file.READ)
        var text = file.get_as_text()
        var temp = parse_json(text)
        file.close()
        call_deferred("move_resource_from_background_to_main", my_num)
        return temp
    else:
        call_deferred("_failed_background_loading", my_num, 
            ERR_FILE_NOT_FOUND, resource_path
        )
