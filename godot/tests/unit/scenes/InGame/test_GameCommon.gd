extends "res://addons/gut/test.gd"

var loading_thread_num
var gamecommon = null

func before_all():
    pass

func before_each():
    background_load_freecoil_interface()
    while gamecommon == null:
        yield(get_tree(), "idle_frame")
    add_child(gamecommon)

func after_each():
    gamecommon.queue_free()
    yield(get_tree(), "idle_frame")
    gamecommon = null

func after_all():
    pass

func background_load_freecoil_interface():
    loading_thread_num = PM.Helpers.background_loader_n_callback(
        "res://scenes/InGame/GameModes/GameCommon.gd", 
        self, "setup_gamecommon"
    )

func setup_gamecommon(background_results):
    var results = background_results[loading_thread_num]
    if results == "finished":
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
            loading_thread_num, self, "setup_gamecommon"
        ).new()
        gamecommon = temp

func test_update_location_quality():
    pass
    # below does not work as an actual unit tests, because PostSplash is 
    # needed to have had run.
#    gamecommon.set_player_respawn_vars()
#    assert_eq(PM.STD.get_("game_player_health"), 
#        PM.STD.get_("initial_health"))
#    assert_eq(PM.STD.get_("initial_health"), 10)
#    assert_eq(PM.STD.get_("game_player_health"), 10)
#    print(PM.STD.get_("game_settings"))
#
    
