extends "res://addons/gut/test.gd"

var Geodetic
var _map_maker
var loading_thread_num

func before_all():
    pass

func before_each():
    pass

func after_each():
    pass

func after_all():
    pass
    
#########################################
# Example Data
#
# Middle-ish of USA; Wichita, KS.
# LatLng: (37.679042, -97.312628)
# Zoom level: 19
# World Coordinate: (58.799908977777775, 99.0355434396344)
# Pixel Coordinate: (30828086, 51923146)
# Tile Coordinate: (120422, 202824)
#
# Chicago, IL
# LatLng: (41.850004, -87.6521887)
# Zoom level: 19
# JavaScript World Coordinate: (65.66955470222223, 95.17492272838473)
# Java World Coordinate: (65.6695537567139, 95.1749210357666)
# Pixel Coordinate: (34429759, 49899069)
# Tile Coordinate: (134491, 194918)
#
# Yukon Delta National Wildlife Refuge, Alaska, USA
# LatLng: (61.3533146, -163.4431429)
# Zoom level: 19
# JavaScript World Coordinate: (11.77376504888889, 72.37692684178135)
# Java World Coordinate: (11.7737636566162, 72.3769264221191)
# Pixel Coordinate: (6172843, 37946354)
# Tile Coordinate: (24112, 148227)

func background_load_freecoil_interface():
    loading_thread_num = PM.Helpers.background_loader_n_callback(
        "res://Logic/FreecoiLInterface/FreecoiLInterface.gd", 
        self, "setup_freecoil_interface"
    )

func setup_freecoil_interface(background_results):
    var results = background_results[loading_thread_num]
    if results == "finished":
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
            loading_thread_num, self, "setup_freecoil_interface"
        ).new()
        PM.SS["FreecoiLInterface"] = temp
        call_deferred("initialize_freecoil_interface")

func initialize_freecoil_interface():
    PM.SS["FreecoiLInterface"].initialize_freecoil_singleton()

func test_update_location_quality():
    if not PM.SS.has("FreecoiLInterface"):
        background_load_freecoil_interface()
        var count = 0 
        while not PM.SS.has("FreecoiLInterface"):
            count += 1
            if count > 100:
                count = 0
                print(PM.Helpers.background_threads)
            yield(get_tree(), "idle_frame")
    if not is_instance_valid(PM.SS["FreecoiLInterface"]):
        background_load_freecoil_interface()
        while not is_instance_valid(PM.SS["FreecoiLInterface"]):
            yield(get_tree(), "idle_frame")
    while not PM.SS["FreecoiLInterface"].interface_initialized:
        yield(get_tree(), "idle_frame")
    PM.SS["FreecoiLInterface"]._update_location_quality(null)
    assert_eq(PM.STD.get_("fi_location_quality"), 0)
    var latitude = 37.679042
    var longitude = -97.312628
    var altitude = 1500.0
    var speed = 1
    var bearing = 359
    var accuracy = 20.1
    var provider = "cell"
    var timestamp = OS.get_unix_time()
    var current_location = {"latitude": latitude, "longitude": longitude, 
        "altitude": altitude, "speed": speed, "bearing": bearing, 
        "accuracy": accuracy, "provider": provider, "timestamp": timestamp
    }
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 1)
    current_location["accuracy"] = 20
    current_location["timestamp"] = OS.get_unix_time() - 120001
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 1)
    current_location["timestamp"] = OS.get_unix_time()
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 2)
    current_location["accuracy"] = 10
    current_location["timestamp"] = OS.get_unix_time() - 60001
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 2)
    current_location["timestamp"] = OS.get_unix_time()
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 3)
    current_location["accuracy"] = 6
    current_location["timestamp"] = OS.get_unix_time() - 30001
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 3)
    current_location["timestamp"] = OS.get_unix_time()
    PM.SS["FreecoiLInterface"]._update_location_quality(current_location)
    assert_eq(PM.STD.get_("fi_location_quality"), 4)
    
    
