extends "res://addons/gut/test.gd"

var EventSync

var ut_server_mup_id = "939972095cf1459c8b22cc608eff85da"
var ut_server_rpc_id = 1
var ut_client1_mup_id = "888972095cf1459c8b22cc608eff888"
var ut_client1_rpc_id = 54321
var ut_client2_mup_id = "f1459c8b21ef15e5e1a2adf15df5ece"
var ut_client2_rpc_id = 65351

var event1 = {"additional":{"test":"works"}, "event_num":0, 
    "rec_by":ut_client1_mup_id, "time":12345, 
    "type":"Test"
}
var event1_id
var event1_diff_mem = {"additional":{"test":"works"}, "event_num":0, 
    "rec_by":ut_client1_mup_id, "time":12345, 
    "type":"Test"
}
var event1_diff_order = {"additional":{"test":"works"}, "event_num":0, "time":12345,
    "rec_by":ut_client1_mup_id,  
    "type":"Test"
}

func before_each():
    EventSync = load("res://Logic/EverGreen/Networking/EventSync_Module.gd").new()
    EventSync.unit_testing = true
    event1_id = EventSync.get_event_id_from_event(event1)

func test_server_process_loop():
    var event_sync_mod_gd = "res://Logic/EverGreen/Networking/EventSync_Module.gd"
    var StubbedEventSync = double(event_sync_mod_gd).new()
    stub(StubbedEventSync, "server_process_loop").to_call_super()
    StubbedEventSync.unit_testing = true
    StubbedEventSync.events_unsent.append(event1)
    StubbedEventSync.server_unprocessed_events.append(event1)
    StubbedEventSync.server_unprocessed_events_by_id.append(event1_id)
    StubbedEventSync.server_untransmitted_events_history.append(event1)
    StubbedEventSync.server_untransmitted_events_history_by_id.append(event1_id)
    StubbedEventSync.server_unacknowledged_events_by_mup[ut_client1_mup_id] = event1
    StubbedEventSync.server_unacknowledged_events_by_mup_by_id[ut_client1_mup_id] = event1_id
    StubbedEventSync.server_process_loop()
    assert_called(StubbedEventSync, "server_transfer_event_for_processing")
    assert_called(StubbedEventSync, "server_process_an_unprocessed_event")
    assert_called(StubbedEventSync, "server_tx_an_event_history_to_clients")
    assert_called(StubbedEventSync, "server_tx_unack_event_to_clients")
    
func test_client_process_loop():
    var event_sync_mod_gd = "res://Logic/EverGreen/Networking/EventSync_Module.gd"
    var StubbedEventSync = double(event_sync_mod_gd).new()
    stub(StubbedEventSync, "client_process_loop").to_call_super()
    StubbedEventSync.unit_testing = true
    StubbedEventSync.events_unsent.append(event1)
    StubbedEventSync.client_unprocessed_events_history.append(event1)
    StubbedEventSync.client_unprocessed_events_history_by_id.append(event1_id)
    StubbedEventSync.client_unacknowledged_events.append(event1)
    StubbedEventSync.client_unacknowledged_events_by_id.append(event1_id)
    StubbedEventSync.client_process_loop()
    assert_called(StubbedEventSync, "client_tx_event_to_server")
    assert_called(StubbedEventSync, "client_tx_unack_event_to_server")
    assert_called(StubbedEventSync, "client_process_an_event_history")

func test_server_process_an_unprocessed_event():
    EventSync.server_unprocessed_events.append(event1)
    EventSync.server_unprocessed_events_by_id.append(event1_id)
    EventSync.server_process_an_unprocessed_event()
    assert_eq(EventSync.events_history.size(), 1)
    assert_eq(EventSync.events_history_by_id.size(), 1)
    assert_eq(EventSync.events_history_by_id[0], event1_id)
    assert_eq(EventSync.server_untransmitted_events_history.size(), 1)
    assert_eq(EventSync.server_untransmitted_events_history_by_id.size(), 1)
    assert_eq(EventSync.server_untransmitted_events_history_by_id[0], event1_id)
    EventSync.server_unprocessed_events.append(event1)
    EventSync.server_unprocessed_events_by_id.append(event1_id)
    EventSync.server_process_an_unprocessed_event()
    assert_eq(EventSync.events_history.size(), 1)
    assert_eq(EventSync.events_history_by_id.size(), 1)
    assert_eq(EventSync.events_history_by_id[0], event1_id)
    assert_eq(EventSync.server_untransmitted_events_history.size(), 1)
    assert_eq(EventSync.server_untransmitted_events_history_by_id.size(), 1)
    assert_eq(EventSync.server_untransmitted_events_history_by_id[0], event1_id)

func test_client_process_an_event_history():
    EventSync.client_unprocessed_events_history.append(event1)
    EventSync.client_unprocessed_events_history_by_id.append(event1_id)
    EventSync.client_process_an_event_history()
    assert_eq(EventSync.events_history.size(), 1)
    EventSync.client_unprocessed_events_history.append(event1)
    EventSync.client_unprocessed_events_history_by_id.append(event1_id)
    EventSync.client_process_an_event_history()
    assert_eq(EventSync.events_history.size(), 1)
    assert_eq(EventSync.events_history_by_id.size(), 1)
    assert_eq(EventSync.events_history_by_id[0], event1_id)

func test_get_event_id_from_event():
    var event_id = EventSync.get_event_id_from_event(event1)
    assert_eq(event_id, event1_id)
    var next_event = event1.duplicate(true)
    next_event["rec_by"] = ut_server_mup_id
    next_event["event_num"] = 22
    var event_id2 = EventSync.get_event_id_from_event(next_event)
    var exp_event_id2 = ut_server_mup_id + "-" + str(22)
    assert_eq(event_id2, exp_event_id2)

func test_recording_an_event():
    var time_used = OS.get_ticks_usec()
    EventSync.record_event("Test", {"test":"works"}, time_used)
    assert_eq(EventSync.events_unsent.size(), 1)
    var event_recd = EventSync.events_unsent[0]
    var event_expect = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":ut_server_mup_id, "time":time_used, 
        "type":"Test"
    }
    assert_eq_deep(event_recd, event_expect)
    
func test_client_tx_event_to_server():
    assert_eq(EventSync.events_unsent.size(), 0)
    var time_used = OS.get_ticks_usec()
    EventSync.record_event("Test", {"test":"works"}, time_used)
    assert_eq(EventSync.events_unsent.size(), 1)
    var event_expect = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":ut_server_mup_id, "time":time_used, 
        "type":"Test"
    }
    EventSync.client_tx_event_to_server()
    var event_id = EventSync.get_event_id_from_event(event_expect)
    assert_eq(EventSync.client_unacknowledged_events.size(), 1)
    assert_eq(EventSync.client_unacknowledged_events_by_id.size(), 1)
    assert_eq_deep(EventSync.client_unacknowledged_events[0], event_expect)
    assert_eq(EventSync.client_unacknowledged_events_by_id[0], event_id)

func test_server_rx_event_from_client():
    EventSync.server_rx_event_from_client(event1)
    assert_eq(EventSync.server_unprocessed_events.size(), 1, 
        "Ensure that the event is stored for future processing."
    )
    assert_eq_deep(EventSync.server_unprocessed_events[0], event1)
    assert_eq(EventSync.server_unprocessed_events_by_id[0], event1_id)
    EventSync.server_rx_event_from_client(event1)
    assert_eq(EventSync.server_unprocessed_events.size(), 1,
        "Make sure the event does not get duplicated for future processing."
    )
    EventSync.server_rx_event_from_client(event1_diff_mem)
    assert_eq(EventSync.server_unprocessed_events.size(), 1,
        "Make sure the event does not get duplicated even if the memory " +
        "location is different."
    )
    EventSync.server_rx_event_from_client(event1_diff_order)
    assert_eq(EventSync.server_unprocessed_events.size(), 1,
        "Make sure the event does not get duplicated even if the memory " +
        "location is different and the order is different."
    )
    EventSync.server_unprocessed_events.clear()
    EventSync.server_unprocessed_events_by_id.clear()
    EventSync.events_history_by_id.append(event1_id)
    EventSync.events_history.append(event1)
    EventSync.server_rx_event_from_client(event1)
    assert_eq(EventSync.server_unprocessed_events.size(), 0,
        "Make sure the event does not get duplicated if in events_history either."
    )

func test_client_rx_ackn_from_server():
    var time_used = OS.get_ticks_usec()
    EventSync.record_event("Test", {"test":"works"}, time_used)
    EventSync.client_tx_event_to_server()
    EventSync.client_rx_ackn_from_server("baddatajoe-0")
    assert_eq(EventSync.client_unacknowledged_events.size(), 1)
    assert_eq(EventSync.client_unacknowledged_events_by_id.size(), 1)
    var event_id = EventSync.get_event_id_from_event(
        EventSync.client_unacknowledged_events[0]
    )
    EventSync.client_rx_ackn_from_server(event_id)
    assert_eq(EventSync.client_unacknowledged_events.size(), 0, 
        "Unacknowledged_events should not have the event in it, for the " +
        "event_id that was acknowledged"
    )
    assert_eq(EventSync.client_unacknowledged_events_by_id.size(), 0)

func test_server_transfer_event_for_processing():
    var time_used = OS.get_ticks_usec()
    EventSync.record_event("Test", {"test":"works"}, time_used)
    assert_eq(EventSync.events_unsent.size(), 1)
    var event_expect = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by": ut_server_mup_id, "time":time_used, 
        "type":"Test"
    }
    EventSync.server_transfer_event_for_processing()
    assert_eq(EventSync.events_unsent.size(), 0)
    var event_id = EventSync.get_event_id_from_event(event_expect)
    var server_unprocessed_events_size = EventSync.server_unprocessed_events.size()
    assert_eq(server_unprocessed_events_size, 1)
    var server_unprocessed_events_by_id_size = (
        EventSync.server_unprocessed_events_by_id.size()
    )
    assert_eq(server_unprocessed_events_by_id_size, 1)
    if server_unprocessed_events_size > 0:
        assert_eq_deep(EventSync.server_unprocessed_events[0], event_expect)
    else:
        assert_true(false)
    if server_unprocessed_events_by_id_size > 0:
        assert_eq(EventSync.server_unprocessed_events_by_id[0], event_id)
    else:
        assert_true(false)

func test_server_tx_an_event_history_to_clients():
    EventSync.server_untransmitted_events_history.append(event1)
    EventSync.server_untransmitted_events_history_by_id.append(event1_id)
    EventSync.server_tx_an_event_history_to_clients()
    assert_eq(EventSync.server_untransmitted_events_history.size(), 0)
    assert_eq(EventSync.server_untransmitted_events_history_by_id.size(), 0)
    assert_eq(EventSync.events_history.size(), 0)
    assert_eq(EventSync.events_history_by_id.size(), 0)
    assert_eq(EventSync.server_unacknowledged_events_by_mup[ut_client1_mup_id
        ].size(), 1
    )
    assert_eq(EventSync.server_unacknowledged_events_by_mup_by_id[
        ut_client1_mup_id].size(), 1
    )
    assert_eq(EventSync.server_unacknowledged_events_by_mup_by_id[
        ut_client1_mup_id][0], event1_id
    )
    # Below is to make sure duplication can't happen even by accident on the server.
    EventSync.server_untransmitted_events_history.append(event1)
    EventSync.server_untransmitted_events_history_by_id.append(event1_id)
    EventSync.server_tx_an_event_history_to_clients()
    assert_eq(EventSync.server_untransmitted_events_history.size(), 0)
    assert_eq(EventSync.server_untransmitted_events_history_by_id.size(), 0)
    assert_eq(EventSync.events_history.size(), 0)
    assert_eq(EventSync.events_history_by_id.size(), 0)
    assert_eq(EventSync.server_unacknowledged_events_by_mup[ut_client1_mup_id
        ].size(), 1
    )
    assert_eq(EventSync.server_unacknowledged_events_by_mup_by_id[
        ut_client1_mup_id].size(), 1
    )
    
func test_client_rx_an_event_history_from_server():
    assert_eq(EventSync.client_unprocessed_events_history.size(), 0)
    EventSync.client_rx_an_event_history_from_server(event1)
    assert_eq(EventSync.client_unprocessed_events_history.size(), 1)
    assert_eq(EventSync.client_unprocessed_events_history_by_id.size(), 1)
    EventSync.client_rx_an_event_history_from_server(event1)
    assert_eq(EventSync.client_unprocessed_events_history.size(), 1, 
        "Don't duplicate again."
    )
    assert_eq(EventSync.client_unprocessed_events_history_by_id.size(), 1)
    EventSync.client_rx_an_event_history_from_server(event1_diff_mem)
    assert_eq(EventSync.client_unprocessed_events_history.size(), 1, 
        "Don't duplicate again, just from different memory location."
    )
    assert_eq(EventSync.client_unprocessed_events_history_by_id.size(), 1)
    EventSync.client_rx_an_event_history_from_server(event1_diff_order)
    assert_eq(EventSync.client_unprocessed_events_history.size(), 1, 
        "Don't duplicate again, just from different order."
    )
    assert_eq(EventSync.client_unprocessed_events_history_by_id.size(), 1)
    EventSync.client_unprocessed_events_history.clear()
    EventSync.client_unprocessed_events_history_by_id.clear()
    EventSync.events_history.append(event1)
    EventSync.events_history_by_id.append(event1_id)
    EventSync.client_rx_an_event_history_from_server(event1)
    assert_eq(EventSync.client_unprocessed_events_history.size(), 0, 
        "Don't duplicate if it is already in events_history."
    )
    assert_eq(EventSync.client_unprocessed_events_history_by_id.size(),0)

func test_server_rx_ackn_from_client():
    EventSync.server_unacknowledged_events_by_mup[ut_client1_mup_id] = [event1]
    EventSync.server_unacknowledged_events_by_mup_by_id[ut_client1_mup_id] = [
        event1_id
    ]
    EventSync.server_rx_ackn_from_client("baddatajoe-0")
    assert_eq(EventSync.server_unacknowledged_events_by_mup[ut_client1_mup_id
        ].size(), 1
    )
    assert_eq(EventSync.server_unacknowledged_events_by_mup_by_id[
            ut_client1_mup_id].size(), 1
    )
    EventSync.server_rx_ackn_from_client(event1_id)
    assert_eq(EventSync.server_unacknowledged_events_by_mup.size(), 0, 
        "Unacknowledged_events should no longer have the event in it, for " +
        "the event_id that was acknowledged"
    )
    assert_eq(EventSync.server_unacknowledged_events_by_mup_by_id.size(), 0)
    
func test_client_process_event_sync_var_in_time_order():
    while not PM.submodules_ready:
        yield(get_tree(), "idle_frame")
    PM.STD.set_("is_host", 0)
    var time_1 = OS.get_ticks_usec()
    var event5 = {"additional":{"var_name":"test_time_ordered", "var_val": 1}, 
        "event_num":0, "rec_by": ut_server_mup_id, "time":time_1, 
        "type":"sync_var"
    }
    var event2 = {"additional":{"var_name":"test_time_ordered", "var_val": 2}, 
        "event_num":1, "rec_by": ut_server_mup_id, "time":time_1 + 5, 
        "type":"sync_var"
    }
    var event3 = {"additional":{"var_name":"test_time_ordered", "var_val": 3}, 
        "event_num":2, "rec_by": ut_server_mup_id, "time":time_1 + 10, 
        "type":"sync_var"
    }
    EventSync.process_event_sync_var(event2)
    assert_eq(PM.STD.get_("test_time_ordered"), 2)
    EventSync.process_event_sync_var(event5)
    assert_eq(PM.STD.get_("test_time_ordered"), 2)
    EventSync.process_event_sync_var(event3)
    assert_eq(PM.STD.get_("test_time_ordered"), 3)
    EventSync.process_event_sync_var(event5)
    assert_eq(PM.STD.get_("test_time_ordered"), 3)


