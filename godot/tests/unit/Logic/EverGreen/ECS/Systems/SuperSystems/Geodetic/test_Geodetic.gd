extends "res://addons/gut/test.gd"

var all_interactive = true

var Geodetic
var _map_maker
var loading_thread_num
var loaded_map_tester = false
var had_to_load_geo = false

func before_all():
    if not PM.SS.has("Geodetic"):
        had_to_load_geo = true
        Geodetic = load("res://Logic/EverGreen/ECS/Systems/SuperSystems/Geodetic/Geodetic.tscn").instance()
        add_child(Geodetic)
    else:
        Geodetic = PM.SS["Geodetic"]

func before_each():
    pass

func after_each():
    pass

func after_all():
    if had_to_load_geo:
        remove_child(Geodetic)
        Geodetic.queue_free()

func test_haversine_v0():
    #58°38'38.0"N 5°42'53.6"W
    #N 36°7.2',   W 86°40.2'
    var lat1 = 36.12
    var long1 =  -86.67
    #58°38'38.0"N 5°42'53.5"W
    #N 33°56.4',  W 118°24.0'
    var lat2 = 33.94
    var long2 = -118.40
    assert_almost_eq(Geodetic.haversine_v1(lat1, long1, lat2, long2, 6372800), 2887259.95060711, 0.000001)
    assert_almost_eq(Geodetic.haversine_v0(lat1, long1, lat2, long2, 6372800), 2887259.95060711, 0.000001)
    lat1 = 51.5007
    long1 =  0.1246
    lat2 = 40.6892
    long2 = 74.0445
    assert_almost_eq(Geodetic.haversine_v0(lat1, long1, lat2, long2, 6371000), 5574840.456848555, 0.000001)
    assert_almost_eq(Geodetic.haversine_v1(lat1, long1, lat2, long2, 6371000), 5574840.456848555, 0.000001)
    lat1 = 61.320658
    long1 = -149.531634
    lat2 = 61.320604
    long2 = -149.531658
    assert_almost_eq(Geodetic.haversine_v0(lat1, long1, lat2, long2, 6371000), 6.139591, 0.000001)
    assert_almost_eq(Geodetic.haversine_v1(lat1, long1, lat2, long2, 6371000), 6.139591, 0.000001)

func test_wrap360():
    assert_eq(Geodetic.wrap360(361), 1.0)
    assert_eq(Geodetic.wrap360(1), 1.0)
    assert_eq(Geodetic.wrap360(181), 181.0)
    assert_eq(Geodetic.wrap360(360), 360.0)
    assert_eq(Geodetic.wrap360(0), 0.0)
    assert_eq(Geodetic.wrap360(722), 2.0)
    assert_almost_eq(Geodetic.wrap360(722.56), 2.56, 0.01)
    
func test_wrap_180_to_minus_180():
    assert_eq(Geodetic.wrap_180_to_minus_180(180), 180.0)
    assert_eq(Geodetic.wrap_180_to_minus_180(-180), -180.0)
    assert_eq(Geodetic.wrap_180_to_minus_180(0), 0.0)
    assert_eq(Geodetic.wrap_180_to_minus_180(181), -179.0)
    assert_eq(Geodetic.wrap_180_to_minus_180(-181), -179.0)
    
func test_wrap_90_to_minus_90():
    assert_eq(Geodetic.wrap_90_to_minus_90(90), 90.0)
    assert_eq(Geodetic.wrap_90_to_minus_90(-90), -90.0)
    assert_eq(Geodetic.wrap_90_to_minus_90(0), 0.0)
    assert_eq(Geodetic.wrap_90_to_minus_90(91), -89.0)
    assert_eq(Geodetic.wrap_90_to_minus_90(-91), -89.0)

func test_apply_projection():
    var results = Geodetic.apply_projection(41.850004, -87.6521887, 256)  # Chicago, Illionois, USA
    assert_almost_eq(results[0], 65.66955470222223, 0.00000000000001)
    assert_almost_eq(results[1], 95.17492272838473, 0.00000000000001)
    results = Geodetic.apply_projection(61.3533146,-163.4431429, 256)  # Yukon Delta National Wildlife Refuge, Alaska, USA
    assert_almost_eq(results[0], 11.77376504888889, 0.00000000000001)
    assert_almost_eq(results[1], 72.37692684178135, 0.00000000000001)
    results = Geodetic.apply_projection(50.1213479, 8.4964819, 256)  # Frankfurt, Germany
    assert_almost_eq(results[0], 134.04194268444445, 0.00000000000001)
    assert_almost_eq(results[1], 86.68664622374683, 0.00000000000001)
    results = Geodetic.apply_projection(-34.6156625, -58.503338, 256)  # Buenos Aires, Argentina
    assert_almost_eq(results[0], 86.39762631111111, 0.00000000000001)
    assert_almost_eq(results[1], 154.266087980084, 0.00000000000001)
    
func test_lat_long_to_pixel():
    # Chicago, Illionois, USA
    assert_eq(Geodetic.convert_lat_long_to_pixel(41.850004, -87.6521887, 19), [34429759, 49899069])
    # Test Close changes of longitude:
    assert_eq(Geodetic.convert_lat_long_to_pixel(41.850006, -87.6521873, 19), [34429760, 49899068])
    assert_eq(Geodetic.convert_lat_long_to_pixel(41.850006, -87.6521874, 19), [34429759, 49899068])
    assert_eq(Geodetic.convert_lat_long_to_pixel(41.850006, -87.6521900, 19), [34429759, 49899068])
    assert_eq(Geodetic.convert_lat_long_to_pixel(41.850006, -87.6521901, 19), [34429758, 49899068])
    # Yukon Delta National Wildlife Refuge, Alaska, USA
    assert_eq(Geodetic.convert_lat_long_to_pixel(61.3533146,-163.4431429, 19), [6172843, 37946354])
    # Frankfurt, Germany
    assert_eq(Geodetic.convert_lat_long_to_pixel(50.1213479, 8.4964819, 19), [70276582, 45448768])
    # Buenos Aires, Argentina
    assert_eq(Geodetic.convert_lat_long_to_pixel(-34.6156625, -58.503338, 19), [45297238, 80879858])
    # Validated using a JSFiddle launched from this page with the exact same coords.
    # https://developers.google.com/maps/documentation/javascript/examples/map-coordinates?hl=ko
    var result1 = Geodetic.convert_lat_long_to_pixel(41.850004, -87.6521887, 19)
    # [34429759, 49899069]
    assert_eq(result1[0], 34429759)
    assert_eq(result1[1], 49899069)
    var result2 = Geodetic.convert_lat_long_to_pixel(61.3533146, -163.4431429, 19)
    # [6172843,  37946354]
    assert_eq(result2[0], 6172843)
    assert_eq(result2[1], 37946354)

func test_tile_coordinates():
    assert_eq(Geodetic.get_tile_coordinates_from_x_y(34429759, 49899069, 256), [134491, 194918])

func test_pixel_to_projection():
    # Chicago, Illionois, USA
    var results = Geodetic.convert_pixel_to_projection(34429759, 49899069, 19)
    # NOTE: Projection numbers updated to reflect what Java gets as a result in 
    # Libre Office, instead of using the percision error numbers from Javascript.
    assert_almost_eq(results[0], 65.66955470222223, 0.000001)
    assert_almost_eq(results[1], 95.17492103577,    0.000001)
    #Yukon Delta National Wildlife Refuge, Alaska, USA
    results = Geodetic.convert_pixel_to_projection(6172843, 37946354, 19)
    # Proven By: Repo: docs/source/documentation/tests/GeodeticTests.ods
    assert_almost_eq(results[0], 11.7737636566162,  0.000001)
    assert_almost_eq(results[1], 72.37692684178135, 0.000001)
    
func test_convert_projection_to_lat_long():
    var results = Geodetic.convert_projection_to_lat_long(65.6695537567139, 
        95.1749210357666, 19
    )
    # NOTE: Due to rounding errors in Godot we can not get more percise than this.
    # Godot uses 32-bit floats and that is the best we can do. About 5-7 places
    # after the decimal of accuracy. So really it is a little better than 5 but 
    # not completely 6 digits either.
    assert_almost_eq(results[0], 41.8500040,   0.000002)
    assert_almost_eq(results[1], -87.6521887, 0.000002)
    results = Geodetic.convert_projection_to_lat_long(11.77376504888889, 
        72.37692684178135, 19
    )
    assert_almost_eq(results[0], 61.3533146,   0.000002)
    assert_almost_eq(results[1], -163.4431429, 0.000002)
    
func test_convert_pixel_to_lat_long():
    var results = Geodetic.convert_pixel_to_lat_long(34429759, 49899069, 19)
    # NOTE: Old Accuracy was 76.2m or 250ft
    # New accuracy is 15.2 meters or 49.8 feet.
    assert_almost_eq(results[0],  41.8500040,   0.000002) 
    assert_almost_eq(results[1], -87.6521887, 0.000002)
    results = Geodetic.convert_pixel_to_lat_long(6172843, 37946354, 19)
    assert_almost_eq(results[0],  61.3533146,   0.000002) 
    assert_almost_eq(results[1], -163.4431429, 0.000002)

func test_calc_map_movement():
    Geodetic.set_map_origin(41.850004, -87.6521887, 19)
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850004, -87.6521887, 19), [0, 0])
    # Move North which will cause the map to shift to the South.
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850006, -87.6521887, 19), [0, 1])
    # Move South which will cause the map to shift to the North.
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850002, -87.6521887, 19), [0, -1])
    # Reset to center.
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850004, -87.6521887, 19), [0, 0])
    # Move East which will cause the map to shift to the West.
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850004, -87.6521873, 19), [-1, 0])
    # Move West which will cause the map to shift to the East.
    assert_eq(Geodetic.calc_map_movement_from_origin(41.850004, -87.6521901, 19), [1, 0])
    
func test_plot_entity():
    Geodetic.set_map_origin(41.850004, -87.6521887, 19)
    # Plot entity to the North.
    assert_eq(Geodetic.plot_entity(41.850006, -87.6521887, 19), [0, -1])
    # Plot entity to the South.
    assert_eq(Geodetic.plot_entity(41.850002, -87.6521887, 19), [0, 1])
    # Plot entity at map origin.
    assert_eq(Geodetic.plot_entity(41.850004, -87.6521887, 19), [0, 0])
    # Plot entity to the East.
    assert_eq(Geodetic.plot_entity(41.850004, -87.6521873, 19), [1, 0])
    # Plot entity to the West.
    assert_eq(Geodetic.plot_entity(41.850004, -87.6521901, 19), [-1, 0])

func test_get_meters_per_pixel():
    Geodetic.set_map_origin(41.850004, -87.6521887, 19)
    assert_almost_eq(Geodetic.get_meters_per_pixel(19, 41.850004), 0.111206, 0.000001)
    
func test_bearing():
    # https://www.movable-type.co.uk/scripts/latlong.html
    # Proven By: Repo: docs/source/documentation/tests/GeodeticTests.ods
    var result0 = Geodetic.bearing_from_to(50.06632, -5.71475, 
       58.64402, -3.07009
    )
    assert_almost_eq(result0, 9.119368, 0.000001)

func test_range():
    # Proven By: Repo: docs/source/documentation/tests/GeodeticTests.ods
    var result0 = Geodetic.haversine_v0(50.06632, -5.71475, 
       58.64402, -3.07009
    )
    assert_almost_eq(result0, 969960.068336, 0.000001)
    
func test_get_bearing_and_range():
    var result0 = Geodetic.get_bearing_n_range(50.06632, -5.71475, 
       58.64402, -3.07009
    )
    assert_almost_eq(result0[0], 9.119368, 0.0001)
    assert_almost_eq(result0[1], 969960.068336, 0.001)
    
func test_convert_lat_long_to_pixel():
    # Validated using a JSFiddle launched from this page with the exact same coords.
    # https://developers.google.com/maps/documentation/javascript/examples/map-coordinates?hl=ko
    var result1 = Geodetic.convert_lat_long_to_pixel(41.850004, -87.6521887, 19)
    # [34429759, 49899069]
    assert_eq(result1[0], 34429759)
    assert_eq(result1[1], 49899069)
    var result2 = Geodetic.convert_lat_long_to_pixel(61.3533146, -163.4431429, 19)
    # [6172843,  37946354]
    assert_eq(result2[0], 6172843)
    assert_eq(result2[1], 37946354)
    var _result3 = result1[0] / 640

func test_get_next_tile_from_center():
    var north_tile_center = Geodetic.get_next_tile_from_center(41.850004, 
        -87.6521887, 19, 0
    )
#    assert_almost_eq(north_tile_center[0], 41.850643, 0.000001)
    assert_almost_eq(north_tile_center[1], -87.652189, 0.000001)
    north_tile_center = Geodetic.get_next_tile_from_center(-22.9107204,
        -43.1808817, 19, 0
    )
#    assert_almost_eq(north_tile_center[0], -22.90993, 0.000001)
    assert_almost_eq(north_tile_center[1], -43.180882, 0.000001)

#func test_get_next_tile_from_center():
#    var north_tile_center = get_next_tile_from_center(41.850004, -87.6521887, 
#        zoom_lvl, 0
#    )
    
#func test_get_neighbor_tile_centers():
#    pass
    
#########################################
# Example Data
#
# Middle-ish of USA; Wichita, KS, USA
# LatLng: (37.679042, -97.312628)
# Zoom level: 19
# World Coordinate: (58.799908977777775, 99.0355434396344)
# Pixel Coordinate: (30828086, 51923146)
# Tile Coordinate: (120422, 202824)
#
# Chicago, IL, USA
# LatLng: (41.850004, -87.6521887)
# Zoom level: 19
# JavaScript World Coordinate: (65.66955470222223, 95.17492272838473)
# Java World Coordinate: (65.6695537567139, 95.1749210357666)
# Pixel Coordinate: (34429759, 49899069)
# Tile Coordinate: (134491, 194918)

#
# 61.229852,-149.7743312
#
# The one below is bad data now as google does not provide zoom level 19 data.
# Yukon Delta National Wildlife Refuge, Alaska, USA
# LatLng: (61.3533146, -163.4431429)
# Zoom level: 19
# JavaScript World Coordinate: (11.77376504888889, 72.37692684178135)
# Java World Coordinate: (11.7737636566162, 72.3769264221191)
# Pixel Coordinate: (6172843, 37946354)
# Tile Coordinate: (24112, 148227)
#
# Catedral Metropolitana de São Sebastião do Rio de Janeiro, Brazil
# LatLng: (-22.9107204,-43.1808817)
# Zoom level: 19
# JavaScript World Coordinate: (97.29359523555556, 144.7444393989852)
# Java World Coordinate: ()
# Pixel Coordinate: (51009864, 75887772)
# Tile Coordinate: (199257, 296436)

func background_load_freecoil_interface():
    loading_thread_num = PM.Helpers.background_loader_n_callback(
        "res://Logic/FreecoiLInterface/FreecoiLInterface.gd", 
        self, "setup_freecoil_interface"
    )

func setup_freecoil_interface(background_results):
    var results = background_results[loading_thread_num]
    if results == "finished":
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
            loading_thread_num, self, "setup_freecoil_interface"
        ).new()
        PM.SS["FreecoiLInterface"] = temp
        call_deferred("initialize_freecoil_interface")

func initialize_freecoil_interface():
    PM.SS["FreecoiLInterface"].initialize_freecoil_singleton()

#func test_download_map_tile():
#    if not PM.SS.has("FreecoiLInterface"):
#        background_load_freecoil_interface()
#        while not PM.SS.has("FreecoiLInterface"):
#            yield(get_tree(), "idle_frame")
#        while not PM.SS["FreecoiLInterface"].interface_initialized:
#            yield(get_tree(), "idle_frame")
#    Geodetic.on_real_ready()
#    Geodetic.unit_testing = true
#    Geodetic.unit_testing_link = 1
#    Geodetic.unit_trigger = false
#    Geodetic.download_map_tile(37.679042, -97.312628)
#    while not Geodetic.unit_trigger:
#        yield(get_tree(), "idle_frame")
#    assert_eq(Geodetic.download_successful, true)
#    Geodetic.unit_testing_link = 2
#    Geodetic.unit_trigger = false
#    Geodetic.download_map_tile(45.679042, -22.312628)
#    while not Geodetic.unit_trigger:
#        yield(get_tree(), "idle_frame")
#    assert_eq(Geodetic.download_successful, false)
#    Geodetic.unit_testing_link = 1
#    Geodetic.unit_trigger = false

#func test_accuracy_assessment():
#    Accuracy is tested and done in the FreecoiLInterface.

#func test_map_complete_map_download():
#    PM.erase_user_data_dir()
#    yield(get_tree(), "idle_frame")
#    if PM.SS["FreecoiLInterface"] == null:
#        background_load_freecoil_interface()
#        while PM.SS["FreecoiLInterface"] == null:
#            yield(get_tree(), "idle_frame")
#        while not PM.SS["FreecoiLInterface"].interface_initialized:
#            yield(get_tree(), "idle_frame")
#    Geodetic.unit_testing = false
#    Geodetic.unit_trigger2 = false
#    Geodetic.on_real_ready()
#    var test_lat = 41.850004
#    var test_long = -87.6521887
#    Geodetic.set_map_origin(test_lat, test_long)
#    var download_map_group = Geodetic.download_complete_map_group(
#        test_lat, test_long
#    )
#    if download_map_group is GDScriptFunctionState: # Still working.
#        download_map_group = yield(download_map_group, "completed")
#    while Geodetic.unit_trigger2 == false:
#        yield(get_tree(), "idle_frame")
#    var file_list = PM.Helpers.list_files_in_directory("user://maps/")
#    assert_eq(file_list.size(), 9)
#    ############################################################################
#    # Test de-duplication of map downloads
#    download_map_group = Geodetic.download_complete_map_group(
#        test_lat, test_long
#    )
#    if download_map_group is GDScriptFunctionState: # Still working.
#        download_map_group = yield(download_map_group, "completed")
#    while Geodetic.unit_trigger2 == false:
#        yield(get_tree(), "idle_frame")
#    file_list = PM.Helpers.list_files_in_directory("user://maps/")
#    assert_eq(file_list.size(), 9)

func background_load_map_tester():
     loading_thread_num = PM.Helpers.background_loader_n_callback(
        "res://tests/specified/Geodetic/MapTester.tscn", 
        self, "setup_map_tester"
    )
    
func setup_map_tester(background_results):
    var results = background_results[loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_map_tester")
        var temp = PM.Helpers.get_loaded_resource_from_background(
            loading_thread_num
        ).instance()
        get_tree().root.get_node("Gut").get_gut().get_gui().add_child(temp)
        yield(get_tree(), "idle_frame")
        loaded_map_tester = true

func ask_map_offset_q(offset, new_q):
    var MapTesterRef = get_tree().root.get_node("Gut"
        ).get_gut().get_gui().get_node("MapTester"
    )
    var new_xy = Geodetic.calc_map_movement_from_origin(offset[0], offset[1])
    MapTesterRef.offset_map_to_player(new_xy[0], new_xy[1])
    MapTesterRef.UI.get_node("Label").text = (new_q)
    MapTesterRef.UI.visible = true
    while MapTesterRef.test_result == 0:
        yield(get_tree(), "idle_frame")
    if MapTesterRef.test_result == 1:
        assert_true(true)
    else:  # == -1
        assert_true(false)
    MapTesterRef.test_result = 0  # Reset the test.
    MapTesterRef.UI.visible = false
     
func test_map_to_pixel_plotting():
    if not PM.SS.has("FreecoiLInterface"):
        background_load_freecoil_interface()
        while not PM.SS.has("FreecoiLInterface"):
            yield(get_tree(), "idle_frame")
        while not PM.SS["FreecoiLInterface"].interface_initialized:
            yield(get_tree(), "idle_frame")
    Geodetic.set_map_origin(37.679042, -97.312628, 19)
    var new_xy = Geodetic.calc_map_movement_from_origin(37.67918,-97.31241, 19)
    assert_eq(new_xy[0], -81)
    assert_eq(new_xy[1], 65)
    Geodetic.calc_map_movement_from_origin(37.679042, -97.312628, 19)
    assert_almost_eq(Geodetic.lat_meter_plus_to_px, 4.713568, 0.00001)
    assert_almost_eq(Geodetic.long_meter_plus_to_px, 3.728643, 0.00001)
    var center_tile = Geodetic.load_map_from_disk(90.0, -90.0)
    assert_null(center_tile)
    new_xy = Geodetic.plot_entity(37.679042,-97.312498, 19)
    assert_eq(new_xy[0], 49)
    new_xy = Geodetic.plot_entity(37.679042,-97.312488, 19)
    assert_eq(new_xy[0], 52)
    new_xy = Geodetic.plot_entity(37.679042,-97.310913, 19)
    assert_eq(new_xy[0], 640)
    new_xy = Geodetic.plot_entity(37.679042,-97.306228, 19)
    assert_eq(new_xy[0], 2386)
    var avg_diff = Geodetic.calc_difference_for_long_change(19)
    assert_almost_eq(avg_diff, 3.728643, 0.000001)
    assert_almost_eq(int(avg_diff*640), 2386, 1.0)
    var east_640px = (640.0/avg_diff) * 0.00001
    var new_lat_long = [Geodetic.map_origin_lat, 
        Geodetic.map_origin_long + east_640px
    ]
    new_xy = Geodetic.plot_entity(new_lat_long[0], new_lat_long[1], 19)
    assert_eq(new_xy[0], 640)
    assert_eq(new_xy[1], 0)

    
