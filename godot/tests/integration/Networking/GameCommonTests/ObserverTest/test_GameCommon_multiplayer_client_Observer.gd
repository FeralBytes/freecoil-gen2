extends "res://addons/gut/test.gd"


func before_all():
    pass
    
func test_observer_client_game_state_is_one():
    assert_eq(PM.SS["GameCommon"].game_state
        , 1, "Observer client should only be at a game state of 1."
    )
