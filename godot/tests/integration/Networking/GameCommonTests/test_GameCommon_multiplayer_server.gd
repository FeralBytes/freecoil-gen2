extends "res://addons/gut/test.gd"

var killed_by_updated = false

func before_all():
    while PM.STD.get_("game_started") != 1:
        yield(get_tree(), "idle_frame")
    yield(get_tree().create_timer(7), "timeout")

func update_killed_by(_new_killed_by_mup):
    killed_by_updated = true
    
func test_get_shot_and_updates_killed_by():
    killed_by_updated = false
    PM.STD.connect(PM.STD.monitor_(
            "game_player_last_killed_by"), self, "update_killed_by")
    PM.SS["GameCommon"].respawn_start(2)
    assert_true(killed_by_updated)
    var last_killed_by = PM.STD.get_("game_player_last_killed_by")
    var player_name_by_mup = PM.STD.get_("player_name_by_mup")
    gut.p(last_killed_by)
    gut.p(player_name_by_mup)
    gut.p(PM.STD.get_("player_mup_by_laser_id")) 
    var killers_name = PM.STD.get_("player_name_by_mup")[last_killed_by]
    assert_eq(killers_name, "Client")
    

