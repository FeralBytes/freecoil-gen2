extends "res://addons/gut/test.gd"

func before_all():
    while PM.STD.get_("game_started") != 1:
        yield(get_tree(), "idle_frame")
    yield(get_tree().create_timer(0.5), "timeout")
    
func test_can_see_game_history():
    pending("Need a better test than just checking how many, or we check after " +
        "the other tests. Manually comparing a client and server, prooves that " +
        "the history is syncing exactly as expected."
    )

func test_can_advance_to_game_state_lvl_3():
    pass
