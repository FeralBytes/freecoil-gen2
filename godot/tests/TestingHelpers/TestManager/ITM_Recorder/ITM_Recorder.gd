extends Node

var recorded_events: Array = []
var recording = true
var time_last = OS.get_ticks_msec()

func _input(event):
    if recording:
        var new_event = null
        var time_now = OS.get_ticks_msec()
        if event is InputEventMouseButton:
            if not event.pressed:  # Then we know it was released.
                new_event = {"type": "InputEventMouseButton", 
                    "position": event.position, "button_index": event.button_index,
                    "time_before_exec": time_now - time_last,
                }
                PM.Log("ITM Recorder: Possibly a missed button Event!", "warning")
        elif event is InputEventKey:
            if not event.pressed:  # Then we know it was released.
                if event.scancode == 16777249:  # F6 == Stop recording.
                    var last_event = {"type": "End", 
                        "time_before_exec": time_now - time_last,
                    }
                    recorded_events.append(last_event)
                    recording = false
                    var itm_out_path = PM.ITM_test_path
                    itm_out_path = itm_out_path.replace("res://", "")
                    itm_out_path = ProjectSettings.globalize_path("res://") + itm_out_path
                    var file_name
                    if PM.TheContainer.ITM_act_as_server:
                        file_name = "server"
                    else:
                        file_name = "client"
                    itm_out_path = itm_out_path.plus_file(file_name) + ".json"
                    var file = File.new()
                    var _err = file.open(itm_out_path, File.WRITE)
                    file.store_string(to_json(recorded_events))
                    file.close()
                    PM.Log("Recording has ended!", "warning")
                else:
                    new_event = {"type": "InputEventKey", 
                        "unicode": event.unicode, "scancode": event.scancode,
                        "time_before_exec": time_now - time_last,
                    }
        elif event is Dictionary:
            new_event = event
            var time_since = time_now - time_last
            if time_since <= 1:
                time_since = recorded_events[-1]["time_before_exec"]
                recorded_events.pop_back()
            new_event["time_before_exec"] = time_since
        if not new_event == null:
            recorded_events.append(new_event)
            time_last = time_now

