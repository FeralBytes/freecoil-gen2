extends Node

var arguments: Dictionary = {}
var acting_as_client = false
var recorded_events: Array = []
var time_last = OS.get_ticks_msec()
var Gut = load("res://addons/gut/comparator.gd").new()
var has_failed = false

# Called when the node enters the scene tree for the first time.
func _ready():
    OS.set_exit_code(125)  # Set exit code to a failure until the tests are complete.
    var args = PM.command_line_args
    if args.has("ITM_act_as"):
        if args["ITM_act_as"] == "client":
            acting_as_client = true
            PM.ITM_act_as_server = false
    else:
        if not PM.ITM_act_as_server:
            acting_as_client = true
    get_recorded_events()
    if acting_as_client:
        print("ITM playing as Client.")
        OS.set_window_position(Vector2(0, 0))
    else:  # Server
        print("ITM playing as Server.")
    playback_events()
    
func _input(event):
    if event is InputEventMouseButton:
            if not event.pressed:  # Then we know it was released.
                pass
                #print(str(event) + " | " + str(event.position))
    elif event is InputEventKey:
        if not event.pressed:
            pass
            #print(str(event) + " | " + str(event.unicode))
        
func launch_clients(options):
    if PM.ITM_allow_launch_clients:
        var all_options = ["--path", ProjectSettings.globalize_path("res://")]
        for option in options:
            all_options.append(option)
#        ["--path", ProjectSettings.globalize_path("res://"), 
#            "--ITM_act_as=client", "ITM_clear_user_dir=true"
#        ]
        # warning-ignore:return_value_discarded
        OS.execute(OS.get_executable_path(), all_options, false)

func playback_events():
    for event in recorded_events:
        var time = float(event["time_before_exec"]) * 0.001 
        yield(get_tree().create_timer(time), "timeout")
        var evt
        print("Playing Back Event: " + str(event))
        match event["type"]:
            "InputEventMouseButton":
                evt = InputEventMouseButton.new()
                evt.button_index = event["button_index"]
                var position = event["position"].replace("(", "")
                position = position.replace(")", "")
                position = position.split(",")
                evt.position = Vector2(position[0], position[1])
                evt.pressed = true
                auto_press_unpress_event(evt)
            "InputEventKey":
                evt = InputEventKey.new()
                evt.unicode = event["unicode"]
                evt.scancode = event["scancode"]
                evt.pressed = true
                auto_press_unpress_event(evt)
            "Test":
                run_tests(event["script_path"])
            "LaunchClient":
                launch_clients(event["options"])
            "End":
                OS.set_exit_code(0)
                get_tree().call_deferred("quit")
            "ButtonPressed":
                var node = PM.get_tree().root.get_node(event["node_path"])
                if node == null:
                    PM.Log("ITM Player: Button press failed, node path "
                        +"is null. Node Path = " + str(event["node_path"]), "critical"
                    )
                elif not is_instance_valid(node):
                    PM.Log("ITM Player: Button press failed, the node "
                        +"is not a valid instance. Node Path = " + str(event["node_path"]), "critical"
                    )
                else:
                    if node is CheckButton:
                        var temp = not node.pressed
                        node.emit_signal("toggled", temp)
                        yield(get_tree(), "idle_frame")
                        if node.pressed != temp:
                            node.pressed = temp
                    else:
                        node.emit_signal("pressed")
#                evt = InputEventMouseButton.new()
#                evt.button_index = 1
#                var camera_pos = PM.TheContainer.get_node(
#                    "Camera/TheCamera").get_camera_position()
#                print(node.get_global_position())
#                print(camera_pos)
#                evt.position = node.get_global_position() - camera_pos
#                print(evt.position)
#                evt.pressed = true
#                auto_press_unpress_event(evt)
            "ClearUserDir":
                PM.erase_user_data_dir()
        
func auto_press_unpress_event(event):
    Input.parse_input_event(event)
    var unpress = event.duplicate()
    unpress.pressed = false
    call_deferred("simulate_unpress", unpress)

func get_recorded_events():
    var itm_events_path = PM.TheContainer.ITM_test_path
    var role = ""
    if acting_as_client:
        role = "client"
    else:
        role = "server"
    itm_events_path = itm_events_path.plus_file(role + ".json")
    var file = File.new()
    # warning-ignore:unused_variable
    var err = file.open(itm_events_path, File.READ)
    var text = file.get_as_text()
    recorded_events = parse_json(text)
    file.close()

func simulate_unpress(event):
    Input.parse_input_event(event)

func _on_Gut_gut_ready():
    $Gut.export_if_tests_found()
    $Gut.import_tests_if_none_found()
    
func run_tests(script_path):
    $Gut.get_gut().test_script(script_path)
    
